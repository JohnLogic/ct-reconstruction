import keras
import keras.backend as Kb
import numpy as np
import tensorflow as tf


##########################################################
# Metrics
##########################################################

# DSSIM = 1 - SSIM (acts like a "proper" distance metric)
def dssim_metric(y_true, y_pred):
    # if (auto_preproc):
    #     prop_y_true, prop_y_pred = _preproc_parameters(y_true, y_pred)
    # else:
    #     prop_y_true, prop_y_pred = y_true, y_pred
    # TODO: inspect if SSIM returns the results that we want (do we need 1 - ssim)
    return tf.subtract(1., tf.reduce_mean(tf.image.ssim(img1=y_true, img2=y_pred, max_val=1.0)))

def msssim_metric(y_true, y_pred):
    return tf.subtract(1.0, tf.reduce_mean(tf.image.ssim_multiscale(img1=y_true, img2=y_pred, max_val=1.0)))

def root_mean_squared_error(y_true, y_pred):
    # if (auto_preproc):
    #     prop_y_true, prop_y_pred = _preproc_parameters(y_true, y_pred)
    # else:
    #     prop_y_true, prop_y_pred = y_true, y_pred
    # TODO: inspect why RMSE returns 0 when comparing two windowed images
    # y_true = tf.Print(y_true, [tf.shape(y_true)], "\ninput shape: ", summarize = -1)
    return Kb.sqrt(Kb.mean(Kb.square(y_true - y_pred)))

################################
######## CUSTOM SSIM ###########
################################

## ref code: https://stackoverflow.com/questions/39051451/ssim-ms-ssim-for-tensorflow
def _tf_fspecial_gauss(size, sigma):
    """Function to mimic the 'fspecial' gaussian MATLAB function
    """
    x_data, y_data = np.mgrid[-size//2 + 1:size//2 + 1, -size//2 + 1:size//2 + 1]

    x_data = np.expand_dims(x_data, axis=-1)
    x_data = np.expand_dims(x_data, axis=-1)

    y_data = np.expand_dims(y_data, axis=-1)
    y_data = np.expand_dims(y_data, axis=-1)

    x = tf.constant(x_data, dtype=tf.float32)
    y = tf.constant(y_data, dtype=tf.float32)

    g = tf.exp(-((x**2 + y**2)/(2.0*sigma**2)))
    return g / tf.reduce_sum(g)

def apply_sobel_filter(img):
    sobel_x = tf.constant([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], tf.float32)
    sobel_x_filter = tf.reshape(sobel_x, [3, 3, 1, 1])
    sobel_y_filter = tf.transpose(sobel_x_filter, [1, 0, 2, 3])
    filtered_x = tf.nn.conv2d(img, sobel_x_filter,
                              strides=[1, 1, 1, 1], padding='SAME')
    filtered_y = tf.nn.conv2d(img, sobel_y_filter,
                              strides=[1, 1, 1, 1], padding='SAME')
    return filtered_x, filtered_y

# "Carbon-copy of 'grdabs' function in gssim matlab impl (see gssim.m)
def grdabs(img1, img2):
    i1, i2 = apply_sobel_filter(img1)
    i3, i4 = apply_sobel_filter(img2)
    gx = tf.add(tf.abs(i1), tf.abs(i2))
    gy = tf.add(tf.abs(i3), tf.abs(i4))
    # NOTE TO SELF: this is a carbon copy from the gssim impl in matlab
    # TODO: the gx, gy look fishy, but probably check out - check
    return gx, gy

# "Carbon-copy of 'gssim' function in gssim matlab impl (see gssim.m)
def custom_tf_gssim(img1, img2, mean_metric=True, size=11, sigma=1.5):

    gx, gy = grdabs(img1, img2)
    # img1 = tf.Print(img1, [tf.shape(img1)], "\ninput shape: ", summarize = -1)

    # This gives a uniform window (we could also gaussian init)
    window = tf.ones([11,11,1,1], tf.float32)
    window = tf.divide(window, tf.reduce_sum(window))

    K1 = 0.01
    K2 = 0.03
    L = 1  # depth of image (255 in case the image has a differnt scale)
    C1 = (K1*L)**2
    C2 = (K2*L)**2
    stride_list = [1, 1, 1, 1]

    mu1 = tf.nn.conv2d(img1, window, strides=stride_list, padding='VALID')
    mu2 = tf.nn.conv2d(img2, window, strides=stride_list, padding='VALID')
    mu1_sq = mu1*mu1
    mu2_sq = mu2*mu2
    mu1_mu2 = mu1*mu2

    gmu1 = tf.nn.conv2d(gx, window, strides=stride_list, padding='VALID')
    gmu2 = tf.nn.conv2d(gy, window, strides=stride_list, padding='VALID')
    gmu1_sq = gmu1*gmu1
    gmu2_sq = gmu2*gmu2
    gmu1_gmu2 = gmu1*gmu2
    gsigma1_sq = tf.abs(tf.nn.conv2d(gx * gx, window, strides=stride_list, padding='VALID') - gmu1_sq)
    gsigma2_sq = tf.abs(tf.nn.conv2d(gy * gy, window, strides=stride_list, padding='VALID') - gmu2_sq)
    gsigma12 = tf.nn.conv2d(gx * gy, window, strides=stride_list, padding='VALID') - gmu1_gmu2

    # if cs_map:
    #     value = (((2*mu1_mu2 + C1)*(2*sigma12 + C2))/((mu1_sq + mu2_sq + C1)*
    #                 (sigma1_sq + sigma2_sq + C2)),
    #             (2.0*sigma12 + C2)/(sigma1_sq + sigma2_sq + C2))
    # else:
    value = ((2*mu1_mu2 + C1)*(2*gsigma12 + C2))/((mu1_sq + mu2_sq + C1)*(gsigma1_sq + gsigma2_sq + C2))

    if mean_metric:
        value = tf.reduce_mean(value)
    return tf.subtract(1., value)

# TODO: note that this metric was initially designed to be used with 4 dims (batch_size, x, y, colors=1)
# NOTE: this metric has been modified to run 3-dim strides and to only use 3 dims instead of 4
def custom_tf_ssim(img1, img2, cs_map=False, mean_metric=True, size=11, sigma=1.5):

    window = _tf_fspecial_gauss(size, sigma) # window shape [size, size]
    K1 = 0.01
    K2 = 0.03
    L = 1  # depth of image (255 in case the image has a differnt scale)
    C1 = (K1*L)**2
    C2 = (K2*L)**2
    stride_list = [1, 1, 1, 1]

    mu1 = tf.nn.conv2d(img1, window, strides=stride_list, padding='VALID')
    mu2 = tf.nn.conv2d(img2, window, strides=stride_list, padding='VALID')
    mu1_sq = mu1*mu1
    mu2_sq = mu2*mu2
    mu1_mu2 = mu1*mu2
    sigma1_sq = tf.nn.conv2d(img1 * img1, window, strides=stride_list, padding='VALID') - mu1_sq
    sigma2_sq = tf.nn.conv2d(img2 * img2, window, strides=stride_list, padding='VALID') - mu2_sq
    sigma12 = tf.nn.conv2d(img1 * img2, window, strides=stride_list, padding='VALID') - mu1_mu2
    if cs_map:
        value = (((2*mu1_mu2 + C1)*(2*sigma12 + C2))/((mu1_sq + mu2_sq + C1)*
                    (sigma1_sq + sigma2_sq + C2)),
                (2.0*sigma12 + C2)/(sigma1_sq + sigma2_sq + C2))
    else:
        value = ((2*mu1_mu2 + C1)*(2*sigma12 + C2))/((mu1_sq + mu2_sq + C1)*
                    (sigma1_sq + sigma2_sq + C2))

    if mean_metric:
        value = tf.reduce_mean(value)
    return tf.subtract(1., value)

# TODO: this function does not work! (problem = for loop) - we already have a PROPER MS-SSIM
# If a for loop is necessary, look up the SLSSIM implementation in tf from before
def custom_tf_ms_ssim(img1, img2, mean_metric=True, level=5):
    weight = tf.constant([0.0448, 0.2856, 0.3001, 0.2363, 0.1333], dtype=tf.float32)
    mssim = []
    mcs = []
    for l in range(level):
        ssim_map, cs_map = custom_tf_ssim(img1, img2, cs_map=True, mean_metric=False)
        mssim.append(tf.reduce_mean(ssim_map))
        mcs.append(tf.reduce_mean(cs_map))
        filtered_im1 = tf.nn.avg_pool(img1, [1,2,2,1], [1,2,2,1], padding='SAME')
        filtered_im2 = tf.nn.avg_pool(img2, [1,2,2,1], [1,2,2,1], padding='SAME')
        img1 = filtered_im1
        img2 = filtered_im2

    # list to tensor of dim D+1
    mssim = tf.stack(mssim, axis=0)
    mcs = tf.stack(mcs, axis=0)

    value = (tf.reduce_prod(mcs[0:level-1]**weight[0:level-1])*
                            (mssim[level-1]**weight[level-1]))

    if mean_metric:
        value = tf.reduce_mean(value)
    return value

def psnr(img1, img2):
    return tf.image.psnr(img1, img2, max_val=1.0)

def split_top_3_in_4_metric(img_1, img_2, divide_one_dim : int, metric, reduction_fn):
    def top_3_in_4_reduction(score_tensor):
        vals, indices = tf.math.top_k(score_tensor, divide_one_dim ** 2 * 3 // 4)
        return reduction_fn(vals)
    return split_metric(img_1, img_2, divide_one_dim, metric, top_3_in_4_reduction)

def split_metric(img_1, img_2, divide_one_dim : int, metric, reduction_fn, initial_image_size = 256):

    # initial_image_size = img_1.shape[1]

    def divide_tensor(tensor, division_index : int, initial_size : int):
        splits = tf.reshape(tf.map_fn(lambda t: tf.split(t, division_index, 2), tf.split(tensor, division_index, 1)),
                            [-1, division_index**2, initial_size // division_index, initial_size // division_index, 1])
        # splits[:, 0, :, :].shape # This is how we unpack individual images from there
        parts = tf.unstack(splits, axis=1)
        stacked = tf.stack(parts)
        return stacked

    stacked_1 = divide_tensor(img_1, divide_one_dim, initial_image_size)
    stacked_2 = divide_tensor(img_2, divide_one_dim, initial_image_size)

    zipped = tf.stack([stacked_1, stacked_2], axis=-1)
    results = tf.map_fn(lambda pair: metric(pair[:,:,:,:,1], pair[:,:,:,:,0]), zipped)
    return reduction_fn(results)

############ BETTER SLSSIM IMPL ###############

MAX_INTENSITY = 255

def slssim_while_cond(t1, t2, i, iters, step, acc):
    return tf.less(i,iters)

def slssim_while_body(t1, t2, i, iters, step, acc):

    # start = tf.constant(i*step)
    start = tf.multiply(i, step)
    # end = tf.constant(step*(i+1) - 1)
    end = tf.subtract(tf.multiply(step, tf.add(i, 1)), 1)
    # print("Start: {}, end: {}".format(start,end) )

    rangedT1 = tf.clip_by_value(t1, start, end)
    rangedT2 = tf.clip_by_value(t2, start, end)
    score = dssim_metric(rangedT1, rangedT2)

    return [t1, t2, tf.add(i,1), iters, step, tf.multiply(acc, score)]


def calcL1(t1,t2):
    subResultT = tf.subtract(t1, t2)
    absResultT = tf.abs(subResultT)
    return tf.reduce_sum(absResultT)

def calcL1_mod(t1,t2, size = 256):

    l1T = calcL1(t1,t2)
    l1T = tf.cast(l1T, tf.float32)

    constOneT = tf.constant(1.0)

    # Maximum value for normalization
    maxValT = tf.multiply(tf.constant(MAX_INTENSITY), (size))
    maxValT = tf.multiply(maxValT, (size))
    maxValT = tf.cast(maxValT, tf.float32)

    dividedT = tf.divide(l1T, maxValT)
    return tf.subtract(constOneT, dividedT)

# NOTE: this SLSSIM is much "cleaner" and intended for optimization
# TODO: look into types, perhaps float32 is not enough
def calcSLSSIMv5(t1, t2, n, size = 256):

    stepPy = (MAX_INTENSITY+1)/n
    step = tf.constant(stepPy, name = 'step')

    n = tf.convert_to_tensor(n, dtype=tf.float32, name = 'iters')
    acc = tf.constant([1], tf.float32, name = 'initialAcc') #NOTE [ ] is necessary for now

    _,_,_,_,_,res = tf.while_loop(
        slssim_while_cond,
        slssim_while_body,
        [t1, t2, 0., n, step, acc],
    )
    l1T = calcL1_mod(t1,t2, size=size)
    product = tf.multiply(l1T, res)
    power = tf.reciprocal(tf.cast(n+1, tf.float32) )
    return tf.pow(product, power)


# def calc_slice_window(img1, img2, slice_pair_tensor, metric,
#                       normalize : bool = False):
#     start, end = slice_pair_tensor[0], slice_pair_tensor[1]
#     img1_slice = tf.clip_by_value(img1, start, end)
#     img2_slice = tf.clip_by_value(img2, start, end)
#     score = metric(img1_slice, img2_slice)
#     return score

def calc_slice_window(img1, img2, slice_pair_tensor, metric, normalize = False):
    start, end = slice_pair_tensor[0], slice_pair_tensor[1]
    step = tf.subtract(end, start)
    img1_slice = tf.clip_by_value(img1, start, end)
    img2_slice = tf.clip_by_value(img2, start, end)

    def normalize_foo(tensor):
        return tf.divide(tf.subtract(tensor, start), step)

    if (normalize):
        score = metric(normalize_foo(img1_slice), normalize_foo(img2_slice))
    else:
        score = metric(img1_slice, img2_slice)
    return score

def SLSSIM_working(img1, img2, n, size = 256, normalize = False):
    MAX_INTENSITY = 255
    step = (MAX_INTENSITY+1) // n
    slice_pairs = [tf.constant([i * step / MAX_INTENSITY, ((i + 1) * step - 1) / MAX_INTENSITY], tf.float32)
                   for i in range(256 // step)]

    ssim_slices = tf.map_fn(lambda pair:
                            calc_slice_window(img1, img2, pair, dssim_metric, normalize=normalize),
                            tf.stack(slice_pairs))
    product = tf.multiply(tf.reduce_prod(ssim_slices), calcL1_mod(img1, img2, size))
    return tf.pow(product, tf.constant(n+1., tf.float32))

def SLGSSIM_test(img1, img2, n, size = 256, normalize = False):
    MAX_INTENSITY = 255
    step = (MAX_INTENSITY+1) // n
    slice_pairs = [tf.constant([i * step / MAX_INTENSITY, ((i + 1) * step - 1) / MAX_INTENSITY], tf.float32)
                   for i in range(256 // step)]

    gssim_slices = tf.map_fn(lambda pair:
                            calc_slice_window(img1, img2, pair, custom_tf_gssim, normalize=normalize),
                            tf.stack(slice_pairs))
    # gssim_slices = tf.Print(gssim_slices, [gssim_slices], "\nGSSIM slices", summarize = -1)
    return tf.reduce_mean(gssim_slices)
    # return tf.pow(product, tf.constant(n+1., tf.float32))

#         "mean_split_4_gssim": lambda x, y: Met.split(x, y, 2, Met.custom_tf_gssim, tf.reduce_mean),
#         "sum_split_4_gssim": lambda x, y: Met.split_metric(x, y, 2, Met.custom_tf_gssim, tf.reduce_sum),
#         "mean_split_16_gssim" : lambda x,y: Met.split_metric(x, y, 4, Met.custom_tf_gssim, tf.reduce_mean),
def max_split_16_gssim(x,y):
    return split_metric(x, y, 4, custom_tf_gssim, tf.reduce_max)

def sum_split_16_gssim(x,y):
    return split_metric(x, y, 4, custom_tf_gssim, tf.reduce_sum)

def mean_split_64_gssim(x,y):
    return split_metric(x, y, 8, custom_tf_gssim, tf.reduce_mean)

def sum_split_64_gssim(x,y):
    return split_metric(x, y, 8, custom_tf_gssim, tf.reduce_sum)

def mean_top_3_in_4_split_16_gssim(x,y):
    return split_top_3_in_4_metric(x, y, 4, custom_tf_gssim, tf.reduce_mean)

def mean_top_3_in_4_split_64_gssim(x,y):
    return split_top_3_in_4_metric(x, y, 8, custom_tf_gssim, tf.reduce_mean)

def old_slssim_4(x,y):
    return SLSSIM_working(x, y, 4),

def old_slssim_8(x,y) :
    return SLSSIM_working(x, y, 8)

def old_slssim_8_normed(x,y) :
    return SLSSIM_working(x, y, 8, normalize=True)

def experimental_split_plus_gssim(x,y) :
    return experiment_metric(x,y)

def slgssim_8(x,y):
    return SLGSSIM_test(x, y, 8)

def slgssim_8_normed(x,y):
    return SLGSSIM_test(x, y, 8, normalize=True)

def slgssim_4(x,y):
    return SLGSSIM_test(x, y, 4)

def slgssim_4_normed(x,y):
    return SLGSSIM_test(x, y, 4, normalize=True)

def mean_split_4_gssim(x,y):
    return split_metric(x, y, 2, custom_tf_gssim, tf.reduce_mean)

def sum_split_4_gssim(x,y):
    return split_metric(x, y, 2, custom_tf_gssim, tf.reduce_sum)

def mean_split_16_gssim(x,y):
    return split_metric(x, y, 4, custom_tf_gssim, tf.reduce_mean)

def experiment_metric(x, y):
    return tf.add(split_top_3_in_4_metric(x, y, 8, custom_tf_gssim, tf.reduce_mean), tf.divide(custom_tf_gssim(x, y), 8*8/4.0))

def experiment_metric_16(x, y):
    return tf.add(split_top_3_in_4_metric(x, y, 4, custom_tf_gssim, tf.reduce_mean), tf.divide(custom_tf_gssim(x, y), 4*4/4.0))

def three_dimensional_metric(stacked_1, stacked_2, metric, reduce_foo = tf.reduce_mean, image_size = 256, sandwich_stack = 4):
    stacked_1 = tf.reshape(stacked_1, [-1, sandwich_stack, image_size, image_size, 1])
    stacked_2 = tf.reshape(stacked_2, [-1, sandwich_stack, image_size, image_size, 1])

    stacked_1_list = tf.unstack(stacked_1, num=sandwich_stack, axis=1)
    stacked_2_list = tf.unstack(stacked_2, num=sandwich_stack, axis=1)
    zipped_stacked_list = list(zip(stacked_1_list, stacked_2_list))
    zipped_stacked_tensor = tf.stack(zipped_stacked_list)
    # zipped_stacked_tensor = tf.Print(zipped_stacked_tensor, [tf.shape(zipped_stacked_tensor)], "\nzipped shape: ", summarize = -1)

    stacked_results = tf.map_fn(lambda tuple: metric(tuple[0], tuple[1]), zipped_stacked_tensor)
    return reduce_foo(stacked_results)

def three_dimensional_reduction_proper_only(stacked_metrics_results):
    return stacked_metrics_results[2]

def three_dimensional_mse(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, keras.losses.mean_squared_error)

def three_dimensional_gssim(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, custom_tf_gssim)

def three_dimensional_experimental_gssim(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, experimental_split_plus_gssim)

def three_dimensional_gssim_proper_only(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, custom_tf_gssim, three_dimensional_reduction_proper_only)

def three_dimensional_experimental_gssim_proper_only(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, experimental_split_plus_gssim, three_dimensional_reduction_proper_only)

def three_dimensional_experimental_ssim_proper_only(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, dssim_metric, three_dimensional_reduction_proper_only)

def three_dimensional_experimental_mse_proper_only(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, keras.losses.mean_squared_error, three_dimensional_reduction_proper_only)

def three_dimensional_experimental_split_plus_gssim(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, experimental_split_plus_gssim)

def three_dimensional_experimental_split_plus_gssim_proper_only(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, experimental_split_plus_gssim, three_dimensional_reduction_proper_only)

def three_dimensional_mean_top_3_in_4_split_64_gssim(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, mean_top_3_in_4_split_64_gssim)

def three_dimensional_experimental_split_plus_gssim_with_mse(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, lambda x,y: experiment_metric(x,y) + tf.multiply(0.01, keras.metrics.mean_squared_error(x,y)) )

def three_dimensional_experimental_split_plus_gssim_proper_only_with_rmse(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, lambda x,y: experiment_metric(x,y) + tf.multiply(0.01, keras.metrics.mean_squared_error(x,y)), three_dimensional_reduction_proper_only)

def three_dimensional_mean_top_3_in_4_split_64_gssim_proper_only(stacked_1, stacked_2):
    return three_dimensional_metric(stacked_1, stacked_2, mean_top_3_in_4_split_64_gssim, three_dimensional_reduction_proper_only)

##########################################################

##########################################################
# Preprocessing: np.array -> tensor [used for testing]
##########################################################

# Preprocesses the parameters to turn them into tensors as required by tensorflow
def _preproc_parameters(y_true, y_pred, should_255_rescale = False):
    return _preprocess_one_parameter(y_true, should_255_rescale), \
           _preprocess_one_parameter(y_pred, should_255_rescale)

# If numpy array, makes 3d, converts to float32 and converts to tensor
def _preprocess_one_parameter(param, should_255_rescale = False, expand_to_4_dims = False):
    if (type(param) is np.ndarray):
        updated_param = param
        param_dims = len(param.shape)
        if (param_dims == 2):
            updated_param = np.expand_dims(param, axis=2)
        elif(param_dims < 2):
            raise ValueError(f"Initial parameter dimensions {param_dims} < 2!")
        if (expand_to_4_dims and len(param.shape) == 3):
            updated_param = np.expand_dims(param, axis=0)
        if (str(param.dtype) != 'float32'):
            # NOTE: DICOM preprocessing results in floats - 'unsafe' casting necessary
            updated_param.astype(np.float32, casting='unsafe')
        tensor_param = tf.convert_to_tensor(updated_param)
        if (should_255_rescale):
            tensor_param = tf.dtypes.cast(tensor_param, tf.float32)
            tensor_param = tf.divide(tensor_param, 255.0)
        return tensor_param
    else:
        return param

