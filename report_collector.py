import argparse
import re

import pandas as pd

import old_repo.sendy as sendy
from evaluate import evaluate_model
from utils import *


def conts(string : str, string2 : str):
    return string2.lower() in string.lower()

def get_int_attr(lines : [str], name : str):
    attr = get_attr(lines, name)
    return int(attr) if attr != 'NA' else -99

def get_attr(lines : [str], name : str, split_char : str = ':'):
    found = list(filter(lambda line: conts(line, name), lines))
    if (len(found) == 0):
        print(f"Found NONE of {name}")
        return "NA"
    if (len(found) != 1):
        print(f"Found {len(found)} of {name}!")
    return found[0].split(split_char)[1].strip()

def get_loss_from_loss_string(loss_string: str, loss_contains_name: str):
    regex_loss_pattern = '\(.*?\)'
    loss_list = re.findall(regex_loss_pattern, loss_string)
    stripped_loss_list = list(map(lambda s: s.strip('(').strip(')'), loss_list))
    loss_value = -9999.9
    try:
        loss_value = float(get_attr(stripped_loss_list, loss_contains_name, split_char=','))
    except Exception as exc:
        print(f"Exception: {exc}")
    return loss_value

def extract_info_from_report(test_file_path : str):
    lines = None
    with open(test_file_path, 'r') as f:
        lines = f.readlines()

    assert (lines is not None)

    df_object = {}

    df_object["epoch_count"] = get_int_attr(lines, 'epoch count')
    df_object["model"] = get_attr(lines, 'model trained')
    df_object["optimizer"] = get_attr(lines, 'optimizer used')
    df_object["batch_size"] = get_int_attr(lines, 'batch size')
    df_object["initial_epochs"] = get_int_attr(lines, 'initial model epochs')

    test_loss_str = get_attr(lines, 'calculated test loss')
    val_loss_str = get_attr(lines, 'calculated validation loss')

    df_object["val_gssim"] = get_loss_from_loss_string(val_loss_str, "gssim")
    df_object["val_ssim"] = get_loss_from_loss_string(val_loss_str, "dssim_metric")
    df_object["val_mse"] = get_loss_from_loss_string(val_loss_str, "'mean_squared_error")
    df_object["val_rmse"] = get_loss_from_loss_string(val_loss_str, "root_mean_squared_error")

    df_object["test_gssim"] = get_loss_from_loss_string(test_loss_str, "gssim")
    df_object["test_ssim"] = get_loss_from_loss_string(test_loss_str, "dssim_metric")
    df_object["test_mse"] = get_loss_from_loss_string(test_loss_str, "'mean_squared_error")
    df_object["test_rmse"] = get_loss_from_loss_string(test_loss_str, "root_mean_squared_error")
    return df_object

if __name__ == '__main__':

    ap = argparse.ArgumentParser(
        description="This script extracts all of the reports from experiment folders")
    ap.add_argument("-f", "--experiment_folder", required=True,
                    help="Experiment folder path")
    ap.add_argument("-c", "--calculate_scores", required=False, action='store_true',
                    help="Should calculate scores")
    ap.add_argument("-d", "--experiment_data_folder", required=False,
                    help="Full path to folder with low/full train/test/val data")
    ap.add_argument("-dt", "--experiment_three_d_data_folder", required=False,
                    help="Full path to folder with 3D low/full train/test/val data")
    ap.add_argument("-a", "--add_all_trained_models", required=False, action="store_true",
                    help="use if you want to include ALL models, even without ***report.txt")
    ap.add_argument("-t", "--test_on_nvi_data", required=False, action="store_true",
                    help="should test on nvi data (test only)")
    ap.add_argument("-s", "--save_predictions", required=False, action="store_true",
                    help="should export predictions")
    args = vars(ap.parse_args())

    ARG_FOLDER_PATH = args["experiment_folder"]
    ARG_SHOULD_CALC = args["calculate_scores"]
    ARG_ADD_ALL_MODELS = args["add_all_trained_models"]
    ARG_EXPERIMENT_DATA_FOLDER = args["experiment_data_folder"]
    ARG_EXPERIMENT_3D_DATA_FOLDER = args["experiment_three_d_data_folder"]
    ARG_NVI_DATA = args["test_on_nvi_data"]
    ARG_SAVE_PRED = args["save_predictions"]
    print(f"Should calculate scores: {ARG_SHOULD_CALC}")

    if (ARG_NVI_DATA):
        if (not ARG_SHOULD_CALC):
            raise ValueError("Experiment data folder must be given")

    if (ARG_SHOULD_CALC):
        if (ARG_EXPERIMENT_DATA_FOLDER is None or ARG_EXPERIMENT_3D_DATA_FOLDER is None):
            raise ValueError("--experiment_data_folder MUST be given if --calculate_scores is enabled")

    experiment_result_folder = ARG_FOLDER_PATH
    # test_file_path = r"C:\Users\Lenovo\repos\ct-reconstruction\output\experiment_results\Testing this crap_unet_gssim_adam_1_2020-04-16_00-23-14\Testing this crap_report.txt"

    result_dataframe = pd.DataFrame()

    result_file_list = []
    experiment_folders_with_report = []
    for experiment_folder in os.listdir(experiment_result_folder):

        experiment_folder_full_path = os.path.join(experiment_result_folder, experiment_folder)
        folder_result_file_list = list(filter(lambda f: f.endswith('report.txt'), os.listdir(experiment_folder_full_path)))

        if (len(folder_result_file_list) != 0):
            file_path = os.path.join(experiment_folder_full_path, folder_result_file_list[0])
            result_file_list.append(file_path)
            experiment_folders_with_report.append(experiment_folder)

    result_dataframe = pd.DataFrame()
    for result_file in result_file_list:
        result_row = extract_info_from_report(result_file)
        result_dataframe = result_dataframe.append(result_row, ignore_index=True)

    formatted_datetime_now = DateUtils.formatted_datetime_now()
    result_dataframe.to_csv(f'results_report.csv', encoding='utf-8', mode='w', header=True, index=False)

    if (ARG_SHOULD_CALC):

        eval_start = DateUtils.formatted_datetime_now()

        if (ARG_ADD_ALL_MODELS):
            # NOTE: this "ignores" the requirement for report to be present beforehand
            experiment_folders_with_report = os.listdir(experiment_result_folder)

        model_dict = {}
        # Getting model list to evaluate
        for experiment_folder in experiment_folders_with_report:
            experiment_folder_full_path = os.path.join(experiment_result_folder, experiment_folder)
            final_model_list = list(
                filter(lambda f: f.endswith('.h5') and "last-only" in f,
                       os.listdir(experiment_folder_full_path)))
            if (len(final_model_list) != 0):
                file_path = os.path.join(experiment_folder_full_path, final_model_list[0])
                model_dict[experiment_folder] = file_path

        print(f"*****************\nTotal found model count: {len(model_dict.keys())}\n***************")

        nice_experiment_string = ""
        global model_val_test_dfs
        model_val_test_dfs = {}
        model_val_test_dfs_3d = {}
        for model_folder in model_dict.keys():
            try:
                nice_experiment_string+=f"  - {model_folder}\n"
                model_path = model_dict[model_folder]
                experiment_name = model_folder
                is_3d = "3d" in experiment_name
                if (not is_3d):
                    val_df, test_df = evaluate_model(experiment_name = experiment_name,
                                                     model_path=model_path,
                                                     data_folder=ARG_EXPERIMENT_DATA_FOLDER,
                                                     nvi_data=ARG_NVI_DATA,
                                                     export_images=ARG_SAVE_PRED,
                                                     is_3d=is_3d,)
                    model_val_test_dfs[model_folder] = (val_df, test_df)
                else:
                    val_df, test_df = evaluate_model(experiment_name = experiment_name,
                                                     model_path=model_path,
                                                     data_folder=ARG_EXPERIMENT_3D_DATA_FOLDER,
                                                     nvi_data=ARG_NVI_DATA,
                                                     export_images=ARG_SAVE_PRED,
                                                     is_3d=is_3d,)
                    model_val_test_dfs_3d[model_folder] = (val_df, test_df)
            except Exception as exc:
                print("Exception for {}\n{}".format(model_folder, exc))


        def calculate_statistics_for_model(model_folder : str, df : pd.DataFrame):
            df_means = df.mean(axis=0).add_prefix('avg_')
            df_maxs = df.max(axis=0).add_prefix('max_')
            df_mins = df.min(axis=0).add_prefix('min_')
            resulting_df = pd.concat([df_means, df_maxs, df_mins])
            resulting_df['model_name'] = model_folder
            return resulting_df.to_dict()

        test_result_df = pd.DataFrame()
        val_result_df = pd.DataFrame()
        test_3d_result_df = pd.DataFrame()
        val_3d_result_df = pd.DataFrame()

        for model_folder in model_val_test_dfs.keys():
            val_df, test_df = model_val_test_dfs[model_folder]

            if (not ARG_NVI_DATA):
                val_df_pruned = val_df.drop(axis=1, columns=['batch', 'size'])
                val_stat_obj = calculate_statistics_for_model(model_folder, val_df_pruned)
                val_result_df = val_result_df.append(val_stat_obj, ignore_index=True)

            test_df_pruned = test_df.drop(axis=1, columns=['batch', 'size'])
            test_stat_obj = calculate_statistics_for_model(model_folder, test_df_pruned)
            test_result_df = test_result_df.append(test_stat_obj, ignore_index=True)

        # TODO: refac this proper
        for model_folder in model_val_test_dfs_3d.keys():
            val_df, test_df = model_val_test_dfs_3d[model_folder]

            if (not ARG_NVI_DATA):
                val_df_pruned = val_df.drop(axis=1, columns=['batch', 'size'])
                val_stat_obj = calculate_statistics_for_model(model_folder, val_df_pruned)
                val_3d_result_df = val_3d_result_df.append(val_stat_obj, ignore_index=True)

            test_df_pruned = test_df.drop(axis=1, columns=['batch', 'size'])
            test_stat_obj = calculate_statistics_for_model(model_folder, test_df_pruned)
            test_3d_result_df = test_3d_result_df.append(test_stat_obj, ignore_index=True)

        eval_end = DateUtils.formatted_datetime_now()

        test_result_df.to_csv(f'test_loss.csv', encoding='utf-8', mode='w', header=True, index=False)
        if (not ARG_NVI_DATA):
            val_result_df.to_csv(f'val_loss.csv', encoding='utf-8', mode='w', header=True, index=False)

        test_3d_result_df.to_csv(f'test_3d_loss.csv', encoding='utf-8', mode='w', header=True, index=False)
        if (not ARG_NVI_DATA):
            val_3d_result_df.to_csv(f'val_3d_loss.csv', encoding='utf-8', mode='w', header=True, index=False)

        global test_result_string, val_result_string
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            test_result_string = str(test_result_df)
            if (not ARG_NVI_DATA):
                val_result_string = str(val_result_df)

        sendy.send_notification_email("Evaluation of models has finished!".format(),
                                      f"Experiment folder:{ARG_EXPERIMENT_DATA_FOLDER}\nFound models:\n{nice_experiment_string}\nEval start: {eval_start}\nEval end: {eval_end}"+
                                      "\n\n ------------------- |Val| ------------------- \n"+val_result_string+
                                      "\n\n ------------------- |Test| ------------------- \n"+test_result_string if not ARG_NVI_DATA else "",
                                      recipient_email_address="jbrusokas@gmail.com"
                                      )
