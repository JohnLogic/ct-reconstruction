# %%%

import tensorflow as tf
tf.test.gpu_device_name()

# %%%

import keras.losses
import keras.backend as K
import pandas as pd

from utils import *
from metrics import *
import metrics as Met
# import dicom_utils as DU

def load_model_from_file(model_path : str):
    # TODO: when adding new metrics, remember to add here!
    model = keras.models.load_model(model_path, compile=True, custom_objects={
        "custom_tf_gssim" : Met.custom_tf_gssim,
        "root_mean_squared_error": Met.root_mean_squared_error,
        "dssim_metric": Met.dssim_metric,
        "msssim_metric": Met.msssim_metric,
        "custom_tf_ssim": Met.custom_tf_ssim,
        "psnr": Met.psnr,
    })
    return model

def generate_predictions(model : keras.Model,
                         input_data_dir : str,
                         target_resolution : int,
                         num_of_images : int = None):

    if (num_of_images is None):
        num_of_images = len(os.listdir(os.path.join(input_data_dir, "0")))

    testing_low_dose_generator = _get_flow_gen(input_data_dir=input_data_dir, target_resolution=target_resolution)

    keras.losses.custom_tf_gssim = custom_tf_gssim
    keras.losses.root_mean_squared_error = Met.root_mean_squared_error,
    keras.losses.dssim_metric = Met.dssim_metric
    keras.losses.msssim_metric = Met.msssim_metric
    keras.losses.custom_tf_ssim = Met.custom_tf_ssim


    # NOTE: these really don't matter in this case, we only care IF we want to continue training
    optimizer = keras.optimizers.adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    loss_foo = Met.dssim_metric

    # model.compile(optimizer=optimizer,
    #                   loss=loss_foo,
    #                   metrics=[keras.losses.mean_squared_error,
    #                            Met.root_mean_squared_error,
    #                            Met.dssim_metric,
    #                            Met.msssim_metric,
    #                            Met.custom_tf_ssim,
    #                            # Met.custom_tf_ms_ssim,
    #                            Met.custom_tf_gssim,
    #                            ],)

    predictions = model.predict_generator(testing_low_dose_generator,
                                          verbose=1,
                                          steps=num_of_images,
                                          use_multiprocessing=False)

    return predictions

def compare_low_full_predicted_images(data_dir : str,
                                      output_dir : str,
                                      model : keras.Model,
                                      display_here : bool = False,
                                      limited_image_count : int = None):

    low_dose_image_dir = os.path.join(data_dir, "test_low_dose")

    low_dose_image_count = len(os.listdir(os.path.join(low_dose_image_dir, "0")))
    num_of_images = low_dose_image_count if limited_image_count is None else limited_image_count

    predictions = generate_predictions(
        model=model, input_data_dir=low_dose_image_dir,
        target_resolution=256, num_of_images=num_of_images)
    full_dose_flow_gen = _get_flow_gen(os.path.join(data_dir, "test_full_dose"), target_resolution=256)
    low_dose_flow_gen = _get_flow_gen(low_dose_image_dir, target_resolution=256)

    low_full_pred_triples = []

    result_dataframe = pd.DataFrame()
    dataframe_file_path = os.path.join(output_dir, "test_set_results.csv")

    for i in range(num_of_images):
        low_dose_image = np.squeeze(low_dose_flow_gen.next(), axis=0)
        full_dose_image = np.squeeze(full_dose_flow_gen.next(), axis=0)
        predicted_image = predictions[i]
        low_full_pred_triples.append(
                                      (low_dose_image,
                                       full_dose_image,
                                       predicted_image)
                                      )

        # TODO: there are problems with pydicom compatibility - do not use that library in code that will be executed remotely
        # if (display_here):
        #     DU.show_image_trio(low_dose_image, predicted_image, full_dose_image)

        proc_low = Met._preprocess_one_parameter(low_dose_image, expand_to_4_dims=True)
        proc_pred = Met._preprocess_one_parameter(predicted_image, expand_to_4_dims=True)
        proc_full = Met._preprocess_one_parameter(full_dose_image, expand_to_4_dims=True)

        list_of_metric_pairs = [
            ('ssim', Met.dssim_metric),
            ('gssim', Met.custom_tf_gssim),
            ('msssim', Met.msssim_metric)
        ]

        result_row = {"image_number": i}

        for metric_name, metric in list_of_metric_pairs:
            low_full_score = metric(proc_low, proc_full)
            low_pred_score = metric(proc_low, proc_pred)
            pred_full_score = metric(proc_pred, proc_full)

            low_full_score_eval = K.eval(low_full_score)
            low_pred_score_eval = K.eval(low_pred_score)
            pred_full_score_eval = K.eval(pred_full_score)

            result_row[f'low_full_{metric_name}'] = low_full_score_eval
            result_row[f'low_pred_{metric_name}'] = low_pred_score_eval
            result_row[f'pred_full_{metric_name}'] = pred_full_score_eval

            print(f"{i}:\tmetric: {metric_name}\tlow-full: {low_full_score_eval}\tlow-pred: {low_pred_score_eval}\tpred-full:{pred_full_score_eval}")

        result_dataframe = result_dataframe.append(result_row, ignore_index=True)

    result_dataframe.to_csv(dataframe_file_path, encoding='utf-8', mode='a', header=True, index=False)

    model.evaluate_generator()
    return low_full_pred_triples

def _get_flow_gen(input_data_dir : str, target_resolution : int):

    batch_size = 1
    target_size = (target_resolution, target_resolution)
    interpolation_method = "nearest"

    flow_param_dictionary = dict(
        image_config=dict(
            rescale=1. / 255,
        ),
        fit_smpl_size=1,
        target_size=target_size,
        batch_size=batch_size,
        shuffle=False,
        seed=1,
        interpolation_method=interpolation_method,
    )

    generator_config = flow_param_dictionary.copy()
    generator_config['directory'] = input_data_dir
    generator = KerasUtils.get_img_fit_flow(**generator_config)

    return generator

if __name__ == '__main__':
    model_path = r"C:\Users\Lenovo\Desktop\test_no_activation_last-only_epoch=100.h5"
    model = load_model_from_file(model_path=model_path)
    triples = compare_low_full_predicted_images(data_dir="./output/prepared_mayo_experiment_data_other_split",
                                                output_dir=".",
                                                model=model,
                                                display_here=True,
                                                limited_image_count=1,
                                                )
