import cv2
import tensorflow as tf
from skimage.measure import compare_ssim

import metrics as Met
from utils import *


def do_comparison():

    low_dose_path = os.path.join('.', 'data', 'example-ct-pngs', 'quarter_dose.png')
    full_dose_path = os.path.join('.', 'data', 'example-ct-pngs', 'full_dose.png')

    # NOTE: based on the old xray similarity stuff
    def open_img(imgPath, mode=0):
        # NOTE there is no need to do .astype( <int> ) conversions
        # We use openCV2 arithmetic subtract to counteract uint8
        return cv2.imread(imgPath, mode)

    low_dose_image = open_img(low_dose_path)
    full_dose_image = open_img(full_dose_path)

    cv2_ssim_value = compare_ssim(low_dose_image, full_dose_image, full=False)

    low_dose_tensor, full_dose_tensor = Met._preproc_parameters(low_dose_image, full_dose_image, should_255_rescale=True)
    print(f"Shape of tensors: {low_dose_tensor.shape}")
    tf_ssim_value = Met.dssim_metric(low_dose_tensor, full_dose_tensor)
    tf_msssim_value = Met.msssim_metric(low_dose_tensor, full_dose_tensor)
    custom_ssim_value = Met.custom_tf_ssim(tf.expand_dims(low_dose_tensor, 0), tf.expand_dims(full_dose_tensor, 0))
    custom_gssim_value = Met.custom_tf_gssim(tf.expand_dims(low_dose_tensor, 0), tf.expand_dims(full_dose_tensor, 0))
    custom_msssim_value = Met.custom_tf_ms_ssim(tf.expand_dims(low_dose_tensor, 0), tf.expand_dims(full_dose_tensor, 0))
    # custom_ssim_value_3dims = Met.tf_ssim(low_dose_tensor, full_dose_tensor)

    print(f"OpenCV2 SSIM: {cv2_ssim_value}")
    print(f"Tensorflow 1-SSIM: {tf_ssim_value}")
    print(f"Custom Tensorflow SSIM: {custom_ssim_value}")
    print(f"Custom Tensorflow GSSIM: {custom_gssim_value}")
    print(f"Tensorflow 1-MSSSIM: {tf_msssim_value}")
    print(f"Custom Tensorflow MS-SSIM: {custom_msssim_value}")

if __name__ == '__main__':
    do_comparison()

