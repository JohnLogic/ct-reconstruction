import os
from pathlib import Path  # for properly dealing with filenames and stuff

import matplotlib.image as mpimg  # used for saving
import matplotlib.pyplot as plt
import numpy as np
import pydicom

# This utility library provides utility functions for manipulating dicom images
# DICOM loading and preproc. inspired by: https://vincentblog.xyz/posts/medical-images-in-python-computed-tomography
# JUPYTER LINK: https://nbviewer.jupyter.org/github/vincent1bt/Healthy-notebooks/blob/master/CT_images.ipynb

# NOTE to self: use type hints
# LINK: https://docs.python.org/3/library/typing.html

EXAMPLE_CT_DCMS_FOLDER_PATH = os.path.join(".", "data", "example-ct-dcms")
EXAMPLE_FULL_IMAGE_NAME = "example_full_image"
OUTPUT_DATA_PATH = os.path.join(".","output")

TEST_EXAMPLE_DCM_IMAGE_PATH : str = os.path.join(EXAMPLE_CT_DCMS_FOLDER_PATH, os.listdir(EXAMPLE_CT_DCMS_FOLDER_PATH)[0])
TEST_EXAMPLE_DCM_IMAGE : pydicom.dataset.FileDataset = pydicom.read_file(TEST_EXAMPLE_DCM_IMAGE_PATH)

# >> Gets medical DICOM image representation as a HU (Hounsfield units) numpy array
# NOTE: equiv to transform_to_hu
def get_dcm_image_hu_array(medical_image : pydicom.dataset.FileDataset) -> np.ndarray:
    intercept = medical_image.RescaleIntercept
    slope = medical_image.RescaleSlope
    medical_image_pixels = medical_image.pixel_array
    hu_image = medical_image_pixels * slope + intercept
    return hu_image

# >> Gets windowed image
# NOTE: window_center === window_level
def window_hu_image(hu_image : np.ndarray, window_center : int, window_width : int):
    img_min = window_center - window_width // 2
    img_max = window_center + window_width // 2
    windowed_image = hu_image.copy()
    windowed_image[windowed_image < img_min] = img_min
    windowed_image[windowed_image > img_max] = img_max
    return windowed_image

# > Shows original DICOM image vs windowed counterpart (passed as param)
# Optionally SAVES the images to output directory
def show_dcm_image(dcm_image: pydicom.dataset.FileDataset, windowed_hu_image, save : bool = False) -> None:
    # hu_image = get_dcm_image_hu_array(dcm_image)
    # windowed_hu_image = window_hu_image(hu_image, window_center=window_level, window_width=window_width)

    file_name = os.path.basename(dcm_image.filename) # NOTE: .filename gives path
    dcm_image_pixels = dcm_image.pixel_array

    plt.figure(figsize=(10, 5))
    plt.style.use('grayscale')

    plt.subplot(121) #NOTE: plt.subplot takes 3 digits (rows,cols,position)
    plt.imshow(dcm_image_pixels)
    plt.title('Original')
    plt.axis('off')

    plt.subplot(122)
    plt.imshow(windowed_hu_image)
    plt.title('brain image')
    plt.axis('off')
    plt.show()

    if save:
        mpimg.imsave(os.path.join(OUTPUT_DATA_PATH, f'{file_name[:-4]}-dcm-image.png'), dcm_image_pixels)
        mpimg.imsave(os.path.join(OUTPUT_DATA_PATH, f'{file_name[:-4]}-windowed-image.png'), windowed_hu_image)

def preprocess_image(image_full_path : str, window_level : int, window_width : int,
                     save : bool = False, output_dir : str = os.path.join(".","output"),
                     preprocessed_image_prefix : str = "", preprocessed_image_postfix : str = ""):
    image = pydicom.read_file(image_full_path, force=True)
    hu_array = get_dcm_image_hu_array(image)
    windowed_image = window_hu_image(hu_array, window_level, window_width)

    plt.style.use('grayscale')
    if (save):
        image_full_path_obj = Path(image_full_path)
        image_name = image_full_path_obj.stem
        output_image_extension = ".png"
        # image_parent_path_obj = image_full_path_obj.parent
        output_image_path_obj = Path(output_dir).joinpath(preprocessed_image_prefix+image_name+preprocessed_image_postfix+output_image_extension)
        mpimg.imsave(f'{str(output_image_path_obj)}', windowed_image)

    return windowed_image

def show_image_trio(low_dose_image:np.ndarray, predicted_image:np.ndarray, full_dose_image:np.ndarray, squeeze = True):

    if (squeeze):
        low_dose_image = low_dose_image.squeeze()
        predicted_image = predicted_image.squeeze()
        full_dose_image = full_dose_image.squeeze()

    plt.figure(figsize=(10, 5))
    plt.style.use('grayscale')

    def add_one_image_to_plot(image : np.ndarray, subplot_number : int, title : str):
        plt.subplot(subplot_number) #NOTE: plt.subplot takes 3 digits (rows,cols,position)
        plt.imshow(image)
        plt.title(title)
        plt.axis('off')

    add_one_image_to_plot(low_dose_image, 131, "low")
    add_one_image_to_plot(predicted_image, 132, "predicted")
    add_one_image_to_plot(full_dose_image, 133, "full")

    plt.show()


###################################################################
##### [ RUN MAIN ] #####
###################################################################

def main():
    hu_array = get_dcm_image_hu_array(TEST_EXAMPLE_DCM_IMAGE)

    # Gets width and center parameters from the DICOM image metadata
    width = int(TEST_EXAMPLE_DCM_IMAGE.WindowWidth) # window width
    center = int(TEST_EXAMPLE_DCM_IMAGE.WindowCenter) # window level / center of the image

    windowed_image = window_hu_image(hu_array, center, width)
    show_dcm_image(TEST_EXAMPLE_DCM_IMAGE, windowed_image, save=True)
    print("Main done")

