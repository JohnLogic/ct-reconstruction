import argparse
import math

import pandas as pd
from keras.models import *

import metrics as Met
from utils import *

noise_levels = [0.001, 0.01, 0.003]

def identity_model():
    inputs = Input(shape=(512,512,1))
    model = Model(inputs=[inputs], outputs=[inputs])
    return model

metrics_dict = {
        "custom_tf_gssim": Met.custom_tf_gssim,
        "root_mean_squared_error": Met.root_mean_squared_error,
        #"dssim_metric": Met.dssim_metric,
        #"msssim_metric": Met.msssim_metric,
        #"custom_tf_ssim": Met.custom_tf_ssim,
        #"psnr": Met.psnr,
        "mean_split_4_gssim": Met.mean_split_4_gssim,
        #"sum_split_4_gssim": Met.sum_split_4_gssim,
        "mean_split_16_gssim": Met.mean_split_16_gssim,
        #"max_split_16_gssim": Met.max_split_16_gssim,
        #"sum_split_16_gssim": Met.sum_split_16_gssim,
        #"mean_split_64_gssim": Met.mean_split_64_gssim,
        #"sum_split_64_gssim": Met.sum_split_64_gssim,
        #"mean_top_3_in_4_split_16_gssim": Met.mean_top_3_in_4_split_16_gssim,
        #"mean_top_3_in_4_split_64_gssim": Met.mean_top_3_in_4_split_64_gssim,
        #"old_slssim_4": Met.old_slssim_4,
        #"old_slssim_8": Met.old_slssim_8,
        #"experiment_metric" : Met.experiment_metric,
        #"slgssim_8": Met.slgssim_8,
        #"slgssim_4": Met.slgssim_4,
        #"three_dimensional_gssim": Met.three_dimensional_gssim,
        #"three_dimensional_experimental_gssim": Met.three_dimensional_experimental_gssim,
        #"three_dimensional_gssim_proper_only": Met.three_dimensional_gssim_proper_only,
        #"three_dimensional_experimental_gssim_proper_only": Met.three_dimensional_experimental_gssim_proper_only,
        #"three_dimensional_ssim_proper_only": Met.three_dimensional_experimental_ssim_proper_only,
        #"three_dimensional_mse_proper_only": Met.three_dimensional_experimental_mse_proper_only,
}


def benchmark_metrics(data_folder: str, noise_level):

    EXPERIMENT_DATA_FOLDER = data_folder

    def noise_foo(image : np.ndarray, noise_level : int = noise_level):
        s = np.random.normal(0, noise_level, 512 * 512).reshape((512, 512, 1))
        noisy = np.add(image, s)
        print(np.sum(np.subtract(noisy, s)))
        return noisy


    BATCH_SIZE = 1
    flow_param_dictionary = dict(
        image_config=dict(
            rescale=1. / 255,
        ),
        fit_smpl_size=1,
        target_size=(512, 512),
        batch_size=BATCH_SIZE,  # <<<< CHANGED <<<<
        shuffle=False,
        seed=1,
        interpolation_method="nearest",
    )

    TEST_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'test_full_dose')

    # use carbon-copy of validation params
    test_flow_param_dictionary = flow_param_dictionary.copy()

    test_full_dose_param_dictionary = test_flow_param_dictionary.copy()
    test_full_dose_param_dictionary['directory'] = TEST_FULL_DOSE_DATA_DIR

    test_low_dose_param_dictionary = test_flow_param_dictionary.copy()
    test_low_dose_param_dictionary['directory'] = TEST_FULL_DOSE_DATA_DIR
    test_full_dose_param_dictionary['image_config'] = {
        "rescale" : 1. / 255,
        "preprocessing_function" : lambda img: noise_foo(img, noise_level)
    }

    # NOTE: ** passes dict as param values to given function
    full_dose_test_flow = KerasUtils.get_img_fit_flow(**test_full_dose_param_dictionary)
    low_dose_test_flow = KerasUtils.get_img_fit_flow(**test_low_dose_param_dictionary)

    assert (full_dose_test_flow.n != 0)
    assert (low_dose_test_flow.n != 0)
    assert (full_dose_test_flow.n == low_dose_test_flow.n)
    low_full_test_generator = zip(low_dose_test_flow, full_dose_test_flow)
    num_of_test_pairs = full_dose_test_flow.n

    class MyCustomCallback(keras.callbacks.Callback):

        def __init__(self):
            super().__init__()
            self.result_df = pd.DataFrame()

        def on_test_batch_end(self, batch, logs=None):
            self.result_df = self.result_df.append(logs, ignore_index=True)

        def get_result_df(self):
            return self.result_df

    before_predictions = DateUtils.now()
    test_cb = MyCustomCallback()

    model = identity_model()
    model.compile(optimizer='adam',  # NOTE: use this, we define optimizer via arguments
                  loss=metrics_dict['custom_tf_gssim'],
                  metrics=list(metrics_dict.values()),
                  )

    evaluated_test_loss = model.evaluate_generator(low_full_test_generator,
                                                   steps=math.ceil(num_of_test_pairs / BATCH_SIZE), verbose=1,
                                                   callbacks=[
                                                       test_cb])
    test_loss_with_names = list(zip(model.metrics_names, evaluated_test_loss))
    test_df = test_cb.get_result_df()
    test_df.to_csv(os.path.join(f'benchmark_noise_{noise_level}.csv'), encoding='utf-8', mode='w', header=True, index=False)

    after_predictions = DateUtils.now()
    print(f"Evaluation duration: {after_predictions - before_predictions}")
    return test_loss_with_names

if __name__ == '__main__':

    ap = argparse.ArgumentParser(description="This script extracts all of the reports from experiment folders")
    ap.add_argument("-d", "--experiment_data_folder", required=True,
                    help="Full path to folder with low/full train/test/val data")
    args = vars(ap.parse_args())

    ARG_EXPERIMENT_DATA_FOLDER = args["experiment_data_folder"]
    data_folder = ARG_EXPERIMENT_DATA_FOLDER

    for noise_level in noise_levels:
        benchmark_metrics(data_folder=data_folder, noise_level=noise_level)
