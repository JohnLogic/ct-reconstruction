import os
import unittest
from pathlib import Path
from unittest import TestCase

import data_prep as Prep


class TestDataPrep(TestCase):

    ### --- setUp / tearDown ---

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    ### --- Unit tests ---

    def test_generate_pngs_from_dcms(self):

        prepared_full_dose_image_prefix = "full_dose_test"
        prepared_low_dose_image_prefix = "low_dose_test"

        test_output_dir = os.path.join('', 'output')
        test_output_path = Path(test_output_dir).joinpath('dcm-pngs')
        test_output_path.mkdir(parents=True, exist_ok=True)  # make sure path exists

        for image_file_path in test_output_path.iterdir():
            if ( image_file_path.stem.startswith(prepared_full_dose_image_prefix) or
                 image_file_path.stem.startswith(prepared_low_dose_image_prefix)
               ): image_file_path.unlink()

        pre_listdir = os.listdir(test_output_path)
        pairs = Prep.fetch_image_pair_dirs(file_extensions=['IMA'])
        _ = Prep.prepare_ct_dataset(pairs, window_level=50, window_width=400, output_dir=test_output_path,
                                    prepared_full_dose_image_prefix=prepared_full_dose_image_prefix,
                                    prepared_low_dose_image_prefix=prepared_low_dose_image_prefix)
        post_listdir = os.listdir(test_output_path)

        generated_image_count = len(pairs) * 2
        self.assertEqual(len(pre_listdir) + generated_image_count, len(post_listdir), "Generated image count is not correct")

    ### --- main method ---

    if __name__ == '__main__':
        unittest.main()