import random

import matplotlib.pyplot as plt
import numpy
import numpy as np
import torch
import torchvision.transforms as transforms
from scipy import spatial
from skimage import io
from skimage.transform import resize
from torchvision import datasets

transform = transforms.ToTensor()
train_data = datasets.MNIST(root='data', train=True, download=True, transform=transform)

test_data = datasets.MNIST(root='data', train=False, download=True, transform=transform)

numpy.random.seed(123)
N_CLUSTERS = 20

# Usage:
# from  simulated import *
# a, b = generate_random_data(192, 192, 1)
# plot(a, b)


def generate_random_data(height, width, count, train=True):
    x, y = zip(*[generate_img_and_mask(height, width, i, train=True) for i in range(0, count)])
    return x, y


def get_random_location(width, height, zoom=1.0):
    x = int(width * random.uniform(0.1, 0.9))
    y = int(height * random.uniform(0.1, 0.9))

    size = int(min(width, height) * random.uniform(0.06, 0.12) * zoom)

    return (x, y, size)

def generate_img_and_mask(height, width, k = 0, train=True):
    shape = (height, width)

    arr = np.zeros((height, width))
    clusters = np.random.randint(N_CLUSTERS) + 2# random number of clusters
    centers = []
    values = []
    for k in range(2, clusters + 2):
        random_location = get_random_location(*shape)
        centers.append(random_location[:-1])
        val = np.random.random()
        values.append(val)

    tree = spatial.KDTree(centers)
    idx = []
    for i in range(height):
        for j in range(width):
            idx.append((i,j))
    tree.query(idx)
    _, cs = tree.query(idx)

    for i in range(len(idx)):
            arr[idx[i]] = values[cs[i]]

    for val in values:
        x, y, size = get_random_location(*shape)
        if train:
            img = train_data[k][0].numpy()
        else:
            img = test_data[k][0].numpy()
        c = resize(img[0], (size, size))
        left_val = 1 - val
        disc = np.random.random()
        #embed()
        if c.shape == (size, size) == arr[x:(x+size), y:(y+size)].shape:
            arr[x:(x+size), y:(y+size)] += c * left_val * disc

    arr = np.reshape(arr, (height, width)).astype(np.float32)
    arr = np.clip(arr, 0, 1)

    noise_factor = min(values) / 6
    noisy_imgs = arr + noise_factor * torch.randn(*shape).numpy()
    noisy_imgs = np.clip(noisy_imgs, 0, 1)

    return arr, noisy_imgs

def plot(img, img_noisy, i = 0):
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(5, 3))
    axes[0].imshow(img[i])
    axes[1].imshow(img_noisy[i])
    fig.tight_layout()
    plt.show()


def save_train():
    for k in range(100):
        f, l = generate_random_data(256, 256, 1)
        f = np.reshape(f[0], (256, 256, 1)).astype(np.float32)
        l = np.reshape(l[0], (256, 256, 1)).astype(np.float32)

        F = (np.asarray(f).repeat(3, axis=2) * 255).astype(np.uint8)
        L = (np.asarray(l).repeat(3, axis=2) * 255).astype(np.uint8)
        io.imsave("./train/full/img_" + str(k) + ".png", F)
        io.imsave("./train/low/img_" + str(k) + ".png", L)



def save_test():
    for k in range(100):
        f, l = generate_random_data(256, 256, 1, False)
        f = np.reshape(f[0], (256, 256, 1)).astype(np.float32)
        l = np.reshape(l[0], (256, 256, 1)).astype(np.float32)

        F = (np.asarray(f).repeat(3, axis=2) * 255).astype(np.uint8)
        L = (np.asarray(l).repeat(3, axis=2) * 255).astype(np.uint8)
        io.imsave("./test/full/img_" + str(k) + ".png", F)
        io.imsave("./test/low/img_" + str(k) + ".png", L)

