 
function [Gx,Gy]=grdabs(i_org,i_dis) 
%GRDABS uses sobel to abstract the gradient component,then get the 
%Amplitude of the piction. 
%we make use of sobel to filter ,and it can reflect the edge of one picture 
%  org=rgb2ycbcr(i_org); 
%  test=rgb2ycbcr(i_dis); 
%  
%  %ԭʼͼ������ȡ�ɫ�ȷ��� 
%  y1=org(:,:,1); 
% %  cb1=org(:,:,2); 
% %  cr1=org(:,:,3); 
%  
% %����ͼ������ȡ�ɫ�ȷ��� 
%  y2=test(:,:,1); 
% cb2=test(:,:,2); 
% cr2=test(:,:,3); 
y1=double(i_org); 
y2=double(i_dis); 
h1=[-1,-2,-1;0,0,0;1,2,1];%ˮƽ 
h2=[-1,0,1;-2,0,2;-1,0,1];%��ֱ 
 
    i1=filter2(h1,y1);   %2άfir�˲��� 
    i2=filter2(h2,y1); 
    Gx=abs(i1)+abs(i2); 
      
    i3=filter2(h1,y2); 
    i4=filter2(h2,y2); 
    Gy=abs(i3)+abs(i4); 
end


 
 
 function [gmssim,gmssim_map]=gssim(i_org,i_dis) 
%function [gmssim,gmssim_map]=gssim(i_org,i_dis,K,window, L) 
 
%GSSIM�� 
%L:���� 
%C:�Աȶ� 
%S:�ṹ 
 
%GSSIM_MAP it is based on the gradient of ssim 
% clear all 
% clc 
% i_org=imread('F:\quality assessment picture\live\refimgs\sailing2.bmp'); 
% i_dis=imread('F:\quality assessment picture\live\gblur\img10.bmp'); 
 if size(i_org,3)~=1   %�ж�ͼ��ʱ���ǲ�ɫͼ������ǣ����Ϊ3������Ϊ1 
   org=rgb2ycbcr(i_org); 
   test=rgb2ycbcr(i_dis); 
   y1=org(:,:,1); 
   y2=test(:,:,1); 
   y1=double(y1); 
   y2=double(y2); 
 else  
     y1=double(i_org); 
     y2=double(i_dis); 
 end 
[Gx,Gy]=grdabs(y1,y2); 
K = [0.01 0.03]; 
window = ones(8); 
L = 255; 
C1 = (K(1)*L)^2; 
C2 = (K(2)*L)^2; 
window = window/sum(sum(window)); 
img1 = double(y1); 
img2 = double(y2); 
 
mu1   = filter2(window, img1, 'same');%%���ֵ����ssimģ�����Ե�����ġ� 
mu2   = filter2(window, img2, 'same'); 
mu1_sq = mu1.*mu1; 
mu2_sq = mu2.*mu2; 
mu1_mu2 = mu1.*mu2;%%%���� 
gmu1  =filter2(window, Gx, 'same'); 
gmu2  =filter2(window, Gy, 'same'); 
gmu1_sq = gmu1.*gmu1; 
gmu2_sq = gmu2.*gmu2; 
gmu1_gmu2 = gmu1.*gmu2;%%%�ݶ� 
gsigma1_sq = abs(filter2(window, Gx.*Gx, 'same') - gmu1_sq);%%%���ݶȵķ��D(x)=E(x^2)-(Ex)^2 
gsigma2_sq = abs(filter2(window, Gy.*Gy, 'same') - gmu2_sq); 
gsigma12  =filter2(window, Gx.*Gy, 'same') - gmu1_gmu2;%%%���Ϸ�� 
 
% L=(2*mu1_mu2 + C1)./(mu1_sq +mu2_sq + C1);%%�������ƶ� 
% C=(2*(sqrt(gsigma1_sq.*gsigma2_sq))+C2)./(gsigma1_sq + gsigma2_sq + C2);%%�ݶȶԱȶ����ƶ� 
% S=(2*gsigma12+C2)./(2*(sqrt(gsigma1_sq.*gsigma2_sq))+C2);%%�ݶ������� 
% if (C1 > 0 & C2 > 0) 
gmssim_map= ((2*mu1_mu2 + C1).*(2*gsigma12 + C2))./((mu1_sq +mu2_sq + C1).*(gsigma1_sq + gsigma2_sq + C2)); 
%%gmssim_map=L*C*S.....��ΪC2��С������C��S���ʱ��Լ��  gsigma1*gsigma2. 
 
 
 
% else 
%    numerator1 = 2*mu1_mu2 + C1; 
%    numerator2 = 2*sigma12 + C2; 
%    
%    gdenominator1 = gmu1_sq + gmu2_sq + C1; 
%    gdenominator2 = gsigma1_sq + gsigma2_sq + C2; 
%     
%    index2 = (gdenominator1.*gdenominator2 > 0); 
%    gmssim_map(index2)=(numerator1(index2).*numerator2(index2))./(gdenominator1(index2).*gdenominator2(index2)); 
%    
%    index2 = (gdenominator1 ~= 0) & (gdenominator2 == 0); 
%    gmssim_map(index2) = numerator1(index2)./gdenominator1(index2); 
% end 
gmssim = mean2(gmssim_map); 
 
end

