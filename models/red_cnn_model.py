from keras.layers import *
from keras.models import *


# def reference_tf_red_cnn(inputs):
#   sc0 = inputs
#   # conv1
#   inputs = tf.layers.conv2d(inputs, 96, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = tf.nn.leaky_relu(inputs)
#   # conv2
#   inputs = tf.layers.conv2d(inputs, 96, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = tf.nn.leaky_relu(inputs)
#   sc1 = inputs
#   # conv3
#   inputs = tf.layers.conv2d(inputs, 96, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = tf.nn.leaky_relu(inputs)
#   # conv4
#   inputs = tf.layers.conv2d(inputs, 96, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = tf.nn.leaky_relu(inputs)
#   sc2 = inputs
#   # conv5
#   inputs = tf.layers.conv2d(inputs, 96, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = tf.nn.leaky_relu(inputs)
#   # deconv1
#   inputs = tf.layers.conv2d_transpose(inputs, 96, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = inputs + sc2
#   inputs = tf.nn.leaky_relu(inputs)
#   # deconv2
#   inputs = tf.layers.conv2d_transpose(inputs, 96, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = tf.nn.leaky_relu(inputs)
#   # deconv3
#   inputs = tf.layers.conv2d_transpose(inputs, 96, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = inputs + sc1
#   inputs = tf.nn.leaky_relu(inputs)
#   # deconv4
#   inputs = tf.layers.conv2d_transpose(inputs, 96, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = tf.nn.leaky_relu(inputs)
#   # deconv5
#   inputs = tf.layers.conv2d_transpose(inputs, 1, kernel_size=(5, 5), strides=(1, 1), padding='same', kernel_initializer=tf.random_normal_initializer(stddev=1e-2), data_format='channels_first')
#   inputs = inputs + sc0
#   inputs = tf.nn.leaky_relu(inputs)
#   inputs = tf.identity(inputs, 'outputs')
#   return inputs

def keras_red_cnn(img_rows, img_cols, use_last_activation = True):
    inputs = Input(shape=(img_rows, img_cols, 1))
    sc0 = inputs # NOTE: we do not use sc0, instead we use 'inputs' - to be more inline with other implementations

    conv1 = Conv2D(96, (5, 5), strides=(1,1), activation='relu', padding='same', kernel_initializer='he_normal')(inputs)
    conv1 = LeakyReLU()(conv1)

    conv2 = Conv2D(96, (5, 5), strides=(1,1), activation='relu', padding='same', kernel_initializer='he_normal')(conv1)
    conv2 = LeakyReLU()(conv2)
    sc1 = conv2

    conv3 = Conv2D(96, (5, 5), strides=(1,1), activation='relu', padding='same', kernel_initializer='he_normal')(conv2)
    conv3 = LeakyReLU()(conv3)

    conv4 = Conv2D(96, (5, 5), strides=(1, 1), activation='relu', padding='same', kernel_initializer='he_normal')(conv3)
    conv4 = LeakyReLU()(conv4)
    sc2 = conv4

    conv5 = Conv2D(96, (5, 5), strides=(1, 1), activation='relu', padding='same', kernel_initializer='he_normal')(conv4)
    conv5 = LeakyReLU()(conv5)

    deconv1 = Conv2DTranspose(96, (5, 5), strides=(1, 1), activation='relu', padding='same', kernel_initializer='he_normal')(conv5)
    deconv1 = add([deconv1, sc2])
    deconv1 = LeakyReLU()(deconv1)

    deconv2 = Conv2DTranspose(96, (5, 5), strides=(1, 1), activation='relu', padding='same', kernel_initializer='he_normal')(deconv1)
    deconv2 = LeakyReLU()(deconv2)

    deconv3 = Conv2DTranspose(96, (5, 5), strides=(1, 1), activation='relu', padding='same', kernel_initializer='he_normal')(deconv2)
    deconv3 = add([deconv3, sc1])
    deconv3 = LeakyReLU()(deconv3)

    deconv4 = Conv2DTranspose(96, (5, 5), strides=(1, 1), activation='relu', padding='same', kernel_initializer='he_normal')(deconv3)
    deconv4 = LeakyReLU()(deconv4)

    deconv5 = Conv2DTranspose(1, (5, 5), strides=(1, 1), activation='relu', padding='same', kernel_initializer='he_normal')(deconv4)
    deconv5 = add([deconv5, inputs])
    if (use_last_activation):
        deconv5 = LeakyReLU()(deconv5) #TODO: check if this is logical - activation after adding???

    outputs = deconv5
    model = Model(inputs=[inputs], outputs=[outputs])
    return model

if __name__ == '__main__':
    red_cnn = keras_red_cnn(256, 256)
    red_cnn.summary()
