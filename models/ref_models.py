from keras.layers import *
from keras.layers import BatchNormalization as BatchNorm

from keras.models import *

from keras.regularizers import l2

def basic_dilated_residual_network():
    inputs = Input(shape=(None,None,1))

    conv1 = Conv2D(64, (5,5), activation='relu', padding='same')(inputs)
    conv1=Conv2D(64, (3,3), activation='relu', padding='same')(conv1)
    conv2 = Conv2D(64, (3, 3), padding='same',dilation_rate=(2,2))(conv1)
    conv2 = BatchNorm()(conv2)
    conv2 = Activation('relu')(conv2)

    conv3 = Conv2D(64, (3, 3), padding='same',dilation_rate=(3,3))(conv2)
    conv3 = BatchNorm()(conv3)
    conv3 = Activation('relu')(conv3)

    conv4 = Conv2D(64, (3, 3), padding='same',dilation_rate=(4,4))(conv3)
    conv4 = BatchNorm()(conv4)
    conv4 = Activation('relu')(conv4)

    conv5 = Conv2D(64, (3, 3), padding='same',dilation_rate=(3,3))(conv4)
    conv5 = BatchNorm()(conv5)
    conv5 = Activation('relu')(conv5)

    conv6 = concatenate([conv5,conv2],axis=3)
    conv6 = Conv2D(64, (3, 3), padding='same',dilation_rate=(2,2))(conv6)
    conv6 = BatchNorm()(conv6)
    conv6 = Activation('relu')(conv6)

    conv7= concatenate([conv6,conv1],axis=3)
    conv7 = Conv2D(1, (3, 3), padding='same')(conv7)

    outputs = add([conv7,inputs])
    #outputs = Lambda(lambda output: kb.clip(output, 0.0, 1.0))(outputs) # TODO: is this logical?
    model = Model(inputs=[inputs], outputs=[outputs])
    return model


"""
Standard U-Net [Ronneberger et.al, 2015]
Total params: 7,759,521
Link: https://github.com/CarryHJR/Nested-UNet/blob/master/model.py

Modified for reconstruction purposes
"""
def unet(img_rows, img_cols, color_type=1, num_class=1, use_addition=True):

    ########################################
    # 2D Standard
    ########################################
    def standard_unit(input_tensor, stage, nb_filter, kernel_size=3):

        smooth = 1.
        dropout_rate = 0.5
        act = 'elu'

        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_1',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(input_tensor)
        x = Dropout(dropout_rate, name='dp' + stage + '_1')(x)
        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_2',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(x)
        x = Dropout(dropout_rate, name='dp' + stage + '_2')(x)

        return x

    nb_filter = [32,64,128,256,512]
    act = 'elu'

    bn_axis = 3
    img_input = Input(shape=(img_rows, img_cols, color_type), name='main_input')

    conv1_1 = standard_unit(img_input, stage='11', nb_filter=nb_filter[0])
    pool1 = MaxPooling2D((2, 2), strides=(2, 2), name='pool1')(conv1_1)

    conv2_1 = standard_unit(pool1, stage='21', nb_filter=nb_filter[1])
    pool2 = MaxPooling2D((2, 2), strides=(2, 2), name='pool2')(conv2_1)

    conv3_1 = standard_unit(pool2, stage='31', nb_filter=nb_filter[2])
    pool3 = MaxPooling2D((2, 2), strides=(2, 2), name='pool3')(conv3_1)

    conv4_1 = standard_unit(pool3, stage='41', nb_filter=nb_filter[3])
    pool4 = MaxPooling2D((2, 2), strides=(2, 2), name='pool4')(conv4_1)

    conv5_1 = standard_unit(pool4, stage='51', nb_filter=nb_filter[4])

    up4_2 = Conv2DTranspose(nb_filter[3], (2, 2), strides=(2, 2), name='up42', padding='same')(conv5_1)
    conv4_2 = concatenate([up4_2, conv4_1], name='merge42', axis=bn_axis)
    conv4_2 = standard_unit(conv4_2, stage='42', nb_filter=nb_filter[3])

    up3_3 = Conv2DTranspose(nb_filter[2], (2, 2), strides=(2, 2), name='up33', padding='same')(conv4_2)
    conv3_3 = concatenate([up3_3, conv3_1], name='merge33', axis=bn_axis)
    conv3_3 = standard_unit(conv3_3, stage='33', nb_filter=nb_filter[2])

    up2_4 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up24', padding='same')(conv3_3)
    conv2_4 = concatenate([up2_4, conv2_1], name='merge24', axis=bn_axis)
    conv2_4 = standard_unit(conv2_4, stage='24', nb_filter=nb_filter[1])

    up1_5 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up15', padding='same')(conv2_4)
    conv1_5 = concatenate([up1_5, conv1_1], name='merge15', axis=bn_axis)
    conv1_5 = standard_unit(conv1_5, stage='15', nb_filter=nb_filter[0])

    unet_output = Conv2D(num_class, (1, 1), name='output', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_5)

    sum_output = add([unet_output, img_input])

    output = sum_output if use_addition else unet_output
    model = Model(input=img_input, output=[output])

    return model

"""
wU-Net for comparison
Total params: 9,282,246
Link: https://github.com/CarryHJR/Nested-UNet/blob/master/model.py

Modified for reconstruction purposes
"""
def wide_unet(img_rows, img_cols, color_type=1, num_class=1, use_addition=True):

    ########################################
    # 2D Standard
    ########################################
    def standard_unit(input_tensor, stage, nb_filter, kernel_size=3):

        smooth = 1.
        dropout_rate = 0.5
        act = 'elu'

        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_1',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(input_tensor)
        x = Dropout(dropout_rate, name='dp' + stage + '_1')(x)
        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_2',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(x)
        x = Dropout(dropout_rate, name='dp' + stage + '_2')(x)

        return x

    # nb_filter = [32,64,128,256,512]
    nb_filter = [35,70,140,280,560]
    act = 'elu'

    bn_axis = 3
    img_input = Input(shape=(img_rows, img_cols, color_type), name='main_input')

    conv1_1 = standard_unit(img_input, stage='11', nb_filter=nb_filter[0])
    pool1 = MaxPooling2D((2, 2), strides=(2, 2), name='pool1')(conv1_1)

    conv2_1 = standard_unit(pool1, stage='21', nb_filter=nb_filter[1])
    pool2 = MaxPooling2D((2, 2), strides=(2, 2), name='pool2')(conv2_1)

    conv3_1 = standard_unit(pool2, stage='31', nb_filter=nb_filter[2])
    pool3 = MaxPooling2D((2, 2), strides=(2, 2), name='pool3')(conv3_1)

    conv4_1 = standard_unit(pool3, stage='41', nb_filter=nb_filter[3])
    pool4 = MaxPooling2D((2, 2), strides=(2, 2), name='pool4')(conv4_1)

    conv5_1 = standard_unit(pool4, stage='51', nb_filter=nb_filter[4])

    up4_2 = Conv2DTranspose(nb_filter[3], (2, 2), strides=(2, 2), name='up42', padding='same')(conv5_1)
    conv4_2 = concatenate([up4_2, conv4_1], name='merge42', axis=bn_axis)
    conv4_2 = standard_unit(conv4_2, stage='42', nb_filter=nb_filter[3])

    up3_3 = Conv2DTranspose(nb_filter[2], (2, 2), strides=(2, 2), name='up33', padding='same')(conv4_2)
    conv3_3 = concatenate([up3_3, conv3_1], name='merge33', axis=bn_axis)
    conv3_3 = standard_unit(conv3_3, stage='33', nb_filter=nb_filter[2])

    up2_4 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up24', padding='same')(conv3_3)
    conv2_4 = concatenate([up2_4, conv2_1], name='merge24', axis=bn_axis)
    conv2_4 = standard_unit(conv2_4, stage='24', nb_filter=nb_filter[1])

    up1_5 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up15', padding='same')(conv2_4)
    conv1_5 = concatenate([up1_5, conv1_1], name='merge15', axis=bn_axis)
    conv1_5 = standard_unit(conv1_5, stage='15', nb_filter=nb_filter[0])

    unet_output = Conv2D(num_class, (1, 1), name='output', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_5)
    sum_output = add([unet_output, img_input])

    output = sum_output if use_addition else unet_output
    model = Model(input=img_input, output=[output])

    return model

"""
Standard UNet++ [Zhou et.al, 2018]
Total params: 9,041,601
Link: https://github.com/CarryHJR/Nested-UNet/blob/master/model.py#L243

Modified for reconstruction purposes
"""
def unet_plus_plus(img_rows,
                   img_cols,
                   color_type=1,
                   num_class=1,
                   use_addition=True,
                   # deep_supervision=False, # TODO JB: see if we can use deep-supervision in a meaningful way
                   ):

    ########################################
    # 2D Standard
    ########################################
    def standard_unit(input_tensor, stage, nb_filter, kernel_size=3):

        smooth = 1.
        dropout_rate = 0.5
        act = 'elu'

        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_1',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(input_tensor)
        x = Dropout(dropout_rate, name='dp' + stage + '_1')(x)
        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_2',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(x)
        x = Dropout(dropout_rate, name='dp' + stage + '_2')(x)

        return x

    nb_filter = [32,64,128,256,512]
    act = 'elu'

    # NOTE: we only work with TF backend
    #
    # Handle Dimension Ordering for different backends
    # global bn_axis
    # if K.image_dim_ordering() == 'tf':
    #   bn_axis = 3
    #   img_input = Input(shape=(img_rows, img_cols, color_type), name='main_input')
    # else:
    #   bn_axis = 1
    #   img_input = Input(shape=(color_type, img_rows, img_cols), name='main_input')

    bn_axis = 3
    img_input = Input(shape=(img_rows, img_cols, color_type), name='main_input')

    conv1_1 = standard_unit(img_input, stage='11', nb_filter=nb_filter[0])
    pool1 = MaxPooling2D((2, 2), strides=(2, 2), name='pool1')(conv1_1)

    conv2_1 = standard_unit(pool1, stage='21', nb_filter=nb_filter[1])
    pool2 = MaxPooling2D((2, 2), strides=(2, 2), name='pool2')(conv2_1)

    up1_2 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up12', padding='same')(conv2_1)
    conv1_2 = concatenate([up1_2, conv1_1], name='merge12', axis=bn_axis)
    conv1_2 = standard_unit(conv1_2, stage='12', nb_filter=nb_filter[0])

    conv3_1 = standard_unit(pool2, stage='31', nb_filter=nb_filter[2])
    pool3 = MaxPooling2D((2, 2), strides=(2, 2), name='pool3')(conv3_1)

    up2_2 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up22', padding='same')(conv3_1)
    conv2_2 = concatenate([up2_2, conv2_1], name='merge22', axis=bn_axis)
    conv2_2 = standard_unit(conv2_2, stage='22', nb_filter=nb_filter[1])

    up1_3 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up13', padding='same')(conv2_2)
    conv1_3 = concatenate([up1_3, conv1_1, conv1_2], name='merge13', axis=bn_axis)
    conv1_3 = standard_unit(conv1_3, stage='13', nb_filter=nb_filter[0])

    conv4_1 = standard_unit(pool3, stage='41', nb_filter=nb_filter[3])
    pool4 = MaxPooling2D((2, 2), strides=(2, 2), name='pool4')(conv4_1)

    up3_2 = Conv2DTranspose(nb_filter[2], (2, 2), strides=(2, 2), name='up32', padding='same')(conv4_1)
    conv3_2 = concatenate([up3_2, conv3_1], name='merge32', axis=bn_axis)
    conv3_2 = standard_unit(conv3_2, stage='32', nb_filter=nb_filter[2])

    up2_3 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up23', padding='same')(conv3_2)
    conv2_3 = concatenate([up2_3, conv2_1, conv2_2], name='merge23', axis=bn_axis)
    conv2_3 = standard_unit(conv2_3, stage='23', nb_filter=nb_filter[1])

    up1_4 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up14', padding='same')(conv2_3)
    conv1_4 = concatenate([up1_4, conv1_1, conv1_2, conv1_3], name='merge14', axis=bn_axis)
    conv1_4 = standard_unit(conv1_4, stage='14', nb_filter=nb_filter[0])

    conv5_1 = standard_unit(pool4, stage='51', nb_filter=nb_filter[4])

    up4_2 = Conv2DTranspose(nb_filter[3], (2, 2), strides=(2, 2), name='up42', padding='same')(conv5_1)
    conv4_2 = concatenate([up4_2, conv4_1], name='merge42', axis=bn_axis)
    conv4_2 = standard_unit(conv4_2, stage='42', nb_filter=nb_filter[3])

    up3_3 = Conv2DTranspose(nb_filter[2], (2, 2), strides=(2, 2), name='up33', padding='same')(conv4_2)
    conv3_3 = concatenate([up3_3, conv3_1, conv3_2], name='merge33', axis=bn_axis)
    conv3_3 = standard_unit(conv3_3, stage='33', nb_filter=nb_filter[2])

    up2_4 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up24', padding='same')(conv3_3)
    conv2_4 = concatenate([up2_4, conv2_1, conv2_2, conv2_3], name='merge24', axis=bn_axis)
    conv2_4 = standard_unit(conv2_4, stage='24', nb_filter=nb_filter[1])

    up1_5 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up15', padding='same')(conv2_4)
    conv1_5 = concatenate([up1_5, conv1_1, conv1_2, conv1_3, conv1_4], name='merge15', axis=bn_axis)
    conv1_5 = standard_unit(conv1_5, stage='15', nb_filter=nb_filter[0])

    # NOTE: we eliminate the sigmoid function at the end and simply ADD the last convolution to the original input
    #
    # nestnet_output_1 = Conv2D(num_class, (1, 1), activation='sigmoid', name='output_1', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_2)
    # nestnet_output_2 = Conv2D(num_class, (1, 1), activation='sigmoid', name='output_2', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_3)
    # nestnet_output_3 = Conv2D(num_class, (1, 1), activation='sigmoid', name='output_3', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_4)
    nestnet_output_4 = Conv2D(num_class, (1, 1), name='output_4', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_5)
    #
    # if deep_supervision:
    #     model = Model(input=img_input, output=[nestnet_output_1,
    #                                            nestnet_output_2,
    #                                            nestnet_output_3,
    #                                            nestnet_output_4])
    # else:
    #     model = Model(input=img_input, output=[nestnet_output_4])

    sum_output = add([nestnet_output_4, img_input])

    output = sum_output if use_addition else nestnet_output_4
    model = Model(input=img_input, output=[output])

    return model

"""
Standard U-Net [Ronneberger et.al, 2015]
Total params: 7,759,521
Link: https://github.com/CarryHJR/Nested-UNet/blob/master/model.py

Modified for reconstruction purposes, added leveling parameter ('level')
level = 5 should be equivalent to 'unet'
"""
def unet_mod_leveled(img_rows, img_cols, color_type=1, num_class=1,
                     use_addition : bool = True, level : int = 5,
                     use_parallel_dilated_conv_module : bool = False):

    assert(level >= 1)
    assert(level <= 5)

    ########################################
    # 2D Standard
    ########################################
    def standard_unit(input_tensor, stage, nb_filter, kernel_size=3):

        smooth = 1.
        dropout_rate = 0.5
        act = 'elu'

        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_1',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(input_tensor)
        x = Dropout(dropout_rate, name='dp' + stage + '_1')(x)
        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_2',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(x)
        x = Dropout(dropout_rate, name='dp' + stage + '_2')(x)

        return x

    # LINK: https://www.researchgate.net/publication/337601036_Accuracy_Improvement_of_UNet_Based_on_Dilated_Convolution
    def dilated_convolution_module(input_tensor, nb_filter, max_dilation_rate=32, kernel_size=3):
        assert (max_dilation_rate >= 1)

        act = 'relu'
        dilation_rate = 1

        x = Conv2D(nb_filter, (kernel_size, kernel_size), dilation_rate=(dilation_rate, dilation_rate),
                   kernel_initializer='he_normal', padding='same', )(input_tensor)
        x = BatchNorm()(x)
        x = Activation('relu')(x)

        while (dilation_rate < max_dilation_rate):
            x = Conv2D(nb_filter, (kernel_size, kernel_size),
                       dilation_rate=(dilation_rate, dilation_rate),
                       kernel_initializer='he_normal', padding='same', )(x)
            x = BatchNorm()(x)
            x = Activation(act)(x)
            dilation_rate *= 2
        return x

    nb_filter = [32,64,128,256,512]
    act = 'elu'

    bn_axis = 3
    img_input = Input(shape=(img_rows, img_cols, color_type), name='main_input')

    conv1_1 = standard_unit(img_input, stage='11', nb_filter=nb_filter[0])
    for_unet_output = conv1_1
    if (level >= 2):
        pool1 = MaxPooling2D((2, 2), strides=(2, 2), name='pool1')(conv1_1)

        conv2_1 = standard_unit(pool1, stage='21', nb_filter=nb_filter[1]) \
        if not use_parallel_dilated_conv_module and level == 2 else \
                dilated_convolution_module(pool1, nb_filter=nb_filter[1], max_dilation_rate=32)

        for_up_1_5 = conv2_1
        if (level >= 3):
            pool2 = MaxPooling2D((2, 2), strides=(2, 2), name='pool2')(conv2_1)

            conv3_1 = standard_unit(pool2, stage='31', nb_filter=nb_filter[2]) \
            if not use_parallel_dilated_conv_module and level == 3 else \
                    dilated_convolution_module(pool2, nb_filter=nb_filter[2], max_dilation_rate=32)

            for_up_2_4 = conv3_1
            if (level >= 4):
                pool3 = MaxPooling2D((2, 2), strides=(2, 2), name='pool3')(conv3_1)

                conv4_1 = standard_unit(pool3, stage='41', nb_filter=nb_filter[3]) \
                if not use_parallel_dilated_conv_module and level == 4 else \
                        dilated_convolution_module(pool3, nb_filter=nb_filter[3], max_dilation_rate=32)

                for_up_3_3 = conv4_1
                if (level == 5):
                    pool4 = MaxPooling2D((2, 2), strides=(2, 2), name='pool4')(conv4_1)

                    conv5_1 = standard_unit(pool4, stage='51', nb_filter=nb_filter[4]) \
                        if not use_parallel_dilated_conv_module and level == 5 else \
                        dilated_convolution_module(pool4, nb_filter=nb_filter[4], max_dilation_rate=32)

                    up4_2 = Conv2DTranspose(nb_filter[3], (2, 2), strides=(2, 2), name='up42', padding='same')(conv5_1)
                    conv4_2 = concatenate([up4_2, conv4_1], name='merge42', axis=bn_axis)
                    conv4_2 = standard_unit(conv4_2, stage='42', nb_filter=nb_filter[3])
                    for_up_3_3 = conv4_2

                up3_3 = Conv2DTranspose(nb_filter[2], (2, 2), strides=(2, 2), name='up33', padding='same')(for_up_3_3)
                conv3_3 = concatenate([up3_3, conv3_1], name='merge33', axis=bn_axis)
                conv3_3 = standard_unit(conv3_3, stage='33', nb_filter=nb_filter[2])
                for_up_2_4 = conv3_3

            up2_4 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up24', padding='same')(for_up_2_4)
            conv2_4 = concatenate([up2_4, conv2_1], name='merge24', axis=bn_axis)
            conv2_4 = standard_unit(conv2_4, stage='24', nb_filter=nb_filter[1])
            for_up_1_5 = conv2_4

        up1_5 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up15', padding='same')(for_up_1_5)
        conv1_5 = concatenate([up1_5, conv1_1], name='merge15', axis=bn_axis)
        conv1_5 = standard_unit(conv1_5, stage='15', nb_filter=nb_filter[0])
        for_unet_output = conv1_5

    unet_output = Conv2D(num_class, (1, 1), name='output', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(for_unet_output)
    sum_output = add([unet_output, img_input])

    output = sum_output if use_addition else unet_output
    model = Model(input=img_input, output=[output])

    return model

def unet_in_3d(img_rows, img_cols, img_stack_size, color_type=1, num_class=1, use_addition=True):

    ########################################
    # 2D Standard
    ########################################
    def standard_unit(input_tensor, stage, nb_filter, kernel_size=3):

        smooth = 1.
        dropout_rate = 0.5
        act = 'elu'

        x = Conv3D(nb_filter, (1, kernel_size, kernel_size), activation=act, name='conv' + stage + '_1',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(input_tensor)
        x = Dropout(dropout_rate, name='dp' + stage + '_1')(x)
        x = Conv3D(nb_filter, (1, kernel_size, kernel_size), activation=act, name='conv' + stage + '_2',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(x)
        x = Dropout(dropout_rate, name='dp' + stage + '_2')(x)

        return x

    nb_filter = [32,64,128,256,512]
    act = 'elu'

    bn_axis = 4
    img_input = Input(shape=(img_stack_size, img_rows, img_cols, color_type), name='main_input')

    conv1_1 = standard_unit(img_input, stage='11', nb_filter=nb_filter[0])
    filter_3d = (1, 2, 2)
    pool_3d = (1,2,2)
    pool1 = MaxPooling3D(pool_3d, strides=filter_3d, name='pool1')(conv1_1)

    conv2_1 = standard_unit(pool1, stage='21', nb_filter=nb_filter[1])
    pool2 = MaxPooling3D(pool_3d, strides=filter_3d, name='pool2')(conv2_1)

    conv3_1 = standard_unit(pool2, stage='31', nb_filter=nb_filter[2])
    pool3 = MaxPooling3D(pool_3d, strides=filter_3d, name='pool3')(conv3_1)

    conv4_1 = standard_unit(pool3, stage='41', nb_filter=nb_filter[3])
    pool4 = MaxPooling3D(pool_3d, strides=filter_3d, name='pool4')(conv4_1)

    conv5_1 = standard_unit(pool4, stage='51', nb_filter=nb_filter[4])

    up4_2 = Conv3DTranspose(nb_filter[3], filter_3d, strides=filter_3d, name='up42', padding='same')(conv5_1)
    conv4_2 = concatenate([up4_2, conv4_1], name='merge42', axis=bn_axis)
    conv4_2 = standard_unit(conv4_2, stage='42', nb_filter=nb_filter[3])

    up3_3 = Conv3DTranspose(nb_filter[2], filter_3d, strides=filter_3d, name='up33', padding='same')(conv4_2)
    conv3_3 = concatenate([up3_3, conv3_1], name='merge33', axis=bn_axis)
    conv3_3 = standard_unit(conv3_3, stage='33', nb_filter=nb_filter[2])

    up2_4 = Conv3DTranspose(nb_filter[1], filter_3d, strides=filter_3d, name='up24', padding='same')(conv3_3)
    conv2_4 = concatenate([up2_4, conv2_1], name='merge24', axis=bn_axis)
    conv2_4 = standard_unit(conv2_4, stage='24', nb_filter=nb_filter[1])

    up1_5 = Conv3DTranspose(nb_filter[0], filter_3d, strides=filter_3d, name='up15', padding='same')(conv2_4)
    conv1_5 = concatenate([up1_5, conv1_1], name='merge15', axis=bn_axis)
    conv1_5 = standard_unit(conv1_5, stage='15', nb_filter=nb_filter[0])

    unet_output = Conv3D(num_class, (1, 1, 1), name='output', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_5)

    sum_output = add([unet_output, img_input])

    output = sum_output if use_addition else unet_output
    model = Model(input=img_input, output=[output])

    return model


"""
"""
def unet_plus_plus_mod_leveled(img_rows,
                               img_cols,
                               color_type=1,
                               num_class=1,
                               use_addition=True,
                               level : int = 5,
                               # deep_supervision=False, # TODO JB: see if we can use deep-supervision in a meaningful way
                               ):

    assert(level >= 2)
    assert(level <= 5)

    ########################################
    # 2D Standard
    ########################################
    def standard_unit(input_tensor, stage, nb_filter, kernel_size=3):

        smooth = 1.
        dropout_rate = 0.5
        act = 'elu'

        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_1',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(input_tensor)
        x = Dropout(dropout_rate, name='dp' + stage + '_1')(x)
        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_2',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(x)
        x = Dropout(dropout_rate, name='dp' + stage + '_2')(x)

        return x

    nb_filter = [32,64,128,256,512]
    act = 'elu'

    # NOTE: we only work with TF backend
    #
    # Handle Dimension Ordering for different backends
    # global bn_axis
    # if K.image_dim_ordering() == 'tf':
    #   bn_axis = 3
    #   img_input = Input(shape=(img_rows, img_cols, color_type), name='main_input')
    # else:
    #   bn_axis = 1
    #   img_input = Input(shape=(color_type, img_rows, img_cols), name='main_input')

    bn_axis = 3
    img_input = Input(shape=(img_rows, img_cols, color_type), name='main_input')

    conv1_1 = standard_unit(img_input, stage='11', nb_filter=nb_filter[0])
    for_unet_output = conv1_1

    pool1 = MaxPooling2D((2, 2), strides=(2, 2), name='pool1')(conv1_1)

    if (level >= 2):
        conv2_1 = standard_unit(pool1, stage='21', nb_filter=nb_filter[1])
        pool2 = MaxPooling2D((2, 2), strides=(2, 2), name='pool2')(conv2_1)

        up1_2 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up12', padding='same')(conv2_1)
        conv1_2 = concatenate([up1_2, conv1_1], name='merge12', axis=bn_axis)
        conv1_2 = standard_unit(conv1_2, stage='12', nb_filter=nb_filter[0])
        for_unet_output = conv1_2

    if (level >= 3):
        conv3_1 = standard_unit(pool2, stage='31', nb_filter=nb_filter[2])
        pool3 = MaxPooling2D((2, 2), strides=(2, 2), name='pool3')(conv3_1)

        up2_2 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up22', padding='same')(conv3_1)
        conv2_2 = concatenate([up2_2, conv2_1], name='merge22', axis=bn_axis)
        conv2_2 = standard_unit(conv2_2, stage='22', nb_filter=nb_filter[1])

        up1_3 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up13', padding='same')(conv2_2)
        conv1_3 = concatenate([up1_3, conv1_1, conv1_2], name='merge13', axis=bn_axis)
        conv1_3 = standard_unit(conv1_3, stage='13', nb_filter=nb_filter[0])
        for_unet_output = conv1_3

    if (level >= 4):
        conv4_1 = standard_unit(pool3, stage='41', nb_filter=nb_filter[3])
        pool4 = MaxPooling2D((2, 2), strides=(2, 2), name='pool4')(conv4_1)

        up3_2 = Conv2DTranspose(nb_filter[2], (2, 2), strides=(2, 2), name='up32', padding='same')(conv4_1)
        conv3_2 = concatenate([up3_2, conv3_1], name='merge32', axis=bn_axis)
        conv3_2 = standard_unit(conv3_2, stage='32', nb_filter=nb_filter[2])

        up2_3 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up23', padding='same')(conv3_2)
        conv2_3 = concatenate([up2_3, conv2_1, conv2_2], name='merge23', axis=bn_axis)
        conv2_3 = standard_unit(conv2_3, stage='23', nb_filter=nb_filter[1])

        up1_4 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up14', padding='same')(conv2_3)
        conv1_4 = concatenate([up1_4, conv1_1, conv1_2, conv1_3], name='merge14', axis=bn_axis)
        conv1_4 = standard_unit(conv1_4, stage='14', nb_filter=nb_filter[0])
        for_unet_output = conv1_4

    if (level >= 5):
        conv5_1 = standard_unit(pool4, stage='51', nb_filter=nb_filter[4])

        up4_2 = Conv2DTranspose(nb_filter[3], (2, 2), strides=(2, 2), name='up42', padding='same')(conv5_1)
        conv4_2 = concatenate([up4_2, conv4_1], name='merge42', axis=bn_axis)
        conv4_2 = standard_unit(conv4_2, stage='42', nb_filter=nb_filter[3])

        up3_3 = Conv2DTranspose(nb_filter[2], (2, 2), strides=(2, 2), name='up33', padding='same')(conv4_2)
        conv3_3 = concatenate([up3_3, conv3_1, conv3_2], name='merge33', axis=bn_axis)
        conv3_3 = standard_unit(conv3_3, stage='33', nb_filter=nb_filter[2])

        up2_4 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up24', padding='same')(conv3_3)
        conv2_4 = concatenate([up2_4, conv2_1, conv2_2, conv2_3], name='merge24', axis=bn_axis)
        conv2_4 = standard_unit(conv2_4, stage='24', nb_filter=nb_filter[1])

        up1_5 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up15', padding='same')(conv2_4)
        conv1_5 = concatenate([up1_5, conv1_1, conv1_2, conv1_3, conv1_4], name='merge15', axis=bn_axis)
        conv1_5 = standard_unit(conv1_5, stage='15', nb_filter=nb_filter[0])
        for_unet_output = conv1_5

    # NOTE: we eliminate the sigmoid function at the end and simply ADD the last convolution to the original input
    #
    # nestnet_output_1 = Conv2D(num_class, (1, 1), activation='sigmoid', name='output_1', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_2)
    # nestnet_output_2 = Conv2D(num_class, (1, 1), activation='sigmoid', name='output_2', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_3)
    # nestnet_output_3 = Conv2D(num_class, (1, 1), activation='sigmoid', name='output_3', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_4)
    # nestnet_output_4 = Conv2D(num_class, (1, 1), name='output_4', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_5)
    #
    # if deep_supervision:
    #     model = Model(input=img_input, output=[nestnet_output_1,
    #                                            nestnet_output_2,
    #                                            nestnet_output_3,
    #                                            nestnet_output_4])
    # else:
    #     model = Model(input=img_input, output=[nestnet_output_4])

    nestnet_output = Conv2D(num_class, (1, 1), name='output_4', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(for_unet_output)
    sum_output = add([nestnet_output, img_input])

    output = sum_output if use_addition else for_unet_output
    model = Model(input=img_input, output=[output])

    return model

def basic_dilated_residual_network_fixed_shape(width, height):
    inputs = Input(shape=(width,height,1))

    conv1 = Conv2D(64, (5,5), activation='relu', padding='same')(inputs)
    conv1=Conv2D(64, (3,3), activation='relu', padding='same')(conv1)
    conv2 = Conv2D(64, (3, 3), padding='same',dilation_rate=(2,2))(conv1)
    conv2 = BatchNorm()(conv2)
    conv2 = Activation('relu')(conv2)

    conv3 = Conv2D(64, (3, 3), padding='same',dilation_rate=(3,3))(conv2)
    conv3 = BatchNorm()(conv3)
    conv3 = Activation('relu')(conv3)

    conv4 = Conv2D(64, (3, 3), padding='same',dilation_rate=(4,4))(conv3)
    conv4 = BatchNorm()(conv4)
    conv4 = Activation('relu')(conv4)

    conv5 = Conv2D(64, (3, 3), padding='same',dilation_rate=(3,3))(conv4)
    conv5 = BatchNorm()(conv5)
    conv5 = Activation('relu')(conv5)

    conv6 = concatenate([conv5,conv2],axis=3)
    conv6 = Conv2D(64, (3, 3), padding='same',dilation_rate=(2,2))(conv6)
    conv6 = BatchNorm()(conv6)
    conv6 = Activation('relu')(conv6)

    conv7= concatenate([conv6,conv1],axis=3)
    conv7 = Conv2D(1, (3, 3), padding='same')(conv7)

    outputs = add([conv7,inputs])
    #outputs = Lambda(lambda output: kb.clip(output, 0.0, 1.0))(outputs) # TODO: is this logical?
    model = Model(inputs=[inputs], outputs=[outputs])
    return model


"""
Standard UNet++ [Zhou et.al, 2018]
Total params: 9,041,601
Link: https://github.com/CarryHJR/Nested-UNet/blob/master/model.py#L243

Modified for reconstruction purposes
"""
def unet_plus_plus_no_pool(img_rows,
                           img_cols,
                           color_type=1,
                           num_class=1,
                           use_addition=True,
                           # deep_supervision=False, # TODO JB: see if we can use deep-supervision in a meaningful way
                           ):

    ########################################
    # 2D Standard
    ########################################
    def standard_unit(input_tensor, stage, nb_filter, kernel_size=3):

        smooth = 1.
        dropout_rate = 0.5
        act = 'elu'

        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_1',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(input_tensor)
        x = Dropout(dropout_rate, name='dp' + stage + '_1')(x)
        x = Conv2D(nb_filter, (kernel_size, kernel_size), activation=act, name='conv' + stage + '_2',
                   kernel_initializer='he_normal', padding='same', kernel_regularizer=l2(1e-4))(x)
        x = Dropout(dropout_rate, name='dp' + stage + '_2')(x)

        return x

    nb_filter = [32,64,128,256,512]
    act = 'elu'

    # NOTE: we only work with TF backend
    #
    # Handle Dimension Ordering for different backends
    # global bn_axis
    # if K.image_dim_ordering() == 'tf':
    #   bn_axis = 3
    #   img_input = Input(shape=(img_rows, img_cols, color_type), name='main_input')
    # else:
    #   bn_axis = 1
    #   img_input = Input(shape=(color_type, img_rows, img_cols), name='main_input')

    bn_axis = 3
    img_input = Input(shape=(img_rows, img_cols, color_type), name='main_input')

    conv1_1 = standard_unit(img_input, stage='11', nb_filter=nb_filter[0])
    pool1 = Conv2D(nb_filter[0], (2, 2), strides=(2, 2), name='conv_pool1')(conv1_1)

    conv2_1 = standard_unit(pool1, stage='21', nb_filter=nb_filter[1])
    pool2 = Conv2D(nb_filter[1], (2, 2), strides=(2, 2), name='pool2')(conv2_1)

    up1_2 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up12', padding='same')(conv2_1)
    conv1_2 = concatenate([up1_2, conv1_1], name='merge12', axis=bn_axis)
    conv1_2 = standard_unit(conv1_2, stage='12', nb_filter=nb_filter[0])

    conv3_1 = standard_unit(pool2, stage='31', nb_filter=nb_filter[2])
    pool3 = Conv2D(nb_filter[2], (2, 2), strides=(2, 2), name='pool3')(conv3_1)

    up2_2 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up22', padding='same')(conv3_1)
    conv2_2 = concatenate([up2_2, conv2_1], name='merge22', axis=bn_axis)
    conv2_2 = standard_unit(conv2_2, stage='22', nb_filter=nb_filter[1])

    up1_3 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up13', padding='same')(conv2_2)
    conv1_3 = concatenate([up1_3, conv1_1, conv1_2], name='merge13', axis=bn_axis)
    conv1_3 = standard_unit(conv1_3, stage='13', nb_filter=nb_filter[0])

    conv4_1 = standard_unit(pool3, stage='41', nb_filter=nb_filter[3])
    pool4 = Conv2D(nb_filter[3], (2, 2), strides=(2, 2), name='pool4')(conv4_1)

    up3_2 = Conv2DTranspose(nb_filter[2], (2, 2), strides=(2, 2), name='up32', padding='same')(conv4_1)
    conv3_2 = concatenate([up3_2, conv3_1], name='merge32', axis=bn_axis)
    conv3_2 = standard_unit(conv3_2, stage='32', nb_filter=nb_filter[2])

    up2_3 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up23', padding='same')(conv3_2)
    conv2_3 = concatenate([up2_3, conv2_1, conv2_2], name='merge23', axis=bn_axis)
    conv2_3 = standard_unit(conv2_3, stage='23', nb_filter=nb_filter[1])

    up1_4 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up14', padding='same')(conv2_3)
    conv1_4 = concatenate([up1_4, conv1_1, conv1_2, conv1_3], name='merge14', axis=bn_axis)
    conv1_4 = standard_unit(conv1_4, stage='14', nb_filter=nb_filter[0])

    conv5_1 = standard_unit(pool4, stage='51', nb_filter=nb_filter[4])

    up4_2 = Conv2DTranspose(nb_filter[3], (2, 2), strides=(2, 2), name='up42', padding='same')(conv5_1)
    conv4_2 = concatenate([up4_2, conv4_1], name='merge42', axis=bn_axis)
    conv4_2 = standard_unit(conv4_2, stage='42', nb_filter=nb_filter[3])

    up3_3 = Conv2DTranspose(nb_filter[2], (2, 2), strides=(2, 2), name='up33', padding='same')(conv4_2)
    conv3_3 = concatenate([up3_3, conv3_1, conv3_2], name='merge33', axis=bn_axis)
    conv3_3 = standard_unit(conv3_3, stage='33', nb_filter=nb_filter[2])

    up2_4 = Conv2DTranspose(nb_filter[1], (2, 2), strides=(2, 2), name='up24', padding='same')(conv3_3)
    conv2_4 = concatenate([up2_4, conv2_1, conv2_2, conv2_3], name='merge24', axis=bn_axis)
    conv2_4 = standard_unit(conv2_4, stage='24', nb_filter=nb_filter[1])

    up1_5 = Conv2DTranspose(nb_filter[0], (2, 2), strides=(2, 2), name='up15', padding='same')(conv2_4)
    conv1_5 = concatenate([up1_5, conv1_1, conv1_2, conv1_3, conv1_4], name='merge15', axis=bn_axis)
    conv1_5 = standard_unit(conv1_5, stage='15', nb_filter=nb_filter[0])

    # NOTE: we eliminate the sigmoid function at the end and simply ADD the last convolution to the original input
    #
    # nestnet_output_1 = Conv2D(num_class, (1, 1), activation='sigmoid', name='output_1', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_2)
    # nestnet_output_2 = Conv2D(num_class, (1, 1), activation='sigmoid', name='output_2', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_3)
    # nestnet_output_3 = Conv2D(num_class, (1, 1), activation='sigmoid', name='output_3', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_4)
    nestnet_output_4 = Conv2D(num_class, (1, 1), name='output_4', kernel_initializer = 'he_normal', padding='same', kernel_regularizer=l2(1e-4))(conv1_5)
    #
    # if deep_supervision:
    #     model = Model(input=img_input, output=[nestnet_output_1,
    #                                            nestnet_output_2,
    #                                            nestnet_output_3,
    #                                            nestnet_output_4])
    # else:
    #     model = Model(input=img_input, output=[nestnet_output_4])

    sum_output = add([nestnet_output_4, img_input])

    output = sum_output if use_addition else nestnet_output_4
    model = Model(input=img_input, output=[output])

    return model
