"""
Diogo Amorim, 2018-07-10
V-Net implementation in Keras 2
https://arxiv.org/pdf/1606.04797.pdf
"""

from keras.layers import *
from keras.models import Model


##https://github.com/amorimdiogo/VNet/blob/master/vnet.py

def Deconvolution3D(filters, kernel_size, output_shape, subsample, name):
    return Conv3DTranspose(filters, kernel_size, strides=subsample, name=name)

def get_act_foo(tensor):
    return ReLU()(tensor)
    # return PReLU()(tensor)

# class Deconvolution3D(Layer):
#     def __init__(self, filters, kernel_size, output_shape, subsample, name):
#         self.filters = filters
#         self.kernel_size = kernel_size
#         self.strides = (1,) + subsample + (1,)
#         self.output_shape_ = output_shape
#         self.name = name
#         assert K.backend() == 'tensorflow'
#         super(Deconvolution3D, self).__init__()
#
#     def build(self, input_shape):
#         assert len(input_shape) == 5
#         self.input_shape_ = input_shape
#         W_shape = self.kernel_size + (self.filters, input_shape[4],)
#         self.W = self.add_weight(shape=W_shape,
#                                  initializer=functools.partial(keras.initializers.glorot_uniform()),
#                                  name='{}_W'.format(self.name))
#         self.b = self.add_weight(shape=(1, 1, 1, self.filters,), initializer='zero', name='{}_b'.format(self.name))
#         self.built = True
#
#     def compute_output_shape(self, input_shape):
#         return (None,) + self.output_shape_[1:]
#
#     def call(self, x, mask=None):
#         return tf.nn.conv3d_transpose(x, self.W, output_shape=self.output_shape_,
#                                       strides=self.strides, padding='SAME', name=self.name) + self.b
#
#     def get_config(self):
#         base_config = super(Deconvolution3D, self).get_config().copy()
#         base_config['output_shape'] = self.output_shape_
#         return base_config


def downward_layer(input_layer, n_convolutions, n_output_channels, name:str, try_no_downsample : bool = False):
    inl = input_layer

    for i in range(n_convolutions):
        inl = get_act_foo(
            Conv3D(filters=(n_output_channels // 2), kernel_size=5,
                   padding='same',
                   kernel_initializer='he_normal',
                   name=f"{name}_chan_{n_output_channels // 2}_{i}")(inl)
        )

    add_l = add([inl, input_layer], name=f"{name}___add")
    KERNEL_SIZE = 2
    STRIDE_SIZE = 2 if not try_no_downsample else 1
    downsample = Conv3D(filters=n_output_channels, kernel_size=KERNEL_SIZE, strides=STRIDE_SIZE,
                        padding='same', kernel_initializer='he_normal',
                        name=f"{name}_c_{n_output_channels}_k_{KERNEL_SIZE}_s_{STRIDE_SIZE}")(add_l)
    downsample = get_act_foo(downsample)
    return downsample, add_l


def upward_layer(input0, input1, n_convolutions, n_output_channels, name, do_not_sample_first = False):
    merged = concatenate([input0, input1], axis=4)
    inl = merged
    for i in range(n_convolutions):
        n_channels = n_output_channels * 4
        inl = get_act_foo(
            Conv3D((n_channels), kernel_size=5,
                   padding='same', kernel_initializer='he_normal',
                   name=f"{name}_c_{n_channels}_k_5_i_{i}")(inl)
        )
    add_l = add([inl, merged])
    shape = add_l.get_shape().as_list()
    shape_1 = shape[1] * 2 if not do_not_sample_first else shape[1]
    first_dim = 2 if not do_not_sample_first else 1
    new_shape = (1, shape_1, shape[2] * 2, shape[3] * 2, n_output_channels)
    upsample = Deconvolution3D(n_output_channels,
                               (first_dim, 2, 2),
                               new_shape,
                               subsample=(first_dim, 2, 2),
                               name=f"{name}_c_{n_output_channels}_k_{first_dim}_2_2_s_{first_dim}_2_2")(add_l)
    return get_act_foo(upsample)


def vnet(input_size=(4,256,256,1)):
         # loss='categorical_crossentropy', metrics=['categorical_accuracy']):

    # Layer 1
    inputs = Input(input_size)
    conv1 = Conv3D(32, kernel_size=5, strides=1, padding='same', kernel_initializer='he_normal',
                   name="conv1_c_32_k_5_s_1")(inputs)
    conv1 = get_act_foo(conv1)
    repeat1 = concatenate(32 * [inputs], axis=-1)
    add1 = add([conv1, repeat1])
    down1 = Conv3D(64, 2, strides=(1, 2, 2), padding='same', kernel_initializer='he_normal', name="down1_c_64_k_2_s_1_2_2")(add1)
    down1 = get_act_foo(down1)

    # Layer 2,3,4
    down2, add2 = downward_layer(down1, 2, 128, "down2_add2")
    down3, add3 = downward_layer(down2, 2, 256, "down3_add3")
    # down4, add4 = downward_layer(down3, 3, 256, "down4_add4")

    # Layer 5
    # !Mudar kernel_size=(5, 5, 5) quando imagem > 64!
    # conv_5_1 = Conv3D(256, kernel_size=(5, 5, 5), padding='same', kernel_initializer='he_normal')(down4)
    # conv_5_1 = PReLU()(conv_5_1)
    conv_5_2 = Conv3D(128, kernel_size=(5, 5, 5), padding='same', kernel_initializer='he_normal')(down3)
    conv_5_2 = get_act_foo(conv_5_2)
    conv_5_3 = Conv3D(256, kernel_size=(5, 5, 5), padding='same', kernel_initializer='he_normal')(conv_5_2)
    conv_5_3 = get_act_foo(conv_5_3)
    add5 = add([conv_5_3, down3])
    aux_shape = add5.get_shape()

    ### TODO: changed filter size from 222 to 122
    deconv_n = 128
    upsample_5 = Deconvolution3D(deconv_n,
                                 (1, 2, 2),
                                 (1, aux_shape[1].value*2,aux_shape[2].value*2,
                                                  aux_shape[3].value*2, deconv_n),
                                 subsample=(2, 2, 2),
                                 name=f"upsample_5_c_{deconv_n}_f_1_2_2_s_2_2_2")(add5)
    upsample_5 = get_act_foo(upsample_5)

    # # Layer 6,7,8
    # # upsample_6 = upward_layer(upsample_5, add4, 3, 64)
    upsample_7 = upward_layer(upsample_5, add3, 2, 64, name="upsample_7")
    upsample_8 = upward_layer(upsample_7, add2, 2, 32, name="upsample_8", do_not_sample_first=True)
    #
    # # Layer 9
    merged_9 = concatenate([upsample_8, add1], axis=4)
    conv_9_1 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', kernel_initializer='he_normal',
                      name=f"conv_9_1_c_{64}_k_5_5_5")(merged_9)
    conv_9_1 = get_act_foo(conv_9_1)
    add_9 = add([conv_9_1, merged_9])
    # conv_9_2 = Conv3D(1, kernel_size=(1, 1, 1), padding='same', kernel_initializer='he_normal')(add_9)
    conv_9_2 = Conv3D(1, kernel_size=(1, 1, 1), padding='same', kernel_initializer='he_normal',
                      name=f"conv_9_2_c_1_k_1_1_1")(add_9)
    # conv_9_2 = get_act_foo(conv_9_2)

    sum_output = add([conv_9_2, inputs])

    # # TODO NOTE: REMOVED SIGMOID ACTIVATION HERE AND PERFORMED ADDITION WITH INPUT
    # # softmax = Softmax()(conv_9_2)
    # # sigmoid = Conv3D(1, kernel_size=(1, 1, 1), padding='same', kernel_initializer='he_normal',
    # #                  activation='sigmoid')(conv_9_2)

    model = Model(inputs=inputs, outputs=[sum_output])
    # model = Model(inputs=inputs, outputs=softmax)
    # model.compile(optimizer, loss, metrics) # WE PERFORM COMPILE OUTSIDE OF MODEL DEF

    return model

if __name__ == '__main__':
    model = vnet()
    model.summary()