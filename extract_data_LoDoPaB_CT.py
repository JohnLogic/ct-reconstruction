import h5py
import matplotlib.image as mpimg  # used for saving
import matplotlib.pyplot as plt

if __name__ == '__main__':
    filename = "./data/lodopab-h5/ground_truth_test_000.hdf5"
    h5f = h5py.File(filename, 'r')
    dataset = h5f['data']
    plt.style.use('grayscale')
    a1 = dataset[0]
    mpimg.imsave('image.png', a1)



