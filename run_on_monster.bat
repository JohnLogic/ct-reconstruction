@echo OFF
set EXPERIMENT_DATA_FOLDER=F:\Jono\ct-reconstruction-experiments
set DATASET_FOLDER=F:\Jono\data\prepared_mayo_experiment_data_other_split_truncated
set MODEL_NAME=unet
set BATCH_SIZE=16
set MAX_EPOCHS=25
set LOSS_FUNCTION_NAME=gssim
set EXPERIMENT_NAME=test_laurynas_pipeline
set OUTPUT_DIRECTORY=%EXPERIMENT_DATA_FOLDER%
set EPOCH_SAVE_INTERVAL=10
set RESOLUTION=256
set EMAIL_ADDRESS=jbrusokas@gmail.com
set OPTIMIZER="adam"

@echo | python train.py --model %MODEL_NAME% --datadir %DATASET_FOLDER% --batchsize %BATCH_SIZE% --epochs %MAX_EPOCHS% --lossfoo %LOSS_FUNCTION_NAME% --name %EXPERIMENT_NAME% --outputdir %OUTPUT_DIRECTORY% --epochsaveinterval %EPOCH_SAVE_INTERVAL% --resolution %RESOLUTION% --emailaddress %EMAIL_ADDRESS% --optimizer %OPTIMIZER%
@echo Done.