import math

import keras.losses
import keras.optimizers

import metrics as Met
from models import ref_models as ModRef
from utils import *


# def try_data_prep():
#     # 1. Input (generator)
#     # 2. Model
#     # 3. objective function (SSIM)
#
#
#     dcm_full_dose_image_dir = "C:\\Users\\Lenovo\\CT\\L067\\full_3mm"
#     dcm_low_dose_image_dir = "C:\\Users\\Lenovo\\CT\\L067\\quarter_3mm"
#
#     output_folder = os.path.join('.', 'output', 'proper_data_L067')
#     full_dose_output_folder = os.path.join(output_folder, "full_dose")
#     low_dose_output_folder = os.path.join(output_folder, "low_dose")
#
#     image_pair_dirs = Prep.fetch_image_pair_dirs(dcm_full_dose_image_dir, dcm_low_dose_image_dir, ['IMA', 'dcm'])
#     dataset = Prep.prepare_ct_dataset(image_pair_dirs, window_level=50, window_width=400,
#                                       output_dir=full_dose_output_folder,
#                                       separate_low_dose_output_dir=low_dose_output_folder,
#                                       ensure_create_dirs=True)

def try_pipeline_on_laptop_new():

    formatted_datetime_now = DateUtils.formatted_datetime_now()

    EXPERIMENT_NAME = "test-experiment"

    VERBOSE_MODE = 1

    DRIVE_FOLDER_ROOT = '/drive/My Drive/Data/training_results'
    MODEL_OUTPUT_FOLDER = FileUtils.create_dir(
        os.path.join(DRIVE_FOLDER_ROOT, f'{EXPERIMENT_NAME}_{formatted_datetime_now}'))

    # EXPERIMENT_DATA_FOLDER = '/drive/My Drive/Data/new/prepared_mayo_experiment_data' #NOTE: do not use this
    # HOME_DIR = str(Path.home())
    # EXPERIMENT_DATA_FOLDER = os.path.join(HOME_DIR, 'prepared_mayo_experiment_data_other')
    EXPERIMENT_DATA_FOLDER = FileUtils.apth("C:\\Users\\Lenovo\\repos\\ct-reconstruction\\output\\prepared_mayo_experiment_data_other_split")

    FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'full_dose')
    LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'low_dose')
    TEST_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'full_dose')
    TEST_LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'low_dose')

    SAVE_EVERY_N_EPOCHS = 5
    EPOCH_COUNT = 6
    BATCH_SIZE = 16  # 16 / 32 / 64 / 128
    TARGET_SIZE = (256, 256)  # TODO: this is VERY small, test something else later
    INTERPOLATION_METHOD = "nearest"  # TODO: "nearest", "bilinear", and "bicubic"

    intermediate_dirs = (FileUtils.create_dir(FileUtils.apth(f"./output/generated_imgs/{formatted_datetime_now}/full")),
                         FileUtils.create_dir(FileUtils.apth(f"./output/generated_imgs/{formatted_datetime_now}/low")))

    flow_param_dictionary = dict(
        image_config=dict(
            # featurewise_center=True,
            # featurewise_std_normalization=True,
            horizontal_flip=True,
            rescale=1. / 255,
        ),
        fit_smpl_size=1,
        # directory = FULL_DOSE_DATA_DIR,
        target_size=TARGET_SIZE,
        batch_size=BATCH_SIZE,
        shuffle=False,
        seed=1,
        interpolation_method=INTERPOLATION_METHOD,
        # save_to_dir = FileUtils.create_dir(FileUtils.apth(INTERMEDIATE_DIR))
    )

    full_dose_param_dictionary = flow_param_dictionary.copy()
    full_dose_param_dictionary['directory'] = FULL_DOSE_DATA_DIR
    # full_dose_param_dictionary['save_to_dir'] = intermediate_dirs[0]

    low_dose_param_dictionary = flow_param_dictionary.copy()
    low_dose_param_dictionary['directory'] = LOW_DOSE_DATA_DIR
    # low_dose_param_dictionary['save_to_dir'] = intermediate_dirs[1]

    # NOTE: ** passes dict as param values to given function
    full_dose_train_flow = KerasUtils.get_img_fit_flow(**full_dose_param_dictionary)
    low_dose_train_flow = KerasUtils.get_img_fit_flow(**low_dose_param_dictionary)

    assert (full_dose_train_flow.n == low_dose_train_flow.n)
    assert (full_dose_train_flow.n != 0)
    assert (low_dose_train_flow.n != 0)

    num_of_pairs = full_dose_train_flow.n

    low_full_train_generator = zip(low_dose_train_flow, full_dose_train_flow)

    ### -- Validation --

    VAL_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'val_full_dose')
    VAL_LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'val_low_dose')

    val_flow_param_dictionary = flow_param_dictionary.copy()
    val_flow_param_dictionary['image_config'] = dict(
        rescale=1. / 255,
    )

    val_full_dose_param_dictionary = val_flow_param_dictionary.copy()
    val_full_dose_param_dictionary['directory'] = VAL_FULL_DOSE_DATA_DIR
    # val_full_dose_param_dictionary['batch_size'] = 1
    # full_dose_param_dictionary['save_to_dir'] = intermediate_dirs[0]

    val_low_dose_param_dictionary = val_flow_param_dictionary.copy()
    val_low_dose_param_dictionary['directory'] = VAL_LOW_DOSE_DATA_DIR
    # val_low_dose_param_dictionary['batch_size'] = 1

    # NOTE: ** passes dict as param values to given function
    full_dose_val_flow = KerasUtils.get_img_fit_flow(**val_full_dose_param_dictionary)
    low_dose_val_flow = KerasUtils.get_img_fit_flow(**val_low_dose_param_dictionary)
    assert(full_dose_val_flow.n != 0)
    assert(low_dose_val_flow.n != 0)
    assert(full_dose_val_flow.n == low_dose_val_flow.n)
    low_full_val_generator = zip(low_dose_val_flow, full_dose_val_flow)
    num_of_val_pairs = full_dose_val_flow.n
    ###

    model = ModRef.basic_dilated_residual_network()
    adam_optimizer = keras.optimizers.adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer='Adam',
                  loss=keras.losses.mean_squared_error,
                  metrics=[keras.losses.mean_absolute_percentage_error, Met.dssim_metric, Met.root_mean_squared_error])

    # earlystopper = kcb.EarlyStopping(patience=3, verbose=1)
    # model_checkpointer = kcb.ModelCheckpoint(
    #     os.path.join(MODEL_OUTPUT_FOLDER, f"test-model-{DateUtils.formatted_datetime_now()}.h5")
    #     , verbose=1, save_best_only=True)

    csv_logger = keras.callbacks.CSVLogger(os.path.join(MODEL_OUTPUT_FOLDER, f"{EXPERIMENT_NAME}_{formatted_datetime_now}_log.csv"))
    epoch_saver = KerasUtils.EpochSaver(SAVE_EVERY_N_EPOCHS, MODEL_OUTPUT_FOLDER + f"/{EXPERIMENT_NAME}",
                                        save_last_only=False)

    model.fit_generator(low_full_train_generator, epochs=EPOCH_COUNT,
        # As per recommendation in Keras docs AND : https://datascience.stackexchange.com/questions/47405/what-to-set-in-steps-per-epoch-in-keras-fit-generator
        # steps_per_epoch=1, #TODO: fix
        steps_per_epoch = math.ceil(num_of_pairs / BATCH_SIZE),
        verbose = VERBOSE_MODE,
        validation_data=low_full_val_generator,
        validation_steps=math.ceil(num_of_val_pairs / BATCH_SIZE),
        callbacks = [epoch_saver, csv_logger],
        )

def try_pipeline_on_laptop():
    VERBOSE_MODE = 1

    FULL_DOSE_DATA_DIR = FileUtils.apth('./output/subset_proper_data_L067/full_dose')
    LOW_DOSE_DATA_DIR = FileUtils.apth('./output/subset_proper_data_L067/low_dose')

    TEST_FULL_DOSE_DATA_DIR = FileUtils.apth('./output/subset_test_proper_data_L067/full_dose')
    TEST_LOW_DOSE_DATA_DIR = FileUtils.apth('./output/subset_test_proper_data_L067/low_dose')

    EPOCH_COUNT = 5
    BATCH_SIZE = 16 # 16 / 32 / 64 / 128
    TARGET_SIZE = (64, 64) # TODO: this is VERY small, test something else later
    INTERPOLATION_METHOD = "nearest" # TODO: "nearest", "bilinear", and "bicubic"

    formatted_datetime_now = DateUtils.formatted_datetime_now()
    intermediate_dirs = (FileUtils.create_dir(FileUtils.apth(f"./output/generated_imgs/{formatted_datetime_now}/full")),
                            FileUtils.create_dir(FileUtils.apth(f"./output/generated_imgs/{formatted_datetime_now}/low")))

    flow_param_dictionary = dict(
        image_config = dict(
            # featurewise_center=True,
            # featurewise_std_normalization=True,
            # horizontal_flip=True, # TODO DO NOT USE
            rescale=1./255,
        ),
        fit_smpl_size = 1,
        # directory = FULL_DOSE_DATA_DIR,
        target_size = TARGET_SIZE,
        batch_size = BATCH_SIZE,
        shuffle = False,
        seed = 1,
        interpolation_method = INTERPOLATION_METHOD,
        # save_to_dir = FileUtils.create_dir(FileUtils.apth(INTERMEDIATE_DIR))
    )

    full_dose_param_dictionary = flow_param_dictionary.copy()
    full_dose_param_dictionary['directory'] = FULL_DOSE_DATA_DIR
    # full_dose_param_dictionary['save_to_dir'] = intermediate_dirs[0]

    low_dose_param_dictionary = flow_param_dictionary.copy()
    low_dose_param_dictionary['directory'] = LOW_DOSE_DATA_DIR
    # low_dose_param_dictionary['save_to_dir'] = intermediate_dirs[1]

    # NOTE: ** passes dict as param values to given function
    full_dose_train_flow = KerasUtils.get_img_fit_flow(**full_dose_param_dictionary)
    low_dose_train_flow = KerasUtils.get_img_fit_flow(**low_dose_param_dictionary)

    assert(full_dose_train_flow.n == low_dose_train_flow.n)
    num_of_pairs = full_dose_train_flow.n

    # input_gen = image_data_gen.flow_from_directory(FULL_DOSE_DATA_DIR,
    #                                                color_mode='grayscale', # ->"grayscale"<-, "rgb", "rgba"
    #                                                TARGET_SIZE=TARGET_SIZE,  #P
    #                                                BATCH_SIZE=BATCH_SIZE,  #P
    #                                                # "categorical", "binary", "sparse", "input", or ->None<-
    #                                                class_mode='input',  #TODO: see if None or 'input'
    #                                                interpolation=INTERPOLATION_METHOD,
    #                                                save_to_dir=FileUtils.create_dir(FileUtils.apth(INTERMEDIATE_DIR))
    #                                                )

    low_full_train_generator = zip(low_dose_train_flow, full_dose_train_flow)

    model = ModRef.basic_dilated_residual_network()
    adam_optimizer = keras.optimizers.adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam_optimizer,
                  loss=keras.losses.mean_squared_error,
                  metrics=[keras.losses.mean_absolute_percentage_error, Met.dssim_metric, Met.root_mean_squared_error])

    # earlystopper = kcb.EarlyStopping(patience=3, verbose=1)
    MODEL_OUTPUT_FOLDER = FileUtils.create_dir('./output/trained_models')
    # model_checkpointer = kcb.ModelCheckpoint(
    #     os.path.join(MODEL_OUTPUT_FOLDER, f"test-model-{DateUtils.formatted_datetime_now()}.h5")
    #     , verbose=1, save_best_only=True)
    epoch_saver = KerasUtils.EpochSaver(1, MODEL_OUTPUT_FOLDER+"/test-model", save_last_only=True)

    model.fit_generator(low_full_train_generator, epochs=EPOCH_COUNT,
                        # As per recommendation in Keras docs AND : https://datascience.stackexchange.com/questions/47405/what-to-set-in-steps-per-epoch-in-keras-fit-generator
                        # steps_per_epoch=1, #TODO: fix
                        steps_per_epoch=math.ceil(num_of_pairs / BATCH_SIZE),
                        verbose=VERBOSE_MODE,
                        callbacks=[epoch_saver]
                        )



    # TODO: model.fit_generator
    # model.fit_generator(training_ts_gen, epochs=opts['EPOCHS'], verbose=opts['VERBOSE_MODE'],callbacks=[epoch_saver_callback])

    # TODO: Test generator -- test_gen (+) model.predict_generator(test_gen)
    # testing_ts_gen = TimeseriesGenerator(test_X.values, test_y.values, opts['PAST_TIME_STEP_COUNT'],
    #                                      BATCH_SIZE=opts['BATCH_SIZE'])
    # predictions = model.predict_generator(testing_ts_gen, verbose=opts['VERBOSE_MODE'])

    #### --- [ TESTING PERFORMANCE ] --- ####

    test_full_dose_param_dictionary = flow_param_dictionary.copy()
    test_full_dose_param_dictionary['directory'] = TEST_FULL_DOSE_DATA_DIR

    test_low_dose_param_dictionary = flow_param_dictionary.copy()
    test_low_dose_param_dictionary['directory'] = TEST_LOW_DOSE_DATA_DIR

    test_full_dose_train_flow = KerasUtils.get_img_fit_flow(**test_full_dose_param_dictionary)
    test_low_dose_train_flow = KerasUtils.get_img_fit_flow(**test_low_dose_param_dictionary)

    test_low_full_train_generator = zip(test_low_dose_train_flow, test_full_dose_train_flow)

    predictions = model.predict_generator(test_low_dose_train_flow, steps=1)
    full_dose_images = test_full_dose_train_flow.next()

    # for idx in range(len(predictions[::])):
    #     full_dose_image = full_dose_images[idx]
    #     predicted_image = predictions[idx]
    #     # predictions will contain (x,y,1) shaped np.arrays - need to remove the '1' dim in order to use plt.imshow
    #     ImageUtils.show_2_ct_images(full_dose_image, predicted_image, "full dose", "denoised")

    return model

# def try_different_hu_windows():
#     print(f"TF executing eagerly: {tf.executing_eagerly()}")
#     dcm_image_hu_array = DU.get_dcm_image_hu_array(DU.TEST_EXAMPLE_DCM_IMAGE)
#     windowed1 = DU.window_hu_image(dcm_image_hu_array, 100, 200)
#     windowed2 = DU.window_hu_image(dcm_image_hu_array, 100, 100)
#     ssim_results = Met.dssim_metric(windowed1, windowed2)
#
# def try_generate_pngs_from_dcms():
#
#     prepared_image_prefix = "test_"
#
#     test_output_dir = DU.OUTPUT_DATA_PATH
#     test_output_path = Path(test_output_dir).joinpath('dcm-pngs')
#     test_output_path.mkdir(parents=True, exist_ok=True) # make sure path exists
#
#     for image_file_path in test_output_path.iterdir():
#         if (image_file_path.stem.startswith(prepared_image_prefix)):
#             image_file_path.unlink()
#
#     pre_listdir = os.listdir(test_output_path)
#     pairs = Prep.fetch_image_pair_dirs(file_extensions=['IMA'])
#     dataset = Prep.prepare_ct_dataset(pairs, window_level=50, window_width=400, output_dir=test_output_path,
#                                       prepared_full_dose_image_prefix=prepared_image_prefix)
#     post_listdir = os.listdir(test_output_path)
#
#     return dataset
#
# def try_generator_code():
#     low_dose_data_dir = 'C:\\Users\\Lenovo\\repos\\ct-reconstruction\\output\\proper_data_L067\\low_dose'
#     full_dose_data_dir = 'C:\\Users\\Lenovo\\repos\\ct-reconstruction\\output\\proper_data_L067\\'
#
#     # TODO: consider rescaling
#     image_data_gen = ImageDataGenerator(featurewise_center=True, featurewise_std_normalization=True,
#                                         horizontal_flip=True)
#
#     image_data_directory = 'data/train'  # TODO
#     batch_size = 32
#     target_size = (128, 128)
#     interpolation_method = "nearest"  # TODO: "nearest", "bilinear", and "bicubic"
#
#     input_gen = image_data_gen.flow_from_directory(full_dose_data_dir,
#                                                    color_mode='grayscale',  # ->"grayscale"<-, "rgb", "rgba"
#                                                    target_size=target_size,  # P
#                                                    batch_size=batch_size,  # P
#                                                    # "categorical", "binary", "sparse", "input", or ->None<-
#                                                    class_mode='input',  # TODO: see if None or 'input'
#                                                    interpolation=interpolation_method,
#                                                    save_to_dir=FileUtils.create_dir(FileUtils.apth("./tmp"))
#                                                    )
#

# --- Run / 'main' ---
if __name__ == '__main__':
    print(">>> Loaded runner <<<")
    # try_pipeline_on_laptop()
    try_pipeline_on_laptop_new()
