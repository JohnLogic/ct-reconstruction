# %%%

import tensorflow as tf
tf.test.gpu_device_name()

# %%%
import math
import argparse

from models import ref_models as ModRef
from models import vnet_model as VNetRef
from models import red_cnn_model as RedRef
from utils import *
import metrics as Met
import old_repo.sendy as sendy

formatted_datetime_now = DateUtils.formatted_datetime_now()

def load_model_from_file(model_path: str):
    # TODO: when adding new metrics, remember to add here!
    model = keras.models.load_model(model_path, compile=True, custom_objects={
        "custom_tf_gssim": Met.custom_tf_gssim,
        "root_mean_squared_error": Met.root_mean_squared_error,
        "dssim_metric": Met.dssim_metric,
        "msssim_metric": Met.msssim_metric,
        "custom_tf_ssim": Met.custom_tf_ssim,
        "psnr": Met.psnr,
        "mean_split_4_gssim": Met.mean_split_4_gssim,
        "sum_split_4_gssim": Met.sum_split_4_gssim,
        "mean_split_16_gssim": Met.mean_split_16_gssim,
        "max_split_16_gssim": Met.max_split_16_gssim,
        "sum_split_16_gssim": Met.sum_split_16_gssim,
        "mean_split_64_gssim": Met.mean_split_64_gssim,
        "sum_split_64_gssim": Met.sum_split_64_gssim,
        "mean_top_3_in_4_split_16_gssim": Met.mean_top_3_in_4_split_16_gssim,
        "mean_top_3_in_4_split_64_gssim": Met.mean_top_3_in_4_split_64_gssim,
        "old_slssim_4": Met.old_slssim_4,
        "old_slssim_8": Met.old_slssim_8,
        "experiment_metric" : Met.experiment_metric,
        "experiment_metric_16" : Met.experiment_metric_16,
        "slgssim_8": Met.slgssim_8,
        "slgssim_4": Met.slgssim_4,
        "three_dimensional_gssim": Met.three_dimensional_gssim,
        "three_dimensional_mse": Met.three_dimensional_mse,
        "three_dimensional_experimental_gssim": Met.three_dimensional_experimental_gssim,
        "three_dimensional_gssim_proper_only": Met.three_dimensional_gssim_proper_only,
        "three_dimensional_experimental_gssim_proper_only": Met.three_dimensional_experimental_gssim_proper_only,
        "three_dimensional_experimental_ssim_proper_only": Met.three_dimensional_experimental_ssim_proper_only,
        "three_dimensional_experimental_mse_proper_only": Met.three_dimensional_experimental_mse_proper_only,
        "three_dimensional_experimental_split_plus_gssim": Met.three_dimensional_experimental_split_plus_gssim,
        "three_dimensional_experimental_split_plus_gssim_proper_only": Met.three_dimensional_experimental_split_plus_gssim_proper_only,
        "three_dimensional_mean_top_3_in_4_split_64_gssim": Met.three_dimensional_mean_top_3_in_4_split_64_gssim,
        "three_dimensional_mean_top_3_in_4_split_64_gssim_proper_only": Met.three_dimensional_mean_top_3_in_4_split_64_gssim_proper_only,
        "three_dimensional_experimental_split_plus_gssim_with_mse": Met.three_dimensional_experimental_split_plus_gssim_with_mse,
        "three_dimensional_experimental_split_plus_gssim_proper_only_with_rmse": Met.three_dimensional_experimental_split_plus_gssim_proper_only_with_rmse,

    })
    return model

if __name__ == '__main__':
    # %%%
    # Prepared experiment data directory

    ap = argparse.ArgumentParser(description="This script runs training of neural networks for Low-dose CT image reconstruction tasks")
    ap.add_argument("-m", "--model", required=True,
        help="model to train")
    ap.add_argument("-d", "--datadir", required=True,
        help="path to dataset")
    ap.add_argument("-b", "--batchsize", required=True,
        help="selected training batch size")
    ap.add_argument("-e", "--epochs", required=True,
        help="number of epochs (MAX if early stopping) for training")
    ap.add_argument("-l", "--lossfoo", required=True,
        help="loss function used for training")
    ap.add_argument("-n", "--name", required=False,
        help="fixed experiment name")
    ap.add_argument("-o", "--outputdir", required=False,
        help="experiment output folder (for storing models, logs, etc.)")
    ap.add_argument("-r", "--resolution", required=True,
                    help="Resolution of input and output images (resolution)x(resolution)")
    ap.add_argument("-i", "--epochsaveinterval", required=False,
                    help="number of epochs between saves")
    ap.add_argument("-a", "--emailaddress", required=True,
                    help="Training completed notification email address")
    ap.add_argument("-t", "--use_tpu", required=False,
                    help="To enable the TPU")
    ap.add_argument("-p", "--optimizer", required=True,
                    help="Select which optimizer to use (default one is 'adam')")
    ap.add_argument("-ctm", "--continue_train_model", required=False,
                    help="Path to model which should be continued to trained")
    ap.add_argument("-cte", "--continue_train_model_initial_epoch", required=False,
                    help="Initial epoch for the model which should be continued to be trained")
    args = vars(ap.parse_args())

    ARG_MODEL_NAME = args["model"]
    ARG_DATA_DIR = args["datadir"]
    ARG_BATCH_SIZE = args["batchsize"]
    ARG_EPOCHS = args["epochs"]
    ARG_LOSS_NAME = args["lossfoo"]
    ARG_EXPERIMENT_NAME = args["name"]
    ARG_OUTPUT_DIR = args["outputdir"]
    ARG_EPOCH_SAVE_INTERVAL = args["epochsaveinterval"]
    ARG_RESOLUTION = args["resolution"]
    ARG_RECIPIENT_EMAIL = args["emailaddress"]
    ARG_TPU = args["use_tpu"]
    ARG_OPTIMIZER_NAME = args["optimizer"]
    ARG_CONTINUE_TRAIN_MODEL = args["continue_train_model"]
    ARG_CONTINUE_TRAIN_MODEL_EPOCH = args["continue_train_model_initial_epoch"]

    if ARG_TPU:
        # ref: https://www.tensorflow.org/guide/tpu
        resolver = tf.distribute.cluster_resolver.TPUClusterResolver()
        tf.config.experimental_connect_to_cluster(resolver)
        # This is the TPU initialization code that has to be at the beginning.
        tf.tpu.experimental.initialize_tpu_system(resolver)
        strategy = tf.distribute.experimental.TPUStrategy(resolver)


    IS_3D = False

    EXPERIMENT_DATA_FOLDER = ARG_DATA_DIR

    # ARCHIVE_FILE_NAME = "prepared_mayo_experiment_data_other_split_with_test" # NOTE: we do not extract archive within python
    # DRIVE_FOLDER_ROOT = '/drive/My Drive/Data/training_results'
    # DATASET_NAME = "prepared_mayo_experiment_data_other_split"
    VERBOSE_MODE = 1
    EXPERIMENT_NAME = ARG_EXPERIMENT_NAME if ARG_EXPERIMENT_NAME is not None else "experiment"
    if (ARG_EXPERIMENT_NAME is None):
        print("Argument 'name' not given. Default 'name' used! = "+EXPERIMENT_NAME)

    # HOME_DIR = str(Path.home()) # gets '.' path
    ALTERNATIVE_EXPERIMENT_OUTPUT_PATH = os.path.join(".", "output", "experiment_results")
    EXPERIMENT_OUTPUT_PATH = ARG_OUTPUT_DIR if ARG_OUTPUT_DIR is not None else ALTERNATIVE_EXPERIMENT_OUTPUT_PATH
    if (ARG_OUTPUT_DIR is None):
        print("Argument 'outputdir' not given. Default 'outputdir' used! = "+EXPERIMENT_OUTPUT_PATH)


    FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'full_dose')
    LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'low_dose')
    TEST_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'full_dose')
    TEST_LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'low_dose')

    SAVE_EVERY_N_EPOCHS = int(ARG_EPOCH_SAVE_INTERVAL) if ARG_EPOCH_SAVE_INTERVAL is not None else 50
    EPOCH_COUNT = int(ARG_EPOCHS)
    BATCH_SIZE = int(ARG_BATCH_SIZE) # 16 / 32 / 64 / 128

    IMAGE_RESOLUTION = int(ARG_RESOLUTION) if ARG_RESOLUTION is not None else 256
    TARGET_SIZE = (IMAGE_RESOLUTION, IMAGE_RESOLUTION)
    INTERPOLATION_METHOD = "nearest"

    # MODEL MAP
    model_map = {
        # TODO / NOTE: all "3d" models have to have '3d in their name to activate all the proper data loaders and stuff
        "unet" : ModRef.unet(img_cols = TARGET_SIZE[0], img_rows = TARGET_SIZE[1]),
        "wide_unet" : ModRef.wide_unet(img_cols = TARGET_SIZE[0], img_rows = TARGET_SIZE[1]),
        "unet_plus_plus" : ModRef.unet_plus_plus(img_cols = TARGET_SIZE[0], img_rows = TARGET_SIZE[1]),
        "basic_dilated_resnet" : ModRef.basic_dilated_residual_network(),
        "unet_level_4" : ModRef.unet_mod_leveled(img_cols = TARGET_SIZE[0], img_rows = TARGET_SIZE[1], level=4),
        "unet_level_3" : ModRef.unet_mod_leveled(img_cols = TARGET_SIZE[0], img_rows = TARGET_SIZE[1], level=3),
        "unet_level_2" : ModRef.unet_mod_leveled(img_cols = TARGET_SIZE[0], img_rows = TARGET_SIZE[1], level=2),
        "unet_plus_plus_level_4": ModRef.unet_plus_plus_mod_leveled(img_cols=TARGET_SIZE[0], img_rows=TARGET_SIZE[1], level=4),
        "unet_plus_plus_level_3": ModRef.unet_plus_plus_mod_leveled(img_cols=TARGET_SIZE[0], img_rows=TARGET_SIZE[1], level=3),
        "unet_plus_plus_level_2": ModRef.unet_plus_plus_mod_leveled(img_cols=TARGET_SIZE[0], img_rows=TARGET_SIZE[1], level=2),
        "unet_with_dilation_module_level_3" : ModRef.unet_mod_leveled(img_cols = TARGET_SIZE[0], img_rows = TARGET_SIZE[1], use_parallel_dilated_conv_module=True, level=3),
        "unet_with_dilation_module_level_4" : ModRef.unet_mod_leveled(img_cols = TARGET_SIZE[0], img_rows = TARGET_SIZE[1], use_parallel_dilated_conv_module=True, level=4),
        "unet_with_dilation_module" : ModRef.unet_mod_leveled(img_cols = TARGET_SIZE[0], img_rows = TARGET_SIZE[1], use_parallel_dilated_conv_module=True),
        "unet_3d_beta" : ModRef.unet_in_3d(img_cols=TARGET_SIZE[0], img_rows=TARGET_SIZE[1], img_stack_size=4),
        "vnet_3d": VNetRef.vnet(input_size=(4, TARGET_SIZE[0], TARGET_SIZE[1], 1)),
        "red_cnn": RedRef.keras_red_cnn(TARGET_SIZE[0], TARGET_SIZE[1], use_last_activation=True),
        "red_cnn_no_act": RedRef.keras_red_cnn(TARGET_SIZE[0], TARGET_SIZE[1], use_last_activation=False),
        "unet_plus_plus_no_pool": ModRef.unet_plus_plus_no_pool(TARGET_SIZE[0], TARGET_SIZE[1]),
    }
    model = model_map.get(ARG_MODEL_NAME)
    if (model is None):
        str_list_of_available_models = ", ".join(list(model_map.keys()))
        raise ValueError('Model with name "' + ARG_MODEL_NAME +'" is not defined!\nList of available models: ' + str_list_of_available_models)

    if ("3d" in ARG_MODEL_NAME):
        IS_3D = True

    # LOSS FUNCTION MAP
    loss_foo_map = {
        "ssim" : Met.dssim_metric,
        "gssim" : Met.custom_tf_gssim,
        "rmse" : Met.root_mean_squared_error,
        "mse" : keras.losses.mean_squared_error,
        "ms_ssim" : Met.msssim_metric,
        "psnr": Met.psnr,
        "mean_split_4_gssim": Met.mean_split_4_gssim,
        "sum_split_4_gssim": Met.sum_split_4_gssim,
        "mean_split_16_gssim" : Met.mean_split_16_gssim, ## <<
        "max_split_16_gssim": Met.max_split_16_gssim,
        "sum_split_16_gssim": Met.sum_split_16_gssim,
        "mean_split_64_gssim": Met.mean_split_64_gssim, ## <<
        "sum_split_64_gssim": Met.sum_split_64_gssim,
        "mean_top_3_in_4_split_16_gssim": Met.mean_top_3_in_4_split_16_gssim, ## <<
        "mean_top_3_in_4_split_64_gssim": Met.mean_top_3_in_4_split_64_gssim, ## <<
        "old_slssim_4": Met.old_slssim_4,
        "old_slssim_8" : Met.old_slssim_8, ## <<
        "experimental_split_plus_gssim" : Met.experiment_metric, ## <<
        "experimental_split_plus_gssim_16" : Met.experiment_metric_16, ## <<
        "slgssim_8": Met.slgssim_8,
        "slgssim_4": Met.slgssim_4,
        "old_slssim_8_normed" : Met.old_slssim_8_normed,
        "slgssim_8_normed" : Met.slgssim_8_normed, ## <<
        "slgssim_4_normed": Met.slgssim_4_normed,
        "three_dimensional_gssim": Met.three_dimensional_gssim,
        "three_dimensional_mse": Met.three_dimensional_mse,
        "three_dimensional_experimental_gssim": Met.three_dimensional_experimental_gssim,
        "three_dimensional_gssim_proper_only": Met.three_dimensional_gssim_proper_only,
        "three_dimensional_experimental_gssim_proper_only": Met.three_dimensional_experimental_gssim_proper_only,
        "three_dimensional_experimental_split_plus_gssim": Met.three_dimensional_experimental_split_plus_gssim,
        "three_dimensional_experimental_split_plus_gssim_proper_only": Met.three_dimensional_experimental_split_plus_gssim_proper_only,
        "three_dimensional_mean_top_3_in_4_split_64_gssim": Met.three_dimensional_mean_top_3_in_4_split_64_gssim,
        "three_dimensional_mean_top_3_in_4_split_64_gssim_proper_only": Met.three_dimensional_mean_top_3_in_4_split_64_gssim_proper_only,
        "three_dimensional_experimental_split_plus_gssim_with_mse" : Met.three_dimensional_experimental_split_plus_gssim_with_mse,
        "three_dimensional_experimental_split_plus_gssim_proper_only_with_rmse" : Met.three_dimensional_experimental_split_plus_gssim_proper_only_with_rmse,
        "three_dimensional_experimental_mse_proper_only": Met.three_dimensional_experimental_mse_proper_only,
    }

    # def three_dimensional_gssim(stacked_1, stacked_2):
    #     return three_dimensional_metric(stacked_1, stacked_2, custom_tf_gssim)
    #
    # def three_dimensional_experimental_gssim(stacked_1, stacked_2):
    #     return three_dimensional_metric(stacked_1, stacked_2, experimental_split_plus_gssim)
    #
    # def three_dimensional_gssim_proper_only(stacked_1, stacked_2):
    #     return three_dimensional_metric(stacked_1, stacked_2, custom_tf_gssim, three_dimensional_reduction_proper_only)
    #
    # def three_dimensional_experimental_gssim_proper_only(stacked_1, stacked_2):

    loss_foo = loss_foo_map.get(ARG_LOSS_NAME)
    if (loss_foo is None):
        str_list_of_available_losses = ", ".join(list(loss_foo_map.keys()))
        raise ValueError('Loss function with name "' + ARG_LOSS_NAME +'" is not defined!\nList of available loss functions: ' + str_list_of_available_losses)


    # %%%
    # PARAMS END

    intermediate_dirs = (FileUtils.create_dir(FileUtils.apth("./output/generated_imgs/{formatted_datetime_now}/full")),
                            FileUtils.create_dir(FileUtils.apth("./output/generated_imgs/{}/low".format(formatted_datetime_now))))

    flow_param_dictionary = dict(
        image_config = dict(
            # featurewise_center=True,
            # featurewise_std_normalization=True,
            # horizontal_flip=True, # TODO: DO NOT USE THIS
            rescale=1./255,
        ),
        fit_smpl_size = 1,
        # directory = FULL_DOSE_DATA_DIR,
        target_size = TARGET_SIZE,
        batch_size = BATCH_SIZE,
        shuffle = False,
        seed = 1,
        interpolation_method = INTERPOLATION_METHOD,
        # save_to_dir = FileUtils.create_dir(FileUtils.apth(INTERMEDIATE_DIR))
    )

    full_dose_param_dictionary = flow_param_dictionary.copy()
    full_dose_param_dictionary['directory'] = FULL_DOSE_DATA_DIR
    # full_dose_param_dictionary['save_to_dir'] = intermediate_dirs[0]

    low_dose_param_dictionary = flow_param_dictionary.copy()
    low_dose_param_dictionary['directory'] = LOW_DOSE_DATA_DIR
    # low_dose_param_dictionary['save_to_dir'] = intermediate_dirs[1]

    # %%%
    # NOTE: ** passes dict as param values to given function

    full_dose_train_flow = KerasUtils.get_img_fit_flow_in_3d(**full_dose_param_dictionary) \
        if IS_3D else KerasUtils.get_img_fit_flow(**full_dose_param_dictionary)
    low_dose_train_flow = KerasUtils.get_img_fit_flow_in_3d(**low_dose_param_dictionary) \
        if IS_3D else KerasUtils.get_img_fit_flow(**low_dose_param_dictionary)

    assert(full_dose_train_flow.n == low_dose_train_flow.n)
    assert(full_dose_train_flow.n != 0)
    assert(low_dose_train_flow.n != 0)

    num_of_pairs = full_dose_train_flow.n

    low_full_train_generator = zip(low_dose_train_flow, full_dose_train_flow)

     ### -- Validation --

    VAL_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'val_full_dose')
    VAL_LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'val_low_dose')

    val_flow_param_dictionary = flow_param_dictionary.copy()
    val_flow_param_dictionary['image_config'] = dict(
        rescale=1. / 255,
    )

    val_full_dose_param_dictionary = val_flow_param_dictionary.copy()
    val_full_dose_param_dictionary['directory'] = VAL_FULL_DOSE_DATA_DIR
    val_full_dose_param_dictionary['batch_size'] = 1
    # full_dose_param_dictionary['save_to_dir'] = intermediate_dirs[0]

    val_low_dose_param_dictionary = val_flow_param_dictionary.copy()
    val_low_dose_param_dictionary['directory'] = VAL_LOW_DOSE_DATA_DIR
    val_low_dose_param_dictionary['batch_size'] = 1

    # NOTE: ** passes dict as param values to given function
    full_dose_val_flow = KerasUtils.get_img_fit_flow_in_3d(**val_full_dose_param_dictionary) \
        if IS_3D else KerasUtils.get_img_fit_flow(**val_full_dose_param_dictionary)
    low_dose_val_flow = KerasUtils.get_img_fit_flow_in_3d(**val_low_dose_param_dictionary) \
        if IS_3D else KerasUtils.get_img_fit_flow(**val_low_dose_param_dictionary)
    assert(full_dose_val_flow.n != 0)
    assert(low_dose_val_flow.n != 0)
    assert(full_dose_val_flow.n == low_dose_val_flow.n)
    low_full_val_generator = zip(low_dose_val_flow, full_dose_val_flow)
    num_of_val_pairs = full_dose_val_flow.n

    ####### BUILDING IN TEST SET RESULTS #########

    TEST_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'test_full_dose')
    TEST_LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'test_low_dose')

    # use carbon-copy of validation params
    test_flow_param_dictionary = val_flow_param_dictionary.copy()

    test_full_dose_param_dictionary = test_flow_param_dictionary.copy()
    test_full_dose_param_dictionary['directory'] = TEST_FULL_DOSE_DATA_DIR
    test_full_dose_param_dictionary['batch_size'] = 1


    test_low_dose_param_dictionary = test_flow_param_dictionary.copy()
    test_low_dose_param_dictionary['directory'] = TEST_LOW_DOSE_DATA_DIR
    test_low_dose_param_dictionary['batch_size'] = 1

    # NOTE: ** passes dict as param values to given function
    full_dose_test_flow = KerasUtils.get_img_fit_flow_in_3d(**test_full_dose_param_dictionary) \
        if IS_3D else KerasUtils.get_img_fit_flow(**test_full_dose_param_dictionary)
    low_dose_test_flow = KerasUtils.get_img_fit_flow_in_3d(**test_low_dose_param_dictionary) \
        if IS_3D else KerasUtils.get_img_fit_flow(**test_full_dose_param_dictionary)

    assert(full_dose_test_flow.n != 0)
    assert(low_dose_test_flow.n != 0)
    assert(full_dose_test_flow.n == low_dose_test_flow.n)
    low_full_test_generator = zip(low_dose_test_flow, full_dose_test_flow)
    num_of_test_pairs = full_dose_test_flow.n

    ###


    # OPTIMIZER MAP
    optimizer_map = {
        "adam" : keras.optimizers.adam(),
        "adam_decay_001" : keras.optimizers.adam(decay=0.01),
        "adam_decay_005" : keras.optimizers.adam(decay=0.05),
        "adam_decay_0025" : keras.optimizers.adam(decay=0.025),
        "custom_adam" : keras.optimizers.adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0),
        "sgd_nesterov" : keras.optimizers.SGD(learning_rate=0.001, momentum=0.9, decay=0.01, nesterov=True),
    }
    optimizer = optimizer_map.get(ARG_OPTIMIZER_NAME)
    if (optimizer is None):
        str_list_of_available_optimizers = ", ".join(list(optimizer_map.keys()))
        raise ValueError('Optimizer with name "' + ARG_OPTIMIZER_NAME +
                         '" is not defined!\nList of available optimizers: ' + str_list_of_available_optimizers)

    initial_model_epochs = 0

    metric_list = [keras.losses.mean_squared_error,
                   Met.root_mean_squared_error,
                   Met.dssim_metric,
                   Met.msssim_metric,
                   Met.custom_tf_ssim,
                   # Met.custom_tf_ms_ssim,
                   Met.custom_tf_gssim,
                   ]
    metric_list_3d = [keras.losses.mean_squared_error,
                      Met.three_dimensional_gssim,
                      Met.three_dimensional_gssim_proper_only,
                      Met.three_dimensional_experimental_ssim_proper_only,
                      Met.three_dimensional_experimental_mse_proper_only,]

    if ARG_CONTINUE_TRAIN_MODEL:
        print(
    """
    ************************************************
    ************************************************
            CONTINUED MODEL TRAINING MODE
    ************************************************
    ************************************************
    """
        )
        continued_model_path = ARG_CONTINUE_TRAIN_MODEL
        if (not os.path.exists(continued_model_path)):
            raise ValueError(f"Model for continued training in path {continued_model_path} does not exist!")
        model = load_model_from_file(continued_model_path)
        if (not ARG_CONTINUE_TRAIN_MODEL_EPOCH):
            raise ValueError(f"Model for continued training initial epoch count not given!")
        initial_model_epochs = int(ARG_CONTINUE_TRAIN_MODEL_EPOCH)
        EXPERIMENT_NAME+="_cont"
    elif ARG_TPU:
        with strategy.scope():
            model.summary()
            # adam_optimizer = keras.optimizers.adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
            model.compile(optimizer=optimizer, # NOTE: use this, we define optimizer via arguments
                          loss=loss_foo,
                          metrics=metric_list if not IS_3D else metric_list_3d
                          )
    else:
        model.summary()
        # adam_optimizer = keras.optimizers.adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
        model.compile(optimizer=optimizer,
                      loss=loss_foo,
                      metrics=metric_list if not IS_3D else metric_list_3d
                      )

    # earlystopper = kcb.EarlyStopping(patience=3, verbose=1)
    # model_checkpointer = kcb.ModelCheckpoint(
    #     os.path.join(MODEL_OUTPUT_FOLDER, f"test-model-{DateUtils.formatted_datetime_now()}.h5")
    #     , verbose=1, save_best_only=True)

    MODEL_OUTPUT_FOLDER = FileUtils.create_dir(os.path.join(EXPERIMENT_OUTPUT_PATH, '{}_{}_{}_{}_epoch={}_{}'
                                                            .format(EXPERIMENT_NAME, ARG_MODEL_NAME,
                                                                    ARG_LOSS_NAME, ARG_OPTIMIZER_NAME,
                                                                    initial_model_epochs+EPOCH_COUNT, formatted_datetime_now)))
    print("Experiment output folder: {}".format(MODEL_OUTPUT_FOLDER))

    csv_log_file_path = os.path.join(MODEL_OUTPUT_FOLDER, "{}_{}_log.csv".format(EXPERIMENT_NAME, formatted_datetime_now))
    csv_logger = keras.callbacks.CSVLogger(csv_log_file_path)
    best_loss_saver = keras.callbacks.ModelCheckpoint(filepath=MODEL_OUTPUT_FOLDER + "/{}_best-only.h5".format(EXPERIMENT_NAME),
                                                                save_best_only=True,
                                                                monitor='val_loss')
    epoch_saver = KerasUtils.EpochSaver(epoch_save_interval=SAVE_EVERY_N_EPOCHS,
                                        file_name_prefix=MODEL_OUTPUT_FOLDER+f"/{EXPERIMENT_NAME}_checkpoint_",
                                        save_last_only=False, initial_model_epochs=initial_model_epochs)

    # tensorboard_cb = keras.callbacks.TensorBoard(logdir, histogram_freq=1) #TODO: we cannot use histograms if we're using a generator for validation data
    # tensorboard_cb = keras.callbacks.TensorBoard(logdir)

    sandwich_divisor = 1 if not IS_3D else 4
    num_of_pairs = math.ceil(num_of_pairs / sandwich_divisor)
    num_of_val_pairs = math.ceil(num_of_val_pairs / sandwich_divisor)
    num_of_test_pairs = math.ceil(num_of_test_pairs / sandwich_divisor)

    history_callback = model.fit_generator(low_full_train_generator, epochs=EPOCH_COUNT,
                        # As per recommendation in Keras docs AND : https://datascience.stackexchange.com/questions/47405/what-to-set-in-steps-per-epoch-in-keras-fit-generator
                        # steps_per_epoch=1, #TODO: fix
                        steps_per_epoch = math.ceil(num_of_pairs / BATCH_SIZE),
                        verbose = VERBOSE_MODE,
                        validation_data=low_full_val_generator,
                        validation_steps=math.ceil(num_of_val_pairs / BATCH_SIZE),
                        callbacks = [best_loss_saver, csv_logger, epoch_saver],  # TODO: add proper tensorboard_cb,
                        shuffle=False,
                        )
    model.save("{}/{}_last-only_epoch={}.h5".format(MODEL_OUTPUT_FOLDER, EXPERIMENT_NAME, EPOCH_COUNT+initial_model_epochs))


    before_predictions = DateUtils.now()
    evaluated_val_loss = model.evaluate_generator(low_full_val_generator, steps=num_of_val_pairs, verbose=1, )
    val_loss_with_names = list(zip(model.metrics_names, evaluated_val_loss))

    evaluated_test_loss = model.evaluate_generator(low_full_test_generator, steps=num_of_test_pairs, verbose=1)
    test_loss_with_names = list(zip(model.metrics_names, evaluated_test_loss))

    # evaluated_test_loss = model.evaluate_generator()

    # Saving model after full training

    # RA.compare_low_full_predicted_images(data_dir=EXPERIMENT_DATA_FOLDER,
    #                                      output_dir=MODEL_OUTPUT_FOLDER,
    #                                      model=model,
    #                                      display_here=False,
    #                                      limited_image_count=10)
    after_predictions = DateUtils.now()

    formatted_datetime_experiment_end = DateUtils.formatted_datetime_now()
    report_message = """
    Model trained: {}
    Loss function used: {}
    Optimizer used: {}
    Trained epoch count: {}
    Batch size: {}
    Resolution: {} x {} (note: 256 default)
    Dataset used: {}
    Experiment started: {}
    Experiment ended: {}
    Validation and test loss calculation duration: {}
    Experiment output directory: {}

    ---
    Calculated validation loss: {}

    Calculated test loss: {}
    ---
    """.format(ARG_MODEL_NAME, ARG_LOSS_NAME, ARG_OPTIMIZER_NAME, EPOCH_COUNT,
               BATCH_SIZE, IMAGE_RESOLUTION, IMAGE_RESOLUTION,
               ARG_DATA_DIR, formatted_datetime_now, formatted_datetime_experiment_end,
               str(after_predictions - before_predictions), MODEL_OUTPUT_FOLDER,
               str(val_loss_with_names),
               str(test_loss_with_names),
               )
    if (ARG_CONTINUE_TRAIN_MODEL):
        report_message = \
    """
    *** Continued training ***
    Given model to train: {}
    Initial model epochs: {}
    *** *** *** *** *** *** **
    """.format(ARG_CONTINUE_TRAIN_MODEL, initial_model_epochs) + report_message

    try:
        sendy.send_notification_email("Experiment '{}' has finished!".format(EXPERIMENT_NAME),
                                      report_message,
                                      ARG_RECIPIENT_EMAIL
                                      )
    except Exception as e:
        print(f"Exception occured:\n{e}")

    report_file_path = os.path.join(MODEL_OUTPUT_FOLDER, "{}_report.txt".format(EXPERIMENT_NAME))
    report_file = open(report_file_path,"w")
    report_file.write(report_message)
    report_file.close()

    # TODO: model.fit_generator
    # model.fit_generator(training_ts_gen, epochs=opts['EPOCHS'], verbose=opts['VERBOSE_MODE'],callbacks=[epoch_saver_callback])

    # TODO: Test generator -- test_gen (+) model.predict_generator(test_gen)
    # testing_ts_gen = TimeseriesGenerator(test_X.values, test_y.values, opts['PAST_TIME_STEP_COUNT'],
    #                                      BATCH_SIZE=opts['BATCH_SIZE'])
    # predictions = model.predict_generator(testing_ts_gen, verbose=opts['VERBOSE_MODE'])

    #### --- [ TESTING PERFORMANCE ] --- ####

    # test_full_dose_param_dictionary = flow_param_dictionary.copy()
    # test_full_dose_param_dictionary['directory'] = TEST_FULL_DOSE_DATA_DIR

    # test_low_dose_param_dictionary = flow_param_dictionary.copy()
    # test_low_dose_param_dictionary['directory'] = TEST_LOW_DOSE_DATA_DIR

    # test_full_dose_train_flow = KerasUtils.get_img_fit_flow(**test_full_dose_param_dictionary)
    # test_low_dose_train_flow = KerasUtils.get_img_fit_flow(**test_low_dose_param_dictionary)

    # test_low_full_train_generator = zip(test_low_dose_train_flow, test_full_dose_train_flow)

    # predictions = model.predict_generator(test_low_dose_train_flow, steps=1)
    # full_dose_images = test_full_dose_train_flow.next()

    # for idx in range(len(predictions[::])):
    #     full_dose_image = full_dose_images[idx]
    #     predicted_image = predictions[idx]
    #     # predictions will contain (x,y,1) shaped np.arrays - need to remove the '1' dim in order to use plt.imshow
    #     ImageUtils.show_2_ct_images(full_dose_image, predicted_image, "full dose", "denoised")
