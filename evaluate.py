import tensorflow as tf

from train import load_model_from_file

# %%%
tf.test.gpu_device_name()

# %%%
import math

import pandas as pd

from utils import *


def evaluate_model(experiment_name : str, model_path: str, data_folder: str,
                   evaluation_results_folder: str = "./evaluation-results",
                   nvi_data = False,
                   export_images = False,
                   is_3d = False):
    model = load_model_from_file(model_path=model_path)

    EXPERIMENT_DATA_FOLDER = data_folder

    VAL_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'val_full_dose')
    VAL_LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'val_low_dose')

    BATCH_SIZE = 1
    flow_param_dictionary = dict(
        image_config=dict(
            # featurewise_center=True,
            # featurewise_std_normalization=True,
            # horizontal_flip=True, # TODO: DO NOT USE THIS
            rescale=1. / 255,
        ),
        fit_smpl_size=1,
        # directory = FULL_DOSE_DATA_DIR,
        target_size=(256, 256),
        batch_size=BATCH_SIZE,  # <<<< CHANGED <<<<
        shuffle=False,
        seed=1,
        interpolation_method="nearest",
        # save_to_dir = FileUtils.create_dir(FileUtils.apth(INTERMEDIATE_DIR))
    )

    val_flow_param_dictionary = flow_param_dictionary.copy()
    val_flow_param_dictionary['image_config'] = dict(
        rescale=1. / 255,
    )

    global low_full_val_generator

    if (not nvi_data):
        val_full_dose_param_dictionary = val_flow_param_dictionary.copy()
        val_full_dose_param_dictionary['directory'] = VAL_FULL_DOSE_DATA_DIR
        # val_full_dose_param_dictionary['batch_size'] = 1
        # full_dose_param_dictionary['save_to_dir'] = intermediate_dirs[0]

        val_low_dose_param_dictionary = val_flow_param_dictionary.copy()
        val_low_dose_param_dictionary['directory'] = VAL_LOW_DOSE_DATA_DIR
        # val_low_dose_param_dictionary['batch_size'] = 1

    # NOTE: ** passes dict as param values to given function
        full_dose_val_flow = KerasUtils.get_img_fit_flow_in_3d(**val_full_dose_param_dictionary) \
            if is_3d else KerasUtils.get_img_fit_flow(**val_full_dose_param_dictionary)
        low_dose_val_flow = KerasUtils.get_img_fit_flow_in_3d(**val_low_dose_param_dictionary) \
            if is_3d else KerasUtils.get_img_fit_flow(**val_low_dose_param_dictionary)
        # full_dose_val_flow = KerasUtils.get_img_fit_flow(**val_full_dose_param_dictionary)
        # low_dose_val_flow = KerasUtils.get_img_fit_flow(**val_low_dose_param_dictionary)

        assert (full_dose_val_flow.n != 0)
        assert (low_dose_val_flow.n != 0)
        assert (full_dose_val_flow.n == low_dose_val_flow.n)

        low_full_val_generator = zip(low_dose_val_flow, full_dose_val_flow)
        num_of_val_pairs = full_dose_val_flow.n

    ####### BUILDING IN TEST SET RESULTS #########

    TEST_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'test_full_dose')
    TEST_LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'test_low_dose')

    # use carbon-copy of validation params
    test_flow_param_dictionary = val_flow_param_dictionary.copy()

    test_full_dose_param_dictionary = test_flow_param_dictionary.copy()
    test_full_dose_param_dictionary['directory'] = TEST_FULL_DOSE_DATA_DIR

    test_low_dose_param_dictionary = test_flow_param_dictionary.copy()
    test_low_dose_param_dictionary['directory'] = TEST_LOW_DOSE_DATA_DIR

    # NOTE: ** passes dict as param values to given function

    full_dose_test_flow = KerasUtils.get_img_fit_flow_in_3d(**test_full_dose_param_dictionary) \
        if is_3d else KerasUtils.get_img_fit_flow(**test_full_dose_param_dictionary)
    low_dose_test_flow = KerasUtils.get_img_fit_flow_in_3d(**test_low_dose_param_dictionary) \
        if is_3d else KerasUtils.get_img_fit_flow(**test_low_dose_param_dictionary)

    assert (full_dose_test_flow.n != 0)
    assert (low_dose_test_flow.n != 0)
    assert (full_dose_test_flow.n == low_dose_test_flow.n)
    low_full_test_generator = zip(low_dose_test_flow, full_dose_test_flow)
    num_of_test_pairs = full_dose_test_flow.n

    sandwich_divisor = 1 if not is_3d else 4
    if (not nvi_data):
        num_of_val_pairs = math.ceil(num_of_val_pairs / sandwich_divisor)
    num_of_test_pairs = math.ceil(num_of_test_pairs / sandwich_divisor)


    class MyCustomCallback(keras.callbacks.Callback):

        def __init__(self):
            super().__init__()
            self.result_df = pd.DataFrame()

        # def on_train_batch_begin(self, batch, logs=None):
        #     print('Training: batch {} begins at {}'.format(batch, datetime.datetime.now().time()))
        #
        # def on_train_batch_end(self, batch, logs=None):
        #     print('Training: batch {} ends at {}'.format(batch, datetime.datetime.now().time()))
        #
        # def on_test_batch_begin(self, batch, logs=None):
        #     print('Evaluating: batch {} begins at {}'.format(batch, datetime.datetime.now().time()))

        def on_test_batch_end(self, batch, logs=None):
            self.result_df = self.result_df.append(logs, ignore_index=True)
            # print('Evaluating: batch {} ends at {}'.format(batch, datetime.datetime.now().time()))

        def get_result_df(self):
            return self.result_df

    before_predictions = DateUtils.now()

    # MAKING SURE THAT THE EVALUATION RESULT FOLDER IS CREATED!
    evaluation_results_folder += "_nvi" if nvi_data else ""
    FileUtils.create_dir(evaluation_results_folder)

    if (export_images):
        # export_full_dose_test_flow = KerasUtils.get_img_fit_flow(**test_full_dose_param_dictionary)
        export_low_dose_test_flow = KerasUtils.get_img_fit_flow_in_3d(**test_low_dose_param_dictionary) \
            if is_3d else KerasUtils.get_img_fit_flow(**test_low_dose_param_dictionary)

        export_num_of_test_pairs = num_of_test_pairs
        predictions = model.predict_generator(export_low_dose_test_flow,
                                              verbose=1,
                                              steps=export_num_of_test_pairs,
                                              use_multiprocessing=False)

        export_folder = os.path.join(evaluation_results_folder, experiment_name)
        FileUtils.create_dir(export_folder)

        import cv2

        if (not is_3d):
            for idx, prediction in enumerate(predictions):
                X = np.clip((prediction) * (255.0), 0, 255).astype(np.uint8)
                cv2.imwrite(os.path.join(export_folder, f"prediction_{idx}.png"), X)
        else:
            for idx, prediction in enumerate(predictions):
                proper_image = prediction[2]
                X = np.clip((proper_image) * (255.0), 0, 255).astype(np.uint8)
                cv2.imwrite(os.path.join(export_folder, f"proper_prediction_{idx}.png"), X)


    global val_df
    if (not nvi_data):
        val_cb = MyCustomCallback()
        evaluated_val_loss = model.evaluate_generator(low_full_val_generator,
                                                      steps=math.ceil(num_of_val_pairs / BATCH_SIZE), verbose=1, callbacks=[
                val_cb])
        val_loss_with_names = list(zip(model.metrics_names, evaluated_val_loss))
        val_df = val_cb.get_result_df()
        val_df.to_csv(os.path.join(evaluation_results_folder,
                                   f'{experiment_name}_val_report.csv'), encoding='utf-8',
                                   mode='w', header=True, index=False)

    test_cb = MyCustomCallback()
    evaluated_test_loss = model.evaluate_generator(low_full_test_generator,
                                                   steps=math.ceil(num_of_test_pairs / BATCH_SIZE), verbose=1,
                                                   callbacks=[
                                                       test_cb])
    test_loss_with_names = list(zip(model.metrics_names, evaluated_test_loss))
    test_df = test_cb.get_result_df()
    nvi_name = "__nvi__" if nvi_data else ""
    test_df.to_csv(os.path.join(evaluation_results_folder, f'{experiment_name}_test_{nvi_name}report.csv'), encoding='utf-8', mode='w',
                   header=True, index=False)

    after_predictions = DateUtils.now()
    print(f"{experiment_name} | evaluation duration: {after_predictions - before_predictions}")

    return (val_df, test_df) if not nvi_data else (None, test_df)

if __name__ == '__main__':
    model_path = r"C:\Users\Lenovo\Desktop\Other\proper_unet_pp_last-only_epoch=500.h5"
    data_folder = r"C:\Users\Lenovo\repos\ct-reconstruction\output\prepared_mayo_experiment_data_other_split_smaller"
    evaluate_model(experiment_name= "proper_unet_pp_last-only_epoch=500",
                   model_path=model_path,
                   data_folder=data_folder)
