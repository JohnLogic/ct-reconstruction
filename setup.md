# Environment setup

- Base environment setup:
```
conda create -n master python=3 -y; 
conda activate master;
conda install numpy scipy pandas matplotlib scikit-learn requests -y;
```

- (TODO UPDATE/FIX) Installing keras + tensorflow
    - For CPU: ``conda install -y tensorflow'<=1.14.0' keras``
    - For GPU: ``conda install -y tensorflow-gpu'<=1.15.0' keras-gpu``
        - Install this also **???** ``conda install cudatoolkit=9.0 cudnn=7.3 keras-gpu``

- DICOM support setup:
```
pip install pydicom; 
conda install plotly, scikit-image -y
conda install -c conda-forge opencv -y
```
- **NOTE**: don't know if we need plotly and scikit-image anymore

# NEW 
```
conda install numpy scipy pandas matplotlib scikit-learn requests plotly scikit-image -y
$conda install -c conda-forge opencv -y
pip install pydicom; 
conda install -y tensorflow-gpu'<=1.15.0' keras-gpu -y
conda install cudatoolkit=9.0 cudnn=7.3 keras-gpu -y
```