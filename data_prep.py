import dicom_utils as DU
from utils import *

# Generates pairs by sorting each path AND pairing in sequence [(first_low, first_high), (second_low, second_high) ... ]
def fetch_image_pair_dirs(full_dose_path:str = os.path.join('.','data','example-full-and-low-dose-pairs','full-dose'),
                          low_dose_path:str = os.path.join('.','data','example-full-and-low-dose-pairs','low-dose'),
                          file_extensions : [str] = ['IMA']) \
                          -> [(str, str)]:
    # Gets sorted names
    sorted_full_dose_names = sorted(os.listdir(full_dose_path))
    sorted_low_dose_names = sorted(os.listdir(low_dose_path))

    if (file_extensions is not None):
        filter_file_extension_foo = lambda path: path.split('.')[-1] in file_extensions
        sorted_full_dose_names = list(filter(filter_file_extension_foo, sorted_full_dose_names))
        sorted_low_dose_names = list(filter(filter_file_extension_foo, sorted_low_dose_names))

    if (len(sorted_full_dose_names) != len(sorted_low_dose_names)):
        raise ValueError(f"There's an uneven amount of images in paths: '{sorted_low_dose_names}' and '{sorted_full_dose_names}'!")

    # Gets absolute paths
    sorted_full_dose_paths = list(map(lambda name: os.path.abspath(os.path.join(full_dose_path, name)), sorted_full_dose_names))
    sorted_low_dose_paths = list(map(lambda name: os.path.abspath(os.path.join(low_dose_path, name)), sorted_low_dose_names))

    return list(zip(sorted_low_dose_paths, sorted_full_dose_paths))

def prepare_ct_dataset(image_pair_paths : (str, str),
                       window_level : int = 100,
                       window_width : int = 100,
                       output_dir : str = os.path.join(".","output"),
                       separate_low_dose_output_dir : str = None, # NOTE: this is if we want separate folders
                       prepared_low_dose_image_prefix : str = "",
                       prepared_full_dose_image_prefix : str = "",
                       ensure_create_dirs = False,
                       multiple_windows : [(int, int)] = [],
                       ):

    if (separate_low_dose_output_dir is None):
        separate_low_dose_output_dir = output_dir

    if (not ensure_create_dirs):
        if not(Path(output_dir).exists()):
            pass
        elif Path(separate_low_dose_output_dir).exists():
            pass
    else:
        Path(output_dir).mkdir(parents=True, exist_ok=True)
        Path(separate_low_dose_output_dir).mkdir(parents=True, exist_ok=True)

    # TODO: counter is not working
    # counter : int = 0
    # image_count = len(image_pair_paths) * 2

    def preproc_image_foo(image, prefix, output_dir):
        DU.preprocess_image(image, window_level=window_level, window_width=window_width,
                            save=True, output_dir=output_dir, preprocessed_image_prefix=prefix)
        # counter+=1
        # if (counter % 5 == 0):
        #     print(f'${counter}/${image_count}')

    if (len(multiple_windows) == 0):
        preprocessed_image_pair_paths = list(
            map(lambda pair: (preproc_image_foo(pair[0], prepared_low_dose_image_prefix, separate_low_dose_output_dir),
                              preproc_image_foo(pair[1], prepared_full_dose_image_prefix, output_dir)),
                              image_pair_paths)
            )
    else:
        def preproc_image_windowed_foo(image, prefix, output_dir):
            lst = []
            for level, width in multiple_windows:
                DU.preprocess_image(image, window_level=level, window_width=width,
                                    save=True, output_dir=output_dir,
                                    preprocessed_image_prefix=prefix,
                                    preprocessed_image_postfix=f"_{level}_{width}")
            return lst

        flatten = lambda l: [item for sublist in l for item in sublist]
        preprocessed_image_pair_paths = (list(
            map(lambda pair: (preproc_image_windowed_foo(pair[0], prepared_low_dose_image_prefix, separate_low_dose_output_dir),
                              preproc_image_windowed_foo(pair[1], prepared_full_dose_image_prefix, output_dir)),
                              image_pair_paths)
            ))

    return preprocessed_image_pair_paths

### --- Premade functions for datasets ---

def preprocess_mayo_data(root_folder_path : str,
                         low_dose_folder_name : str = 'quarter_1mm',
                         full_dose_folder_name : str = 'full_1mm',
                         window_level = 100,
                         window_width = 400,
                         multiple_windows : [(int, int)] = [],
                         output_folder = os.path.join('.', 'output', 'prepared_mayo_experiment_data')):

    list_of_subfolders_in_root = os.listdir(root_folder_path)
    def directory_filter(folder_path):
        return (low_dose_folder_name in
                os.listdir(os.path.join(root_folder_path, folder_path)), list_of_subfolders_in_root
            and full_dose_folder_name
                in os.listdir(os.path.join(root_folder_path, folder_path)), list_of_subfolders_in_root)
    list_of_folders_with_images = list(filter(directory_filter, list_of_subfolders_in_root))

    FileUtils.create_dir(output_folder) # make sure the output folder exists

    for idx, image_folder in enumerate(list_of_folders_with_images):
        low_dose_path = os.path.join(root_folder_path, image_folder, low_dose_folder_name)
        full_dose_path = os.path.join(root_folder_path, image_folder, full_dose_folder_name)
        image_pairs = fetch_image_pair_dirs(full_dose_path, low_dose_path, file_extensions=['IMA','dcm'])
        prepare_ct_dataset(image_pairs, window_level=window_level, window_width=window_width,
                           output_dir=os.path.join(output_folder, 'full_dose'),
                           separate_low_dose_output_dir=os.path.join(output_folder, 'low_dose'),
                           prepared_low_dose_image_prefix=f"LOW_DOSE_",
                           prepared_full_dose_image_prefix=f"FULL_DOSE_",
                           ensure_create_dirs=True,
                           multiple_windows=multiple_windows)
        print(f"Images in folder {image_folder} processed. {idx+1}/{len(list_of_folders_with_images)}")

# This removes 'part_quotient' in every 'part_divisor' files (truncates or trims the dataset)
def remove_some_part_of_images(data_dir : str, part_quotient: int = 1, part_divisor: int = 3):
    assert(part_quotient <= part_divisor)

    def remove_some_part_of_images_in_folder(folder_dir: str, part_quotient: int = 1, part_divisor: int = 3):
        proper_dir = os.path.join(folder_dir, "0")  # to go to necessary subfolder
        file_list = os.listdir(proper_dir)

        to_be_deleted = part_quotient
        for idx, file in enumerate(file_list):
            if (idx % part_divisor == 0):
                to_be_deleted = part_quotient
            elif (to_be_deleted > 0):
                file_path_to_delete = os.path.join(proper_dir, file)
                os.unlink(file_path_to_delete)
                to_be_deleted -= 1
                print(f"Deleted file idx={idx} in folder={proper_dir}")

    folder_list = ['full_dose', 'low_dose', 'test_full_dose', 'test_low_dose', 'val_full_dose', 'val_low_dose']

    for folder in folder_list:
        remove_some_part_of_images_in_folder(os.path.join(data_dir, folder), part_quotient, part_divisor)

def augment_data_set():

    def prepare_data_flow(original_image_directory : str, augmented_output_directory : str):

        proper_output_directory = FileUtils.create_dir(path_string=os.path.join(augmented_output_directory, "0"))

        BATCH_SIZE = 1
        TARGET_SIZE = (256, 256)
        INTERPOLATION_METHOD = "nearest"

        flow_param_dictionary = dict(
            image_config=dict(
                # featurewise_center=True,
                # featurewise_std_normalization=True,
                # horizontal_flip=True, #TODO: do not use flips <<<<
                rescale=1. / 255,
            ),
            directory=original_image_directory,
            fit_smpl_size=1,
            # directory = FULL_DOSE_DATA_DIR,
            target_size=TARGET_SIZE,
            batch_size=BATCH_SIZE,
            shuffle=False,
            seed=1,
            interpolation_method=INTERPOLATION_METHOD,
            # save_to_dir = FileUtils.create_dir(FileUtils.apth(INTERMEDIATE_DIR))
            save_to_dir=proper_output_directory,  # this is where we figure out where to save
            save_prefix='augmented_',  # it will save the images as 'aug_0912' some number for every new augmented image
            save_format='png'
        )

        flow = KerasUtils.get_img_fit_flow(**flow_param_dictionary)
        number_of_images = len([name for name in os.listdir(os.path.join(original_image_directory, "0")) if name.endswith('png')])
        print(f"Augmenting images from {original_image_directory}")
        print(f"Augmenting images to {proper_output_directory}")
        for x, val in zip(flow, range(number_of_images)):
            if (val % 100 == 0):
                print(f'{val}/{number_of_images}')

        return flow

    EXPERIMENT_DATA_FOLDER = os.path.join(".", "output", "prepared_mayo_experiment_data_other_split")

    FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'full_dose')
    LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'low_dose')
    TEST_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'test_full_dose')
    TEST_LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'test_low_dose')
    VAL_FULL_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'val_full_dose')
    VAL_LOW_DOSE_DATA_DIR = os.path.join(EXPERIMENT_DATA_FOLDER, 'val_low_dose')

    OUTPUT_DATA_DIR = os.path.join('.', 'output', 'augmented_data_0409')

    full_dose_flow = prepare_data_flow(FULL_DOSE_DATA_DIR, os.path.join(OUTPUT_DATA_DIR, 'full_dose'))
    low_dose_flow = prepare_data_flow(LOW_DOSE_DATA_DIR, os.path.join(OUTPUT_DATA_DIR, 'low_dose'))
    test_full_dose_flow = prepare_data_flow(TEST_FULL_DOSE_DATA_DIR, os.path.join(OUTPUT_DATA_DIR, 'test_full_dose'))
    test_low_dose_flow = prepare_data_flow(TEST_LOW_DOSE_DATA_DIR, os.path.join(OUTPUT_DATA_DIR, 'test_val_dose'))
    val_full_dose_flow = prepare_data_flow(VAL_FULL_DOSE_DATA_DIR, os.path.join(OUTPUT_DATA_DIR, 'val_full_dose'))
    val_low_dose_flow = prepare_data_flow(VAL_LOW_DOSE_DATA_DIR, os.path.join(OUTPUT_DATA_DIR, 'val_low_dose'))
