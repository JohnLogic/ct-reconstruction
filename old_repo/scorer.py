import glob, os
import argparse
from old.scorerClasses import *

scorerDict = getScorerDict()
noiserDict = getNoiserDict()

descMsg = "Registered scorer algorithms keys: \n"
for scorerItem in scorerDict.keys():
    descMsg += scorerItem+";\n"
descMsg += "Registered noise algorithms: \n"
for noiserItem in noiserDict.keys():
    descMsg += noiserItem+";\n"

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser(description=descMsg)
ap.add_argument("-d", "--dir", required=True,
	help="directory of original images to add noise and score")
ap.add_argument("-o", "--output", required=False,
	help="path to output file for scores per noiseLevel")
ap.add_argument("-s", "--scorer", required=True,
	help="selected image difference scoring algorithm")
ap.add_argument("-n", "--noiser", required=True,
	help="selected noise algorithm")
args = vars(ap.parse_args())

dataset = args["dir"]
outputFile = args["output"]
scorerName = args["scorer"]
noiserName = args["noiser"]

# arg end


############# SCORE AND NOISE ##################

scorer = scorerDict.get(scorerName)
noiser = noiserDict.get(noiserName)

if (scorer == None):
    raise ValueError('Scorer "'+scorerName+'" is not defined')
if (noiser == None):
    raise ValueError('Noiser "'+noiserName+'" is not defined')

# scorer = ScorerSSIM()
# noiser = NoiserSaltPepper()

listOfNoiseLevels = noiser.getLevels()

################################################

if (outputFile == None):
    folders = dataset.split('/')
    lastFolder = folders[len(folders)-1]
    if (folders[len(folders)-1] == ''):
        lastFolder = folders[len(folders)-2]
    outputFile = "{}_result_{}_{}.txt".format(lastFolder, scorerName, noiserName)

if os.path.isfile(outputFile) and os.path.getsize(outputFile) > 0:
    raise ValueError('Output file "{}" already exists and is not empty! Closing script.'.format(outputFile))
f = open(outputFile, "w+")

os.chdir(dataset)
IMG_FILE_EXT = ".png"

# listOfNoiseLevels = [x * 0.005 for x in range(20+1)] # << for salt and pepper
# listOfNoiseLevels = [x * 2 for x in range(10+1)] # << for blur

listOfScoreSumsPerLevel = [0] * len(listOfNoiseLevels)
listOfAverages = []

fileCount = len(glob.glob("*"+IMG_FILE_EXT))

for fileIndex, file in enumerate(glob.glob("*"+IMG_FILE_EXT)):

    # THIS CODE IS INCORRECT - lib.openImg now opens GRAY by default
    # orig_image = lib.openImg(file)
    # image = lib.Transformer.toGrayscale(orig_image)
    # del orig_image
    image = lib.openImg(file)

    for i,noiseLevel in enumerate(listOfNoiseLevels):
        noisyImage = noiser.addNoise(image, noiseLevel) # <<

        # FOR DEBUG outputting generated noisyImages
        # tempFileName = "{}_{}.png".format(fileIndex, noiseLevel)
        # print("trying to save: "+tempFileName)
        # lib.saveImg(tempFileName, noisyImage) # Output actual noisy image

        # listOfScoreSumsPerLevel[i] += lib.AccCalc.calculateSSIM(image, noisyImage) # <<
        score = scorer.calcScore(image, noisyImage) # <<
        listOfScoreSumsPerLevel[i] += score

        # FOR DEBUG outputting intermediate results for each individual file and noiseLevel
        print("File: {}, noiseLevel: {}, score: {}".format(fileIndex, noiseLevel, score))

        del score
        del noisyImage

    print("File "+str(fileIndex+1) + " out of "+str(fileCount)+" done.")

for i, sumOfScores in enumerate(listOfScoreSumsPerLevel):
    avg = sumOfScores/fileCount
    listOfAverages.append(avg)
    # print(str(listOfNoiseLevels[i])+", "+str(avg))
    f.write(str(listOfNoiseLevels[i])+", "+str(avg)+"\n")

f.close()

