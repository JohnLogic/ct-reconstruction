from old import imgLib as lib


# TODO REFAC this ENTIRE THING
# Instead of classes, use:
    # dict "name" -> lambdaCalcScore(image1, image2) OR
    # dict "name" -> (lambdaListOfNoiseLevels, lambdaAddNoise(image, noiseLevel))
    # we can still use the getScorer and getNoiser methods
    # , but this way we can pack stuff in a more compact way

## ---- SCORE CLASSES ---- ##

def getScorerDict():
    ssim = ScorerSSIM()
    slssimv1 = ScorerSLSSIMv1()
    slssimv2 = ScorerSLSSIMv2()
    slssimv2Mod = ScorerSLSSIMv2_mod()
    slssimv3 = ScorerSLSSIMv3()
    slssimv5 = ScorerSLSSIMv5()
    slssimv5Mod = ScorerSLSSIMv5_mod()
    mse = ScorerMSE()
    l1 = ScorerL1()
    wssim = ScorerWSSIM()
    wssim2 = ScorerWSSIMv2()
    wssim3 = ScorerWSSIMv3()
    mi = ScorerMI()
    di = ScorerDi()
    l1Mod = ScorerL1_mod()

    scorerDict = {
        ssim.getName() : ssim,
        slssimv1.getName(): slssimv1,
        slssimv2.getName(): slssimv2,
        mse.getName(): mse,
        slssimv2Mod.getName(): slssimv2Mod,
        slssimv3.getName(): slssimv3,
        slssimv5.getName(): slssimv5,
        slssimv5Mod.getName(): slssimv5Mod,
        l1.getName(): l1,
        wssim.getName(): wssim,
        wssim2.getName(): wssim2,
        wssim3.getName(): wssim3,
        mi.getName(): mi,
        di.getName(): di,
        l1Mod.getName(): l1Mod,
    }

    return scorerDict

def getNoiserDict():
    saltPepper = NoiserSaltPepper()
    gaussAdd = NoiserGaussAdd()
    gaussMul = NoiserGaussMul()

    gaussPoisson = NoiserPoissonGaussian()
    scale = NoiserScaler()
    translate = NoiserTranslaterY()
    rotate = NoiserRotater()
    contrast = NoiserContrastInhomo()

    noiserDict = {
        saltPepper.getName() : saltPepper,
        gaussAdd.getName() : gaussAdd,
        gaussMul.getName() : gaussMul,
        gaussPoisson.getName() : gaussPoisson,
        scale.getName() : scale,
        translate.getName() : translate,
        rotate.getName() : rotate,
        contrast.getName() : contrast,
    }

    return noiserDict

class AbsScorer:
    def __init__(self, name):
        self.name = name
    def getName(self):
        return self.name

class ScorerSSIM(AbsScorer):
    def __init__(self):
        super(ScorerSSIM, self).__init__("SSIM")
    def calcScore(self,image, noisy):
        return lib.AccCalc.calculateSSIM(image, noisy)

class ScorerSLSSIMv1(AbsScorer):
    def __init__(self):
        super(ScorerSLSSIMv1, self).__init__("SLSSIMv1")
    def calcScore(self,image, noisy):
        return lib.AccCalc.calculateSLSSIM(image, noisy, 1)

class ScorerSLSSIMv2(AbsScorer):
    def __init__(self):
        self.step = 32
        super(ScorerSLSSIMv2, self).__init__("SLSSIMv2")
    def calcScore(self,image, noisy):
        return lib.AccCalc.calculateSLSSIM(image, noisy, self.step)

class ScorerSLSSIMv2_mod(AbsScorer):
    def __init__(self):
        self.step = 32
        super(ScorerSLSSIMv2_mod, self).__init__("SLSSIMv2_mod")
    def calcScore(self,image, noisy):
        return lib.AccCalc.calculateSLSSIM_mod(image, noisy, self.step)

class ScorerSLSSIMv3(AbsScorer):
    def __init__(self):
        self.step = 32
        super(ScorerSLSSIMv3, self).__init__("SLSSIMv3")
    def calcScore(self,image, noisy):
        return lib.AccCalc.calculateSLSSIMv3(image, noisy, self.step)

class ScorerSLSSIMv5(AbsScorer):
    def __init__(self):
        self.levels = 8
        super(ScorerSLSSIMv5, self).__init__("SLSSIMv5")
    def calcScore(self,image, noisy):
        return lib.AccCalc.calculateSLSSIMv5(image, noisy, self.levels, False)

class ScorerSLSSIMv5_mod(AbsScorer):
    def __init__(self):
        self.levels = 8
        super(ScorerSLSSIMv5_mod, self).__init__("SLSSIMv5_mod")
    def calcScore(self,image, noisy):
        return lib.AccCalc.calculateSLSSIMv5(image, noisy, self.levels, True)

class ScorerMSE(AbsScorer):
    def __init__(self):
        super(ScorerMSE, self).__init__("MSE")
    def calcScore(self, image, noisy):
        return lib.AccCalc.calculateMSE(image, noisy)

class ScorerL1(AbsScorer):
    def __init__(self):
        super(ScorerL1, self).__init__("L1")
    def calcScore(self, image, noisy):
        return lib.AccCalc.calculateL1(image, noisy)

class ScorerWSSIM(AbsScorer):
    def __init__(self):
        super(ScorerWSSIM, self).__init__("wSSIM")
    def calcScore(self, image, noisy):
        return lib.AccCalc.calculateWSSIM(image, noisy)

class ScorerWSSIMv2(AbsScorer):
    def __init__(self):
        super(ScorerWSSIMv2, self).__init__("wSSIMv2")
    def calcScore(self, image, noisy):
        return lib.AccCalc.calculateWSSIMv2(image, noisy, 32)

class ScorerWSSIMv3(AbsScorer):
    def __init__(self):
        super(ScorerWSSIMv3, self).__init__("wSSIMv3")
    def calcScore(self, image, noisy):
        return lib.AccCalc.calculateWSSIMv3(image, noisy, 8)

class ScorerMI(AbsScorer):
    def __init__(self):
        super(ScorerMI, self).__init__("MI")
    def calcScore(self, image, noisy):
        return lib.AccCalc.calculateMI(image, noisy)

class ScorerDi(AbsScorer):
    def __init__(self):
        super(ScorerDi, self).__init__("Di")
    def calcScore(self, image, noisy):
        return lib.AccCalc.calculateDi(image, noisy)

class ScorerL1_mod(AbsScorer):
    def __init__(self):
        super(ScorerL1_mod, self).__init__("L1_mod")
    def calcScore(self, image, noisy):
        return lib.AccCalc.calculateL1_mod(image, noisy)

## ---- NOISE CLASSES ---- ##

class AbsNoiser:
    def __init__(self, name):
        self.name = name
    def getName(self):
        return self.name

class NoiserSaltPepper(AbsNoiser):
    def __init__(self):
        super(NoiserSaltPepper, self).__init__("SaltAndPepper")
    def addNoise(self,image, amount):
        return lib.Noiser.addSaltAndPepper(image, amount)
    def getLevels(self):
        return [x * 0.005 for x in range(20+1)]

class NoiserGaussAdd(AbsNoiser):
    def __init__(self):
        super(NoiserGaussAdd, self).__init__("GaussAdditive")
    def addNoise(self,image, sigma):
        return lib.Noiser.addGausNoiseAdd(image, sigma)
    def getLevels(self):
        return [x * 1 for x in range(20+1)]

class NoiserGaussMul(AbsNoiser):
    def __init__(self):
        super(NoiserGaussMul, self).__init__("GaussMultiplicative")
    def addNoise(self,image, sigma):
        return lib.Noiser.addGausNoiseMul(image, sigma)
    def getLevels(self):
        return [x * 0.03 for x in range(7+1)]

class NoiserPoissonGaussian(AbsNoiser):
    def __init__(self):
        super(NoiserPoissonGaussian, self).__init__("PoissonGaussian")
    def addNoise(self,image, doNoise):
        return lib.Noiser.addPoissonGaussianNoise(image, doNoise)
    def getLevels(self):
        return [x/20.0 for x in range(21)]

# The ones below are not Noises, they are transformations,
# We are calling them Noisers because of the hierarchy
# TODO, fix naming for clarity

class NoiserTranslaterY(AbsNoiser):
    def __init__(self):
        super(NoiserTranslaterY, self).__init__("TranslateY")
    def addNoise(self,image, y):
        return lib.Noiser.translateImgY(image, y)
    def getLevels(self):
        return [-30 + x*2 for x in range(30+1)]

class NoiserScaler(AbsNoiser):
    def __init__(self):
        super(NoiserScaler, self).__init__("Scale")
    def addNoise(self,image,scale):
        return lib.Noiser.scaleImg(image, scale)
    def getLevels(self):
        return [x * 2 for x in range(20+1)]

class NoiserRotater(AbsNoiser):
    def __init__(self):
        super(NoiserRotater, self).__init__("Rotate")
    def addNoise(self,image, degree):
        return lib.Noiser.rotateImg(image, degree)
    def getLevels(self):
        levels =  [-30 + x*2 for x in range(30+1)]
        levels.extend([-0.5, -0.1, 0.1, 0.5])
        levels.sort()
        return levels

# ContrastInhomogeneity
class NoiserContrastInhomo(AbsNoiser):
    def __init__(self):
        super(NoiserContrastInhomo, self).__init__("ContrastInhomo")
    def addNoise(self,image, k):
        return lib.Noiser.addContrastInhomo(image, k)
    def getLevels(self):
        return [0.00001 + (0.00002*x) for x in range(5+1)]






