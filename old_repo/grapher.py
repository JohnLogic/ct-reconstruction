from matplotlib import pyplot as plt
from matplotlib import style
import numpy as np
import argparse
import shutil
import glob, os

def search_files(extension='', directory='.', limit = -1):
    print("{} {} {}".format(extension, directory, limit))
    extension = extension.lower()
    foundCount = 0
    foundPaths = []
    for dirpath, dirnames, files in os.walk(directory):
        for name in files:
            if extension and name.lower().endswith(extension):
                foundPath = os.path.join(dirpath, name)
                print("{}: {}".format(foundCount, foundPath))
                foundPaths.append(foundPath)
                foundCount += 1
                if (limit > -1 and foundCount >= limit):
                    break
    # foundPaths = foundPaths.sort(key=str.lower)
    return sorted(foundPaths)

# Find more styles in the link below:
# https://matplotlib.org/gallery/style_sheets/style_sheets_reference.html
plt.style.use('fast')
# plt.style.use('seaborn-whitegrid')

# construct the argument parse and parse the arguments

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--file", required=False,
	help=".csv file to graph")
ap.add_argument("-fs", "--files", required=False,
	help=".csv files separated by comma to graph")
ap.add_argument("-fdir", "--fileDir", required=False,
	help="directory containing .csv files to graph")
ap.add_argument("-ns", "--names", required=False,
	help="Score function names separated by comma")
ap.add_argument("-xn", "--xName", required=False,
        help="X axis name (example: Degree of rotation)")
ap.add_argument("-pn", "--pName", required=False,
        help="Plot name (shown on top of plot)")
ap.add_argument("-s", "--save", required=False,
        help="store output to png file", action="store_true")
ap.add_argument("-o", "--output", required=False,
        help="output file name")
ap.set_defaults(saveFlag = False)
args = vars(ap.parse_args())

fileName = args["file"]
fileNames = args["files"]
fileDir = args["fileDir"]
outputFileName = args["output"]

if (fileName == None and fileNames == None and fileDir == None):
    raise ValueError('--file or --files or --fileDir MUST be defined!')

saveFlag = False
if (args["save"] != None):
    saveFlag = True

xTicks = [] #Used for X axis ticks
yMaxValue = 1.0

# This becomes a numpy array
if (fileName != None):
    x,y = np.loadtxt(fileName,
                     unpack = True,
                     delimiter = ',')
    x = list(map(lambda v: float(v), x))
    xTicks = x
    plt.plot(sorted(x),y)
else:

    fileList = []
    if (fileDir != None):
        fileList = search_files(extension='.txt', directory=args["fileDir"])
        fileList = sorted(fileList)
        print("========== Sorted file names =========")
        for idx, fileName in enumerate(fileList):
        	print("{}: {}".format(idx, fileName))
    else:
        fileList = fileNames.split(',')

    

    legendList = []
    namesList = []
    if (args["names"] != None):
        print('NAMES: {}'.format(args["names"]))
        print('FILES: {}'.format(fileList))
        namesList = args["names"].split(',')
        if (len(namesList) != len(fileList)):
            raise ValueError("File list length {} is not equal to names list length {}".format(len(fileList), len(namesList)))


       
    for idx, fileName in enumerate(fileList):
        f = fileName.strip()
        x,y = np.loadtxt(f,
                             unpack = True,
                             delimiter = ',')
        if (max(y) > yMaxValue):
            yMaxValue = max(y)

        if (idx == 0):
            xTicks = x

        plt.plot(sorted(x),y)

        if (len(namesList) == 0):
            legendList.append(f)
        else:
            legendList.append(namesList[idx].strip())

    plt.legend(legendList, loc='lower left')
    # plt.xticks(xTicks)
    # fileName = fileNames #This is for PNG save mechanism
    fileName = "image"


# Removes grid from graph
# plt.grid(False)

# Setup X axis ticks
plt.axes().set_xticks(xTicks, minor=True)

# Setup Y axis ticks
yStart, yEnd = plt.axes().get_ylim()

yStart -= yStart % 0.1 #So that it starts from proper position
# yEnd -= yEnd % 0.1
yEnd = 1.01

# yStart, yEnd = 0.5, 1.01
print("START: {}, END: {}".format(yStart, yEnd))
plt.axes().set_yticks(np.arange(yStart, yEnd, 0.1))
# NOTE: we do not use yEnd BECAUSE it goes over maximum value and adds extra ticks

# Setup plot name (title)
if (args["pName"] != None):
    plt.title(args["pName"])
else:
    plt.title(fileName)

plt.ylabel('1 - Metric value')

# Setup x axis name
if (args["xName"] != None):
    plt.xlabel(args["xName"])
else:
    plt.xlabel('Noise level')

# Setup saving to file
print("SAVE FLAG IS: {}".format(saveFlag))

if (saveFlag):
    if (outputFileName != None):
        plt.savefig(outputFileName)
    else:
        plt.savefig(fileName+".png")
else:
    plt.show()


