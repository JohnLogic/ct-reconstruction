import numpy as np
import random
import string
import qrcode
import os
import cv2
from IPython import embed

def gen_img():
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
        )
    ran = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(32)])
    qr.add_data(ran)
    qr.make(fit=True)
    img = qr.make_image(fill_color="black", back_color="white")
    return (np.array(img)*255).astype(np.uint8)

FOLDER_TRAIN = 'fake/train'
if not os.path.exists(FOLDER_TRAIN):
    os.makedirs(FOLDER_TRAIN)
FOLDER_TEST = 'fake/test'
if not os.path.exists(FOLDER_TEST):
    os.makedirs(FOLDER_TEST)

for p in [FOLDER_TEST, FOLDER_TRAIN]:
    for j in range(100):
        w = np.random.random_integers(300, 1500)
        h = np.random.random_integers(300, 1500)
        tmp = gen_img()
        mark = cv2.resize(gen_img(), (w, h))
        img = np.random.random_integers(-128, 127, (h, w))
        final = np.minimum(np.minimum(mark.astype(np.float32) + img.astype(np.float32), 255.0), 0.0).astype(np.uint8)
        path = "{}/img_{}.png".format(p, j)
        cv2.imwrite(path, final)
#embed()
