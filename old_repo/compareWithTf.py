from old import imgLib as il
import numpy as np

# origPath = "tf/imgs/xray99.png"
# noisyPath = "tf/imgs/xray99_poisson-gauss.png"

origPath = "tf/imgs/xray/8.png"
noisyPath = "tf/imgs/xray-noisy/8.png"


origPath = origPath.strip()
noisyPath = noisyPath.strip()

orig = il.openImg(origPath).astype(np.uint8)
noisy = il.openImg(noisyPath).astype(np.uint8)

print("MSE = {}".format(il.AccCalc.calculateMSE(orig, noisy)) )
print("L1_Mod = {}".format(il.AccCalc.calculateL1_mod(orig, noisy)) )
print("SSIM = {}".format(il.AccCalc.calculateSSIM(orig, noisy)) )
print("WSSIMv1 = {}".format(il.AccCalc.calculateWSSIM(orig, noisy)) )
print("SLSSIMv5 = {}".format(il.AccCalc.calculateSLSSIMv5(orig, noisy, 16, True)) )


