#!/bin/bash

#Registered scorer algorithms keys: SSIM; SLSSIMv1; SLSSIMv2; MSE; SLSSIMv2_mod
#Registered noise algorithms: SaltAndPepper; GaussAdditive; GaussMultiplicative;


declare -a datasets=(/home/jobr2263/datasets/stanford /home/jobr2263/datasets/chest_xray_small
)

declare -a metrics=(MSE L1 MI wSSIMv3 SLSSIMv5_mod)

declare -a cals=(PoissonGaussian TranslateY Scale Rotate)

## now loop through the above array
for cs in "${cals[@]}"
do
    for ds in "${datasets[@]}"
    do
        for i in "${metrics[@]}"
        do
            python scorer_linas.py -d $ds -s $i -n $cs
            echo "$ds $i $cs" "is Done. \n"
        done
    done
done
#python sendy.py
