#!/bin/bash

#Registered scorer algorithms keys: SSIM; SLSSIMv1; SLSSIMv2; MSE; SLSSIMv2_mod
#Registered noise algorithms: SaltAndPepper; GaussAdditive; GaussMultiplicative;



#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SSIM -n GaussAdditive
#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SLSSIMv2 -n GaussAdditive

#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SSIM -n GaussMultiplicative
#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SLSSIMv2 -n GaussMultiplicative

#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SSIM -n SaltAndPepper
#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SLSSIMv2 -n SaltAndPepper

#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SSIM -n GaussAdditive -o boneSSIM_GaussAdd
#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv2 -n GaussAdditive -o boneSLSSIMv2_GaussAdd

#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SSIM -n GaussMultiplicative -o boneSSIM_GaussMul
#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv2 -n GaussMultiplicative -o boneSLSSIMv2_GaussMul

#Didn't do
#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SSIM -n SaltAndPepper -o boneSSIM_SaltPepper
#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv2 -n SaltAndPepper -o boneSLSSIMv2_SaltPepper

#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SLSSIMv2_mod -n GaussAdditive
#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv2_mod -n GaussAdditive

#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SLSSIMv2_mod -n GaussMultiplicative
#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv2_mod -n GaussMultiplicative

#TODO
#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SLSSIMv2 -n GaussMultiplicative
#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv2 -n GaussMultiplicative -o boneSLSSIMv2_GaussMul

# Stanford
#python scorer.py -d /home/jobr2263/datasets/stanford -s SSIM -n GaussAdditive
#python scorer.py -d  /home/jobr2263/datasets/stanford -s SSIM -n GaussMultiplicative
#python scorer.py -d /home/jobr2263/datasets/stanford -s SLSSIMv2_mod -n GaussAdditive
#python scorer.py -d /home/jobr2263/datasets/stanford -s SLSSIMv2_mod -n GaussMultiplicative

#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SLSSIMv3 -n GaussAdditive
#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SLSSIMv5 -n GaussAdditive
#python scorer.py -d /home/jobr2263/datasets/chest_xray_small -s SLSSIMv5_mod -n GaussAdditive

#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv3 -n GaussAdditive
#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv5 -n GaussAdditive
#python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv5_mod -n GaussAdditive

#python scorer.py -d /home/jobr2263/datasets/stanford -s SLSSIMv3 -n GaussAdditive
#python scorer.py -d /home/jobr2263/datasets/stanford -s SLSSIMv5 -n GaussAdditive
#python scorer.py -d /home/jobr2263/datasets/stanford -s SLSSIMv5_mod -n GaussAdditive

# PoissonGaussian, TranslateY, Scale, Rotate

python scorer.py -d /home/jobr2263/datasets/stanford -s SLSSIMv2_mod -n PoissonGaussian
python scorer.py -d /home/jobr2263/datasets/stanford -s SLSSIMv5_mod -n TranslateY
python scorer.py -d /home/jobr2263/datasets/stanford -s wSSIMv3 -n Scale
python scorer.py -d /home/jobr2263/datasets/stanford -s MI -n Rotate

python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv2_mod -n PoissonGaussian
python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s SLSSIMv5_mod -n TranslateY
python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s wSSIMv3 -n Scale
python scorer.py -d /home/jobr2263/datasets/boneage-test-dataset -s MI -n Rotate

python sendy.py
