import shutil
import glob, os
import argparse

def search_files(extension='', directory='.', limit = -1):
    print("{} {} {}".format(extension, directory, limit))
    extension = extension.lower()
    foundCount = 0
    foundPaths = []
    for dirpath, dirnames, files in os.walk(directory):
        for name in files:
            if extension and name.lower().endswith(extension):
                foundPath = os.path.join(dirpath, name)
                foundPaths.append(foundPath)
                foundCount += 1
                if (limit > -1 and foundCount >= limit):
                    break
                # print("WITH EXTENSION: {}".format(foundPath))
            # elif not extension:
                # pass
                # print("NO EXTENSION: {}".format(os.path.join(dirpath, name)))
                # foundPath = os.path.join(dirpath, name)
                # print(foundPath)
    return foundPaths

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dir", required=False,
	help="directory of files we want to copy (extract)")
ap.add_argument("-o", "--output", required=True,
	help="path to output directory to copy (extract) found files")
ap.add_argument("-l", "--limit", required=False,
	help="how many files we want to copy (extract)")
ap.add_argument("-e", "--extension", required=True,
	help="selected file extension")
args = vars(ap.parse_args())

fromDirectory = args["dir"]
if (fromDirectory == None):
    fromDirectory = '.'

outputDirectory = args["output"]

limit = args["limit"]
if (limit != None):
    limit = int(limit)
else:
    limit = -1

extension = args["extension"]
if (not(extension.startswith('.'))):
    extension = '.'+extension

# print("Script ended")
paths = search_files(extension, fromDirectory, limit)
print(paths)
for idx, path in enumerate(paths):
    shutil.copy2(path, outputDirectory+'/'+str(idx)+extension) 
