This is the folder for Tensorflow implementation of SLSSIM.

Start: 2019-02-01
Updated: 2019-02-16

# Environment setup

1. Install conda
2. Do ``conda create -n tf-2 tensorflow python=2`` (installs CPU-based version with python2)
3. Activate environment and do:
  - ``conda install opencv``
  - ``conda install scikit-image``
  - ``pip install imutils``

* _NOTE: after installation, it might be a good idea to reload your terminal and environment_

# Tensorflow usage notes

## Types
* casting from uint_8 to int32 is ok!
```python2
    for i in range(0, 257):
        uintVal = tf.constant(i, tf.uint8)
        intVal = tf.cast(uintVal, tf.int32)
        print("i = {}, uint = {}, int = {}".format(i, uintVal.eval(), intVal.eval()) )
```

