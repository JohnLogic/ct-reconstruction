from __future__ import print_function

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

import shutil
import glob, os

import tfData as td
import tfScorer as ts

def properTest():
    pass

#NOTE: this might not work as intended due to tf.gather and such
# Consider using a fixed size for Tensorflow
def eagerTest():
    tf.enable_eager_execution()

    filenameX = "imgs/xray99.png"
    filenameY = "imgs/xray99_poisson-gauss.png"

    x_decode = td.slowReadPngGray(filenameX)
    y_decode = td.slowReadPngGray(filenameY)

    # print(x_decode)
    print()
    # print(y_decode)

    result = ts.calcSLSSIMv5(x_decode, y_decode, 16)
    print("Better SLSSIM: {}".format(result) )

    newResult = ts.calcSLSSIMv5_old(x_decode, y_decode, 16)
    print("Old SLSSIM: {}".format(newResult) )

###### <MAIN> ######
eagerTest()
