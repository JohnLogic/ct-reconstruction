from __future__ import print_function

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

import shutil
import glob, os

# <GLOBAL> parameters

DEBUG = False

#######################################################

# OPEN IMAGE IN GRAYSCALE
# Reads to tensor flow file (probably slow)
# NOTE: USE ONLY FOR TESTING
def slowReadPngGray(filename):
    x = tf.read_file(filename) # file read image y
    x_decode = tf.image.decode_png(x, 1) # 1 - open in grayscale
    x_decode = tf.cast(x_decode, tf.int32)
    return x_decode

def testSetupDataPipeline(imgDirectory, noisyImgDirectory, batch_size = 1, fileLimit = -1):

    filenames = search_files('png', imgDirectory, fileLimit)
    noisyFileNames = search_files('png', noisyImgDirectory, fileLimit)
    number_of_files = len(filenames)

    with tf.device('/cpu:0'):
        dataset = tf.data.Dataset.from_tensor_slices((filenames, noisyFileNames))
        dataset = dataset.shuffle(number_of_files)
        dataset = dataset.map(parseFoo, num_parallel_calls=4)
        dataset = dataset.map(preprocessFoo, num_parallel_calls=4)
        dataset = dataset.batch(batch_size)
        dataset = dataset.prefetch(batch_size)
        return dataset, number_of_files, max(1, int(np.floor(number_of_files / batch_size)))

def SetupCompressionDataPipeline(imgDirectory, batch_size = 1, fileLimit = -1):

    filenames = search_files('png', imgDirectory, fileLimit)
    number_of_files = len(filenames)

    with tf.device('/cpu:0'):
        dataset = tf.data.Dataset.from_tensor_slices(filenames)
        dataset = dataset.shuffle(number_of_files)
        dataset = dataset.map(openImgs, num_parallel_calls=4)
        #dataset = dataset.map(preprocessFoo, num_parallel_calls=4)
        dataset = dataset.batch(batch_size)
        dataset = dataset.prefetch(batch_size)
        return dataset, number_of_files, max(1, int(np.floor(number_of_files / batch_size)))

def openImgs(orig1, is_testing = True):
    return _openImg(orig1, is_testing)

def parseFoo(orig1, orig2):
    return (_openImg(orig1), _openImg(orig2) )

def _openImg(imgFn, is_testing = False):
    imgStr = tf.read_file(imgFn)
    # Don't use tf.image.decode_image, or the output shape will be undefined
    img = tf.image.decode_png(imgStr, 3)
    # This will convert to float values in [0, 1] <- only if float32
    # img = tf.image.convert_image_dtype(img, tf.int32)

    if (is_testing):
        img = tf.image.resize_images(img, [512, 512])

    img = tf.cast(img, tf.int32)
    # img = tf.Print(img, [img, tf.shape(img)], "IMG: ")

    return img

def preprocessFoo(orig1, orig2):
    return (orig1, _addTestAugmentation(orig2) )

def _addTestAugmentation(image):
    # image = tf.image.random_flip_left_right(image)
    # image = tf.image.random_brightness(image, max_delta=32.0 / 255.0)
    # # image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
    # image = tf.clip_by_value(image, 0.0, 255.0)
    return image

def search_files(extension='', directory='.', limit = -1):
    print("{} {} {}".format(extension, directory, limit))
    extension = extension.lower()
    foundCount = 0
    foundPaths = []
    for dirpath, dirnames, files in os.walk(directory):
        for name in files:
            if extension and name.lower().endswith(extension):
                foundPath = os.path.join(dirpath, name)
                foundPaths.append(foundPath)
                foundCount += 1
                if (limit > -1 and foundCount >= limit):
                    break
                # print("WITH EXTENSION: {}".format(foundPath))
            # elif not extension:
                # pass
                # print("NO EXTENSION: {}".format(os.path.join(dirpath, name)))
                # foundPath = os.path.join(dirpath, name)
                # print(foundPath)

    #print("Found paths: {}".format(foundPaths) )
    return foundPaths

################################################################################
