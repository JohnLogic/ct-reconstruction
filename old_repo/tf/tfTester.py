from __future__ import print_function

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

import shutil
import glob, os

import tfData as td
import tfScorer as ts

# TESTING FROM EXAMPLE
# https://www.tensorflow.org/api_docs/python/tf/image/ssim

DEBUG = False

#######################################################
########################## ATTEMPT WITH DATA API ###############################
def attemptDataset():

	# How many images do we want to measure the SSIM for
    RUN_COUNT = 5
    BATCH_SIZE = 1

    dataset = td.testSetupDataPipeline('imgs/xray', 'imgs/xray-noisy',
                                      batch_size = BATCH_SIZE,
                                      fileLimit = RUN_COUNT)
    iterator = dataset.make_one_shot_iterator()

    sess = tf.InteractiveSession()

    next_element = iterator.get_next()

    scores = []

    ####################################

    i = 0

    while True:
        try:
            i += 1
            img1, img2 = sess.run(next_element)
            # l1_modXY = calcL1_mod(img1, img2)

            # slssimXY = ts.calcSLSSIMv5(img1, img2, 16) 
            slssimXY = ts.calcSLSSIMv5(img1, img2, 16) 
            slssimXY = tf.Print(slssimXY, [i, slssimXY], "SLSSIM: ")

            scores.append(slssimXY)
            if (RUN_COUNT == i):
                break
        except tf.errors.OutOfRangeError:
            break

    scoresT = tf.stack(scores)
    scoreAvgT = tf.reduce_mean(scoresT)
    
    print("Score average: {}".format(scoreAvgT.eval()) )

    sess.close()
    
#######################################################
#######################################################

def _files():
    return {
        "chest99": "imgs/xray99.png",
        "chest99-pg": "imgs/xray99_poisson-gauss.png"
    }

def showTensor(tensor):
    tensor = tensor.eval()
    arr = np.ndarray(tensor)#This is your tensor
    arr_ = np.squeeze(arr) # you can give axis attribute if you wanna squeeze in specific dimension
    plt.imshow(arr_)
    plt.show()
    
################################################################################
################################################################################
################################################################################

# def testConcatOfTensors():
#     firstTensor = tf.constant(1)
#     secondTensor = tf.constant(2)
# 
#     L = []
#     value = 5
#     L.append(1)
#     L.append(value / 2)
#     L.append(value ** 2)
#     L.append(-100)
# 
#     return tf.constant(L)
# 
#     listTensor = tf.concat(firstTensor, secondTensor)
#     return listTensor
# 
# 
# def oldAttempt():
#     filename_queue = tf.train.string_input_producer(['/Users/jonas/repos/uni/xray-sublevel-structure-similarity/tf/xray99.png']) #  list of files to read
#     reader = tf.WholeFileReader()
#     key, value = reader.read(filename_queue)
# 
#     init_op = tf.global_variables_initializer()
# 
#     # Read images from file.
# 
#     im1 = tf.image.decode_png(value, 1)
#     im2 = tf.image.decode_png(value, 1)
# 
#     sess = tf.InteractiveSession() #START SESSION
#     print("Shape of image: {}".format(tf.shape(im1).eval() ) )
# 
#     # Compute SSIM over tf.uint8 Tensors.
#     ssim1 = tf.image.ssim(im1, im2, max_val=255)
#     resultSSIM = ssim1.eval()
#     print("Resulting SSIM: {}".format(resultSSIM) )
# 
#     sess.close()



#######################################################
#### <<<< MAIN >>>>
#######################################################

# newAttempt()
attemptDataset()

