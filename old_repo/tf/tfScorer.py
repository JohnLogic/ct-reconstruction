from __future__ import print_function

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

# <GLOBAL> parameters

DEBUG = False
MAX_INTENSITY = 255

# Returns tensor of dimensions (shape)
def shapeX(t, testing = False):
    # The tensor SHOULD BE [x,y,value_per_pixel]
    position = 1
    if (testing):
        position = 0
    return tf.gather(tf.shape(t), position)

def shapeY(t, testing = False):
    # The tensor SHOULD BE [x,y,value_per_pixel]
    position = 2
    if (testing):
        position = 1
    return tf.gather(tf.shape(t), position)

def calcL1(t1,t2):
    # t1m = tf.cast(t1, tf.int32)
    # t2m = tf.cast(t2, tf.int32)
    # if (DEBUG):
        # t1m = tf.Print(t1m, [t1m], "L1 t1 stuff: ", summarize = 1026)
        # t2m = tf.Print(t2m, [t2m], "L1 t2 stuff: ", summarize = 1026)

    subResultT = tf.subtract(t1, t2)
    # if (DEBUG):
        # subResultT = tf.Print(subResultT, [subResultT], "L1 subResultT: ", summarize = 1026)
    absResultT = tf.abs(subResultT)
    # if (DEBUG):
        # absResultT = tf.Print(absResultT, [absResultT], "L1 absResultT: ", summarize = 1026)
    return tf.reduce_sum(absResultT)


def calcL1_mod(t1,t2):
    # width = tf.gather(tf.shape(t1), 0)
    # height = tf.gather(tf.shape(t1), 1)
    width = shapeX(t1)
    height = shapeY(t1)

    l1T = calcL1(t1,t2)
    # if (DEBUG):
        # l1T = tf.Print(l1T, [l1T], "L1T orig = ")
    l1T = tf.cast(l1T, tf.float32)
    # if (DEBUG):
        # l1T = tf.Print(l1T, [l1T], "L1T = ")

    constOneT = tf.constant(1.0)

    # Maximum value for normalization
    maxValT = tf.multiply(tf.constant(MAX_INTENSITY), (height))
    maxValT = tf.multiply(maxValT, (width))
    maxValT = tf.cast(maxValT, tf.float32)

    # if (DEBUG):
        # maxValT = tf.Print(maxValT, [l1T, maxValT], "L1T, height*width*255: ")

    dividedT = tf.divide(l1T, maxValT)
    return tf.subtract(constOneT, dividedT)

# Calcs SSIM from two tensors (works with Gray only)
def calcSSIM(t1,t2):
    #max_val - if two are identical via SSIM
    ssimXY = tf.image.ssim(t1,t2, max_val = 1.0)
    # NOTE: test reduce_mean correctness(!) with multidimensional data
    return tf.reduce_mean(ssimXY) 

def calcMSE(t1,t2):
    # t1 - labels, t2 - predictions (orig, noisy))
    t1Int32 = tf.cast(t1, tf.int32)
    t2Int32 = tf.cast(t2, tf.int32)
    mse = tf.metrics.mean_squared_error(t1Int32, t2Int32)
    return mse

############ BETTER SLSSIM IMPL ###############

def slssim_while_cond(t1, t2, i, iters, step, acc):
    return tf.less(i,iters)

def slssim_while_body(t1, t2, i, iters, step, acc):

    # start = tf.constant(i*step)
    start = tf.multiply(i, step)
    # end = tf.constant(step*(i+1) - 1)
    end = tf.subtract(tf.multiply(step, tf.add(i, 1)), 1)
    # print("Start: {}, end: {}".format(start,end) )

    rangedT1 = tf.clip_by_value(t1, start, end)
    rangedT2 = tf.clip_by_value(t2, start, end)
    score = calcSSIM(rangedT1, rangedT2)

    return [t1, t2, tf.add(i,1), iters, step, tf.multiply(acc, score)]

# NOTE: this SLSSIM is much "cleaner" and intended for optimization
# TODO: look into types, perhaps float32 is not enough
def calcSLSSIMv5(t1, t2, n):

    stepPy = (MAX_INTENSITY+1)/n
    step = tf.constant(stepPy, name = 'step')

    n = tf.convert_to_tensor(n, dtype=tf.int32, name = 'iters')
    acc = tf.constant([1], tf.float32, name = 'initialAcc') #NOTE [ ] is necessary for now
    zero = tf.constant(0, name = 'zero')

    _,_,_,_,_,res = tf.while_loop(
        slssim_while_cond,
        slssim_while_body,
        [t1, t2, 0, n, step, acc],
        # shape_invariants=[tf.shape(t1),
                          # tf.shape(t2),
                          # tf.shape(n),
                          # tf.shape(n),
                          # tf.shape(step),
                          # tf.shape(acc) ]
    )
    # print("Res: {}".format(res) )
    l1T = calcL1_mod(t1,t2)
    # print("L1_mod: {}".format(l1T) )
    product = tf.multiply(l1T, res)
    # print("Product: {}".format(product) )
    power = tf.reciprocal(tf.cast(n+1, tf.float32) )
    # print("Power: {}".format(power) )
    return tf.pow(product, power)

############## ############### ####################

def calcSLSSIMv5_old(t1,t2,n):
    # n - how many levels should be separately calculated
    # TODO: make this into one single tensor (basically root at the end)
    m = 255 #max intensity value

    scoreList = []
    step = (m+1)/n
    actualCount = 0
    for i in range(0, n):
        actualCount += 1
        start = tf.constant(i*step)
        end = tf.constant(step*(i+1) - 1)
        rangedT1 = tf.clip_by_value(t1, start, end)
        rangedT2 = tf.clip_by_value(t2, start, end)
        # showTensor(rangedT1)
        # showTensor(rangedT2)

        # show2Imgs(rangedImageA, rangedImageB, True)

        bscoreT = (calcSSIM(rangedT1, rangedT2) ) # << SSIM yields different results!
        scoreT = tf.identity(bscoreT)

        if (DEBUG):
            scoreT = tf.Print(scoreT, [start, end, scoreT], "start, end, ssim: ")

        # print("SLSSIM i: {}, start: {}, end: {} | {}".format(i, start, end, rangeScore))
        scoreList.append(scoreT)
    scoreListT = tf.stack(scoreList)
    # scoreListT = tf.Print(scoreListT, [scoreListT]) #DEBUG scoreListT
    # l1 = AccCalc.calculateL1_mod(imageA, imageB)
    # l1T = calcL1_mod(t1,t2)
    l1T = calcL1_mod(t1,t2)
    if (DEBUG):
        l1T = tf.Print(l1T, [l1T], "L1_mod = ")

    # btotalListT = scoreListT + l1TList # NOTE: this will just add element wise - unbelievable
    # totalListT = tf.identity(btotalListT)
    # totalListT = tf.Print(totalListT, [totalListT])

    scoreProductT = tf.reduce_prod(scoreListT)
    productT = tf.multiply(l1T, scoreProductT)

    # powerT = tf.reciprocal(tf.constant(actualCount) )
    if (DEBUG):
        print("Actual count: {}".format(actualCount+1) )

    powerT = tf.reciprocal(tf.constant(actualCount+1, tf.float32) ) #TODO uncomment
    if (DEBUG):
        powerT = tf.Print(powerT, [productT, powerT])

    result = tf.pow(productT, powerT)
    if (DEBUG):
        result = tf.Print(result, [result])
    
    # return (product ** (1/float(actualCount)))
    return result
