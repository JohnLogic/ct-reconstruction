"""Train the medical compression model."""

import tensorflow as tf
from IPython import embed
from old.tf import tfData as td

#from tf.tfCompressionModel import LSTMCNN

FLAGS = tf.app.flags.FLAGS

# Configs
tf.app.flags.DEFINE_string('train_dir', None,
                           """Directory where to write event logs.""")

tf.app.flags.DEFINE_integer('epochs', 1,
                           """The number of epochs""")

tf.app.flags.DEFINE_integer('batch_size', 32,
                           """Batch size for evaluation.""")

tf.app.flags.DEFINE_string('summary_dir', "logs",
                           """The folder for experiments info.""")

tf.app.flags.DEFINE_float('lr', 1e-3,
                           """initial learning rate.""")

opts = tf.app.flags.FLAGS

def train():
    if opts.train_dir is None:
        raise ValueError('Parameter train_dir must be provided with --train_dir')
    tf.logging.set_verbosity(tf.logging.INFO)

    dataset, _, batches_per_epoch = td.SetupCompressionDataPipeline('fake/train',
                                      batch_size = opts.batch_size)

    dataset = dataset.repeat()
    iterator = dataset.make_one_shot_iterator()
    next_batch = iterator.get_next()
    training_init_op = iterator.make_initializer(dataset)
    init_g = tf.global_variables_initializer()
    init_l = tf.local_variables_initializer()


    #model = LSTMCNN(opts.batch_size)
    #train_loss = model.get_loss()
    #train_op = tf.train.AdamOptimizer(opts.lr).minimize(train_loss)

    o = tf.maximum(1, 2)
    tf.summary.scalar('test', o)
    #te = next_batch
    #tf.summary.image('real', te)
    merged = tf.summary.merge_all()



    with tf.Session() as sess:
        train_writer = tf.summary.FileWriter(opts.summary_dir, sess.graph)
        sess.run(init_g)
        sess.run(init_l)
        sess.run([training_init_op])
        #tf.variables_initializer(
        #    [v for v in tf.global_variables() if v.name.split(':')[0] in set(sess#.run(tf.report_uninitialized_variables()))
        #])

#        print(opts.epochs)
#        print(batches_per_epoch)
        for epoch in range(opts.epochs):
            print(".")
            for step in range(batches_per_epoch):
                img_batches = sess.run(next_batch)
                #img1, summary, cur_train_loss, _ = sess.run([next_batch, merged, train_loss, train_op], feed_dict={model.inputs: img_batches})
                #train_writer.add_summary(summary, epoch * batches_per_epoch + step)
                #print(cur_train_loss)
                #slssimXY = ts.calcSLSSIMv5(img1, img1, 16)
                #val = sess.run(slssimXY)
                #print(img1.shape, val.shape)
        embed()





def main(argv=None):
    train()


if __name__ == '__main__':
    tf.app.run()
