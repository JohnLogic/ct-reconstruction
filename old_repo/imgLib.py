# import the necessary packages
from skimage.measure import compare_ssim
import argparse
import imutils
import cv2
import numpy as np
import math
from matplotlib import pyplot as plt

class Transformer:

    def toGrayscale(image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return gray

class AccCalc:

    def fCalculateSSIM(fle1, fle2):
        # load the two input images
        imageA = cv2.imread(fle1)
        imageB = cv2.imread(fle2)
        return AccCalc.calculateSSIM(imageA, imageB)

    # Calculates SSIM
    def calculateSSIM(imageA, imageB, sourceIsGrayscale = True):
        grayA = imageA
        grayB = imageB

        # convert the images to grayscale
        if (not(sourceIsGrayscale)):
            grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
            grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)

        # compute the Structural Similarity Index (SSIM) between the two
        # images, ensuring that the difference image is returned
        #(score, diff) = compare_ssim(grayA, grayB, full=True)
        #diff = (diff * 255).astype("uint8")
        score = compare_ssim(grayA, grayB, full=False)

        # print("SSIM: {}".format(score))
        return score

    def calculateSLSSIM(imageA, imageB, step = 1, sourceIsGrayscale = True):
        m = 255 #max intensity value
        product = float(1)
        actualCount = 0
        for i in range(0, m+1, step):
            for j in range(i, m, step):
                actualCount += 1
                rangedImageA = np.clip(imageA, i, j)
                rangedImageB = np.clip(imageB, i, j)
                rangeScore = AccCalc.calculateSSIM(rangedImageA, rangedImageB, sourceIsGrayscale)
                # print("SLSSIM i: {}, j: {} | {}".format(i, j, rangeScore))
                product *= rangeScore
                del rangedImageA
                del rangedImageB
        return product**(1/float(actualCount))

    ## TODO ask about which is better this or original
    def calculateSLSSIM_mod(imageA, imageB, step = 1, sourceIsGrayscale = True):
        m = 255 #max intensity value
        product = float(1)
        actualCount = 0
        initialStep = step-1 #for j, so that interval size is of size step (eg step 32 = [0;31])
        for i in range(0, m+1, step):
            for j in range(i+initialStep, m+1, step): #m+1 gives [i;255]
                actualCount += 1
                rangedImageA = np.clip(imageA, i, j)
                rangedImageB = np.clip(imageB, i, j)
                # if (j == 255):
                    # show2Imgs(rangedImageA, rangedImageB, True)
                rangeScore = AccCalc.calculateSSIM(rangedImageA, rangedImageB, sourceIsGrayscale)
                # print("SLSSIM i: {}, j: {} | {}".format(i, j, rangeScore))
                product *= rangeScore
                del rangedImageA
                del rangedImageB
        return product**(1/float(actualCount))

    ## TODO consider merging methods into one, lots of repeating code
    def calculateSLSSIMv3(imageA, imageB, step = 1, sourceIsGrayscale = True):
        m = 255 #max intensity value
        resultSum = 0
        actualCount = 0
        initialStep = step-1 #for j, so that interval size is of size step (eg step 32 = [0;31])
        for i in range(0, m+1, step):
            for j in range(i+initialStep, m+1, step): #m+1 gives [i;255]
                actualCount += 1
                rangedImageA = np.clip(imageA, i, j)
                rangedImageB = np.clip(imageB, i, j)
                # if (j == 255):
                    # show2Imgs(rangedImageA, rangedImageB, True)
                ssim = AccCalc.calculateSSIM(rangedImageA, rangedImageB, sourceIsGrayscale)
                # NOTE: log(1) = 0, perfect match means 0 
                rangeScore = ssim * math.log(ssim) #SSIM * log(SSIM)
                # print("SLSSIM i: {}, j: {} | {} {}".format(i, j, ssim,  rangeScore))
                resultSum += rangeScore
                del rangedImageA
                del rangedImageB
        if (resultSum == 0): return 0.0 #SMALL HACK TO NOT SEE -0.0
        return -1 * resultSum

    def calculateSLSSIMv5(imageA, imageB, n, takeRoot = False, sourceIsGrayscale = True):
        # n - how many levels should be separately calculated
        m = 255 #max intensity value
        product = float(1)
        step = (m+1)/n
        actualCount = 0
        for i in range(0, n):
            actualCount += 1
            start = i*step
            end = step*(i+1) - 1
            rangedImageA = np.clip(imageA, start, end)
            rangedImageB = np.clip(imageB, start, end)

            # show2Imgs(rangedImageA, rangedImageB, True)

            rangeScore = AccCalc.calculateSSIM(rangedImageA, rangedImageB, sourceIsGrayscale)
            # print("SLSSIM i: {}, start: {}, end: {} | {}".format(i, start, end, rangeScore))
            product *= rangeScore
            del rangedImageA
            del rangedImageB
        l1 = AccCalc.calculateL1_mod(imageA, imageB) #TODO uncomment this after test
        actualCount += 1
        product *= l1
        if takeRoot:
            return (product ** (1/float(actualCount)))
        else:
            return product


    def calculateMSE(imageA, imageB):
        # LINK: https://www.pyimagesearch.com/2014/09/15/python-compare-two-images/
        # the 'Mean Squared Error' between the two images is the
	# sum of the squared difference between the two images;
	# NOTE: the two images must have the same dimension
        # err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
        err = np.sum( cv2.subtract(imageA.astype(np.int32), imageB.astype(np.int32)) ** 2)
        err /= float(imageA.shape[0] * imageA.shape[1])
        err /= float(255**2)
        err = 1.0 - err

	# return the MSE, the lower the error, the more "similar"
	# the two images are
        return err

    def calculateL1(imageA, imageB):
        #NOTE: we use OpenCV2 subtract because of UNSIGNED arithmetic of uint8
        arr = np.absolute(cv2.subtract(imageA.astype(np.int32), imageB.astype(np.int32) ))
        return np.sum(arr)

    def calculateL1_mod(imageA, imageB):
        width,height = imageA.shape
        #NOTE: it is assumed that we will use L1 for same dimension imgs
        l1 = AccCalc.calculateL1(imageA, imageB)
        return 1.0 - (1.0*l1 / (255*height*width))

    def calculateWSSIMPaper(imageA, imageB, isGrayscale = True):
        return 1.0 - calculateL1_mod(imageA, imageB) * calculateSSIM(imageA, imageB, isGrayscale)

    def calculateWSSIM(imageA, imageB, isGrayscale = True):
        width,height = imageA.shape
        s1 = AccCalc.calculateL1(imageA, imageB) * (1 - AccCalc.calculateSSIM(imageA, imageB, isGrayscale))
        return 1.0 - (1.0*s1 / (255*height*width))

    # NOTE: Code is based on calculateSLSSIM_mod
    def calculateWSSIMv2(imageA, imageB, step = 1, sourceIsGrayscale = True):
        m = 255 #max intensity value
        product = float(1)
        actualCount = 0
        initialStep = step-1 #for j, so that interval size is of size step (eg step 32 = [0;31])
        for i in range(0, m+1, step):
            for j in range(i+initialStep, m+1, step): #m+1 gives [i;255]
                actualCount += 1
                rangedImageA = np.clip(imageA, i, j)
                rangedImageB = np.clip(imageB, i, j)
                # if (j == 255):
                    # show2Imgs(rangedImageA, rangedImageB, True)
                rangeScore = AccCalc.calculateWSSIM(rangedImageA, rangedImageB, sourceIsGrayscale)
                # print("SLSSIM i: {}, j: {} | {}".format(i, j, rangeScore))
                product *= rangeScore
                del rangedImageA
                del rangedImageB
        return product**(1/float(actualCount))

    # NOTE: Code is based on calculateSLSSIMv5
    def calculateWSSIMv3(imageA, imageB, n, sourceIsGrayscale = True):
        # n - how many levels should be separately calculated
        m = 255 #max intensity value
        product = float(1)
        step = (m+1)/n
        actualCount = 0
        for i in range(0, n):
            actualCount += 1
            start = i*step
            end = step*(i+1) - 1
            rangedImageA = np.clip(imageA, start, end)
            rangedImageB = np.clip(imageB, start, end)

            rangeScore = AccCalc.calculateWSSIM(rangedImageA, rangedImageB, sourceIsGrayscale)
            # print("SLSSIM i: {}, start: {}, end: {} | {}".format(i, start, end, rangeScore))
            product *= rangeScore
            del rangedImageA
            del rangedImageB
        return (product ** (1/float(actualCount)))

    # MI - Mutual information
    # NOTE: must be grayscale in this impl
    def calculateMI(imageA, imageB):
        iA = np.true_divide(imageA, 255)
        iB = np.true_divide(imageB, 255)
        iA = np.clip(iA, 0.01, 1)
        iB = np.clip(iB, 0.01, 1)
        return -1 * np.sum(np.multiply(iA,np.log(iB)))

    # TODO: maybe 1.0 is unnecessary?
    def calculateDi(imageA, imageB):
        mi = AccCalc.calculateMI(imageA, imageB)
        hImgA = AccCalc.calculateMI(imageA, imageA)
        hImgB = AccCalc.calculateMI(imageB, imageB)
        return 1.0 - (1.0 * mi)/max(hImgA, hImgB)

class Noiser:

    def img(fle):
        return cv2.imread(fle)

    def addGausNoiseAdd(image, sigma, sourceIsGrayscale = True):
        # Image + N(1, sigma)
        newImage = image.copy()

        mean = 0
        row = None
        col = None
        ch = None
        gauss = None

        if (sourceIsGrayscale):
            row,col = newImage.shape
            gauss = np.random.normal(mean,sigma,(row,col))
            gauss = gauss.reshape(row,col)
        else:
            row,col,ch= newImage.shape
            gauss = np.random.normal(mean,sigma,(row,col,ch))
            gauss = gauss.reshape(row,col,ch)

        # return np.clip( (newImage + gauss).astype('uint8'), 0, 255)
        return np.clip( (newImage + gauss), 0, 255)

    def addGausNoiseMul(image, sigma, sourceIsGrayscale = True):
        # Image + N(1, sigma)
        newImage = image.copy()

        mean = 1
        row = None
        col = None
        ch = None
        gauss = None

        if (sourceIsGrayscale):
            row,col = newImage.shape
            gauss = np.random.normal(mean,sigma,(row,col))
            gauss = gauss.reshape(row,col)
        else:
            row,col,ch= newImage.shape
            gauss = np.random.normal(mean,sigma,(row,col,ch))
            gauss = gauss.reshape(row,col,ch)

        return np.clip( (newImage * gauss), 0, 255)



        # m = mean
        # s = variance
        # return cv2.randn(newImage,m,s);

    # Adds salt and pepper noise
    # amount = amount of image affected, interval (0 ; 1)
    # saltVsPepper = balance between salt and pepper
    # Recommended step would be 0.005 until maybe like 0.1
    def addSaltAndPepper(img, amount, saltVsPepper = 0.5):
        image = img.copy()
        # row,col,ch = image.shape
        s_vs_p = saltVsPepper
        out = image
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
                  for i in image.shape]
        out[coords] = 1

        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
                  for i in image.shape]
        out[coords] = 0
        return out

    # Based on article
    # S. Lee, M.S. Lee, M.G. Kang
    # " Poisson-Gaussian Noise Analysis and Estimation for
    #   Low-dose X-ray Images in the NSCT Domain "
    def addPoissonGaussianNoise(image, b = 1.0/255.0, a = 0, doNoise = 1):
        # We will use normal distribution here
        # N(0, 32.5125 + 0.0844 * pixelIntensity)
        # TODO: figure out a way to parametrize this?
        if (doNoise != 1):
            return image.copy()
        # noiseArray = image.copy() #NOTE: no need to copy image here 32.5125 = 0.0844
        noiserPG = lambda px: px + np.random.normal(0, a + b*px)
        noiseArray = np.array([noiserPG(pixel) for pixel in image])
        noisy = np.clip(noiseArray, 0, 255)
        # show2Imgs(image, noisy) # TODO for testing
        return noisy

    def scaleImg(image, scale):
        other = image.copy()
        return cv2.resize(other, None, fx=scale, fy=scale, interpolation = cv2.INTER_LINEAR)

    def translateImgY(image, y):
        rows, cols = image.shape
        M = np.float32([[1,0,0],[0,1,y]])
        return cv2.warpAffine(image, M, (cols,rows))

    def rotateImg(image, degree):
        rows, cols = image.shape
        M = cv2.getRotationMatrix2D((cols/2, rows/2),degree,1)
        return cv2.warpAffine(image,M,(cols,rows))

    # k1 in [0.0001 ; 0.0004] << FROM ARTICLE
    # Try 0.00001 to 0.0001 << FROM PRACTICE
    def addContrastInhomo(image, k1):
        x,y = image.shape
        noisy = image.copy()
        xc = x/2 + x%2
        yc = y/2 + y%2
        for ix in range(x):
            for iy in range(y):
                xy = noisy[iy,ix]
                # TODO: this returns all 0s CUZ problems with floats
                noisy[iy,ix] = float(xy) / float(1.0+k1 * ((ix-xc)**2 + (iy-yc)**2))
                # noisy[iy,ix] = 2.0
                # print("ORIG = {}, NEW = {}, xc = {}, yc = {}, ix = {}, iy = {}, k1 = {}".format(xy, noisy[iy,ix],xc,yc,ix,iy,k1))
        return np.clip(noisy, 0, 255)

    def addGrain(image, degree):
        pass

    def reduceContrast(image, degree):
        pass

    # Adds blurring
    # degree = size of blur rectangle (width and height)
    # On higher resolution pictures, it becomes noticeable at about 5-6
    # Fairly blurry on about 15-20
    def addBlur(image, degree = 5):
        blur = cv2.blur(image,(degree, degree))
        return blur

    # file version of addBlur
    def fAddBlur(fle, degree):
        return Noiser.addBlur(img(fle), degree)

def openImg(imgPath, mode = 0):
    #NOTE there is no need to do .astype( <int> ) conversions
    #We use openCV2 arithmetic subtract to counteract uint8
    return cv2.imread(imgPath, mode)

def saveImg(imgPath, img):
    return cv2.imwrite(imgPath, img)

def showImg(img, grayscale = True):
    if (not(grayscale)):
        plt.subplot(121),plt.imshow(img),plt.title('Image')
        plt.xticks([]), plt.yticks([])
        plt.show()
    else:
        plt.subplot(121),plt.imshow(img, cmap='gray'),plt.title('Image')
        plt.xticks([]), plt.yticks([])
        plt.show()


def show2Imgs(img1, img2, grayscale = True, img1Title = 'img1', img2Title = 'img2'):
    if (not(grayscale)):
        plt.subplot(121),plt.imshow(img1),plt.title(img1Title)
        plt.xticks([]), plt.yticks([])
        plt.subplot(122),plt.imshow(img2),plt.title(img2Title)
        plt.xticks([]), plt.yticks([])
        plt.show()
    else:
        plt.subplot(121),plt.imshow(img1, cmap='gray'),plt.title(img1Title)
        plt.xticks([]), plt.yticks([])
        plt.subplot(122),plt.imshow(img2, cmap='gray'),plt.title(img2Title)
        plt.xticks([]), plt.yticks([])
        plt.show()



testImgPath = "xray-dataset/orig/0.png"

def openTestImg():
    return openTestImgGray()

def openTestImgColor():
    #NOTE: this is named like this to avoid getting RGB image
    #      by accident (we work with grayscale images)
    origImg = openImg(testImgPath)
    return origImg

def openNoisyTestImg():
    image = openTestImg()
    noisy = Noiser.addPoissonGaussianNoise(image)
    return noisy

def openTestImgGray():
    return cv2.imread(testImgPath, 0)

# MAIN
"""
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--first", required=True,
	help="first input image")
# ap.add_argument("-s", "--second", required=True,
	# help="second")
args = vars(ap.parse_args())

# CALCULATING SSIMS
# AccCalc.fCalculateSSIM(args["first"], args["second"])

img1Path = args["first"]
img1 = Noiser.img(img1Path)
img2 = Noiser.img(img1Path) #Just so the code runs even if img2 isn't provided
# showImg(img1) #Show just one image

# img2 = Noiser.addBlur(img1, 25) #blur second image
# img2 = Noiser.addGausNoise(img1, 1, 0.25)
img2 = Noiser.addSaltAndPepper(img1, 0.1, 0.5)

show2Imgs(img1,img2)
"""

