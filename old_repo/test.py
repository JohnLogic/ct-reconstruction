from old import imgLib as il
import numpy as np
from skimage.measure import compare_ssim as ssim

def addGauss(sigma):
    grayImage = il.openTestImgGray()
    noisy = il.Noiser.addGausNoiseAdd(grayImage, sigma)
    il.show2Imgs(grayImage, noisy, True)

def mulGauss(sigma):
    grayImage = il.openTestImgGray()
    noisy = il.Noiser.addGausNoiseMul(grayImage, sigma)
    il.show2Imgs(grayImage, noisy, True)

def test():
    grayImage = il.openTestImgGray()
    score = il.AccCalc.calculateSLSSIM_mod(grayImage, grayImage, 32)
    print("Score: {}".format(score))
    pass

def testSLSSIMv3():
    grayImage = il.openTestImgGray()
    score = il.AccCalc.calculateSLSSIMv3(grayImage, grayImage, 32)
    print("Score: {}".format(score))

def testSLSSIMv5(n = 4):
    grayImage = il.openTestImgGray()
    score = il.AccCalc.calculateSLSSIMv5(grayImage, grayImage, n)
    print("Score: {}".format(score))

def img():
    return il.openTestImgGray()

def getChestXrays():
    origPath = "/Users/jonas/repos/uni/xray-sublevel-structure-similarity/tf/imgs/xray99.png"
    noisyPath = "/Users/jonas/repos/uni/xray-sublevel-structure-similarity/tf/imgs/xray99_poisson-gauss.png"
    orig = il.openImg(origPath).astype(np.int32)
    noisy = il.openImg(noisyPath).astype(np.int32)

#     noisy = orig
    print("SSIM = {}".format(il.AccCalc.calculateSSIM(orig, noisy)) )
    print("SLSSIMv5 = {}".format(il.AccCalc.calculateSLSSIMv5(orig, noisy, 8, True)) )
    print("SLSSIMv5 16 = {}".format(il.AccCalc.calculateSLSSIMv5(orig, noisy, 16, True)) )

    print("WSSIMv1 = {}".format(il.AccCalc.calculateWSSIM(orig, noisy)) )
    print("L1 = {}".format(il.AccCalc.calculateL1(orig, noisy)) )
    print("L1_Mod = {}".format(il.AccCalc.calculateL1_mod(orig, noisy)) )
    # print("WSSIMv2 = {}".format(il.AccCalc.calculateWSSIMv2(orig, noisy, 16)) )
    # print("WSSIMv3 = {}".format(il.AccCalc.calculateWSSIMv3(orig, noisy, 16)) )
    # print("WSSIMPaper = {}".format(il.AccCalc.calculateWSSIMPaper(orig, noisy)) )
    
    print("\nROTATION\n")
    noisy = il.Noiser.rotateImg(orig, 30)
    
    # print()
#     levels =  [-30 + x*2 for x in range(30+1)]
#     levels.extend([-0.5, -0.1, 0.1, 0.5])
#     levels.sort()
#     print("LEVELS: {}".format(levels) )
    print("SSIM = {}".format(il.AccCalc.calculateSSIM(orig, noisy)) )
    print("SLSSIMv5 = {}".format(il.AccCalc.calculateSLSSIMv5(orig, noisy, 8, True)) )
    print("SLSSIMv5 16 = {}".format(il.AccCalc.calculateSLSSIMv5(orig, noisy, 16, True)) )
    print("WSSIMv1 = {}".format(il.AccCalc.calculateWSSIM(orig, noisy)) )
    print("L1 = {}".format(il.AccCalc.calculateL1(orig, noisy)) )
    print("L1_Mod = {}".format(il.AccCalc.calculateL1_mod(orig, noisy)) )
    
    
    return (orig, noisy)

def calcL1(i1,i2):
    return il.AccCalc.calculateL1(i1,i2)

def printResultsForImages(origPath, noisyPath):

    origPath = origPath.strip()
    noisyPath = noisyPath.strip()

    orig = il.openImg(origPath).astype(np.uint8)
    noisy = il.Noiser.addPoissonGaussianNoise(orig, b = 0.1, a = 31, doNoise = 1).astype(np.uint8)
    # noisy = il.openImg(noisyPath)

    ssim(orig, noisy)

    # print("SLSSIMv5 16 = {}".format(il.AccCalc.calculateSLSSIMv5(orig, noisy, 16, True)) )
    # print("L1 = {}".format(il.AccCalc.calculateL1(orig, noisy)) )
    print()
    print()
    print("MSE = {}".format(il.AccCalc.calculateMSE(orig, noisy)) )
    print("L1_Mod = {}".format(il.AccCalc.calculateL1_mod(orig, noisy)) )
    print("SSIM = {}".format(il.AccCalc.calculateSSIM(orig, noisy)) )
    print("WSSIMv1 = {}".format(il.AccCalc.calculateWSSIM(orig, noisy)) )
    print("SLSSIMv5 = {}".format(il.AccCalc.calculateSLSSIMv5(orig, noisy, 8, True)) )
    print("SSIM again = {}".format(ssim(orig, noisy)) )
    print()
    print()


# getChestXrays()
printResultsForImages("/Users/jonas/Downloads/Conference-LaTeX-template_7-9-18_linas/pix/stanford95.png", "/Users/jonas/Downloads/Conference-LaTeX-template_7-9-18_linas/pix/stanford95_poisson-gauss.png")
printResultsForImages("/Users/jonas/Downloads/Conference-LaTeX-template_7-9-18_linas/pix/xray99.png", "/Users/jonas/Downloads/Conference-LaTeX-template_7-9-18_linas/pix/xray99_poisson-gauss.png")


