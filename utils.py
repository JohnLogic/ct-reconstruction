import os
from datetime import datetime
from pathlib import Path

import keras
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from matplotlib import pyplot as plt

from custom_3d_image_generator import ImageDataGenerator as ImageDG3d


class KerasUtils:

    @classmethod
    # I removed the 'ifs', see if it works or not
    def get_img_fit_flow_in_3d(cls, image_config: dict, fit_smpl_size: int, directory: str,
                         target_size: (int, int), batch_size: int, shuffle: bool,
                         seed: int, interpolation_method: str,
                         save_to_dir: str = None, save_prefix: str = "",
                         save_format: str = None, frames_per_step = 4):
        # Based on: https://github.com/smileservices/keras_utils/blob/master/utils.py
        '''
        Sample the generators to get fit data
        image_config  dict   holds the vars for data augmentation &
        fit_smpl_size float  subunit multiplier to get the sample size for normalization

        directory     str    folder of the images
        target_size   tuple  images processed size
        batch_size    str
        shuffle       bool
        '''
        generator_settings_dict = dict(  # Lets use a dict to reduce repetition
            directory=directory,
            target_size=target_size,
            batch_size=batch_size,
            shuffle=shuffle,
            color_mode='grayscale',  # ->"grayscale"<-, "rgb", "rgba"
            class_mode=None,
            seed=seed,
            save_to_dir=save_to_dir,
            save_prefix=save_prefix,
            save_format=save_format,
        )

        # If featurewise std normalization is used, we perform hack. If not - return simple
        if 'featurewise_std_normalization' in image_config:
            img_gen = ImageDG3d()
            batches = img_gen.flow_from_directory(**generator_settings_dict, frames_per_step=frames_per_step)
            fit_samples = np.array([])

            # TODO: test if third channel should be '1' (grayscale)
            fit_samples.resize((0, target_size[0], target_size[1], 1))
            for i in range(batches.samples // batch_size):  # NOTE: use of '//' yields int
                imgs = next(batches)  # Changed from imgs,labels
                idx = np.random.choice(imgs.shape[0], batch_size * fit_smpl_size, replace=False)
                np.vstack((fit_samples, imgs[idx]))

            new_img_gen = ImageDG3d(**image_config)
            new_img_gen.fit(fit_samples)
            return new_img_gen.flow_from_directory(**generator_settings_dict)
        else:
            # NOTE: In Keras, ImageDataGenerator is NOT additive, it replaces batches with given augments
            simple_img_gen = ImageDG3d(**image_config)
            return simple_img_gen.flow_from_directory(**generator_settings_dict, frames_per_step=frames_per_step)

    @classmethod
    # I removed the 'ifs', see if it works or not
    def get_img_fit_flow(cls, image_config : dict, fit_smpl_size : int, directory : str,
                         target_size : (int, int), batch_size : int, shuffle : bool,
                         seed : int, interpolation_method : str,
                         save_to_dir : str = None, save_prefix : str = "",
                         save_format : str = None):
        # Based on: https://github.com/smileservices/keras_utils/blob/master/utils.py
        '''
        Sample the generators to get fit data
        image_config  dict   holds the vars for data augmentation &
        fit_smpl_size float  subunit multiplier to get the sample size for normalization

        directory     str    folder of the images
        target_size   tuple  images processed size
        batch_size    str
        shuffle       bool
        '''
        generator_settings_dict = dict(  # Lets use a dict to reduce repetition
            directory=directory,
            target_size=target_size,
            batch_size=batch_size,
            shuffle=shuffle,
            color_mode='grayscale',  # ->"grayscale"<-, "rgb", "rgba"
            class_mode=None,
            seed=seed,
            interpolation=interpolation_method,  # ->"nearest"<-, "bilinear", "bicubic"
            save_to_dir=save_to_dir,
            save_prefix=save_prefix,
            save_format=save_format,
        )

        # If featurewise std normalization is used, we perform hack. If not - return simple
        if 'featurewise_std_normalization' in image_config:
            img_gen = ImageDataGenerator()
            batches = img_gen.flow_from_directory(**generator_settings_dict)
            fit_samples = np.array([])

            # TODO: test if third channel should be '1' (grayscale)
            fit_samples.resize((0, target_size[0], target_size[1], 1))
            for i in range(batches.samples // batch_size): #NOTE: use of '//' yields int
                imgs = next(batches) #Changed from imgs,labels
                idx = np.random.choice(imgs.shape[0], batch_size * fit_smpl_size, replace=False)
                np.vstack((fit_samples, imgs[idx]))

            new_img_gen = ImageDataGenerator(**image_config)
            new_img_gen.fit(fit_samples)
            return new_img_gen.flow_from_directory(**generator_settings_dict)
        else:
            # NOTE: In Keras, ImageDataGenerator is NOT additive, it replaces batches with given augments
            simple_img_gen = ImageDataGenerator(**image_config)
            return simple_img_gen.flow_from_directory(**generator_settings_dict)

    # LINK TO ORIGINAL: https://stackoverflow.com/questions/54323960/save-keras-model-at-specific-epochs
    class EpochSaver(keras.callbacks.Callback):

        def __init__(self, epoch_save_interval : int, file_name_prefix : str,
                     save_last_only : bool = False, initial_model_epochs = 0):
            super().__init__()
            self._epoch_save_interval = epoch_save_interval
            self._file_name_prefix = file_name_prefix
            self._save_last_only = save_last_only
            #NOTE: initial_model_epochs only required when continuing to train already trained model
            self._initial_model_epochs = initial_model_epochs

        def on_epoch_end(self, epoch, logs={}):
            if epoch % self._epoch_save_interval == 0 and epoch > 0:  # or save after some epoch, each k-th epoch etc.
                if self._save_last_only:
                    self.model.save("{}_last-only.h5".format(self._file_name_prefix))
                else:
                    self.model.save("{}_epoch-{}.h5".format(self._file_name_prefix, epoch+self._initial_model_epochs))

class FileUtils:

    DEFAULT_TEMP_FOLDER = "./tmp"

    @classmethod
    def apth(cls, path_string) -> str:
        path_obj = Path(path_string)
        absolute_path = str(path_obj.resolve())
        return cls.pth(absolute_path)

    @classmethod
    def pth(cls, path_string) -> str:
        return os.path.join(*path_string.split('/'))

    @classmethod
    def create_dir(cls, path_string : str, fail_if_exists : bool = False) -> str:
        path_obj = Path(path_string)
        path_obj.mkdir(parents=True, exist_ok=not fail_if_exists)
        return str(path_obj.resolve())

    @classmethod
    def file_name(cls, path_string : str, with_extension : bool = False) -> str:
        path_obj = Path(path_string)
        return path_obj.stem if not with_extension else path_obj.name

    @classmethod
    def home_dir(cls) -> str:
        return str(Path.home())

class DateUtils:

    DATE_FORMAT = "%Y-%m-%d"
    TIME_FORMAT = "%H-%M-%S"
    DATETIME_FORMAT = "{}_{}".format(DATE_FORMAT, TIME_FORMAT)

    @classmethod
    def now(cls):
        now = datetime.now()
        return now

    @classmethod
    def formatted_datetime_now(cls):
        return cls.now().strftime(cls.DATETIME_FORMAT)

class FunkyUtils:

    @classmethod
    def len_iterable(cls, iterable):
        # Based on: https://stackoverflow.com/questions/31011631/python-2-3-object-of-type-zip-has-no-len
        return sum(1 for _ in iterable)


class ImageUtils:

    @classmethod
    def unscale_image(cls, input_image : np.ndarray, scaling_factor : int = 255):
        image = input_image.copy() # Make a copy
        if (scaling_factor is not None): # scale
            image = np.multiply(image, scaling_factor)
        image = np.clip(image, 0, 255) # clip grayscale to [0, 255]
        if len(image.shape) == 3 and image.shape[2] == 1: # squeeze for plt.imgshow
            image = image.squeeze(axis=2)
        return image

    @classmethod
    def show_ct_image(cls, image : np.ndarray, scaling_factor = None):
        pass

    @classmethod
    def show_2_ct_images(cls, image_1 : np.ndarray, image_2 : np.ndarray,
                         image_1_title : str = "", image_2_title : str = "",
                         scaling_factor : int = 255):

        img_1 = image_1.copy()
        img_2 = image_2.copy()

        if (scaling_factor is not None):
            img_1 = cls.unscale_image(img_1, scaling_factor)
            img_2 = cls.unscale_image(img_2, scaling_factor)

        plt.subplot(121), plt.imshow(img_1, cmap='gray'), plt.title(image_1_title)
        plt.xticks([]), plt.yticks([])
        plt.subplot(122), plt.imshow(img_2, cmap='gray'), plt.title(image_2_title)
        plt.xticks([]), plt.yticks([])
        plt.show()

