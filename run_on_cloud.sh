#!/bin/bash

EXPERIMENT_DATA_FOLDER=$(pwd)"/../ct-reconstruction-experiments/"
DATASET_FOLDER=$(pwd)"/../prepared_mayo_experiment_data_other_split"
MODEL_NAME="unet"
BATCH_SIZE=16
MAX_EPOCHS=1
LOSS_FUNCTION_NAME="mse"
EXPERIMENT_NAME="unet_experiment_256_no_flip"
OUTPUT_DIRECTORY=$EXPERIMENT_DATA_FOLDER
EPOCH_SAVE_INTERVAL=10
RESOLUTION=256

python train.py --model "$MODEL_NAME" --datadir "$DATASET_FOLDER" --batchsize "$BATCH_SIZE" --epochs "$MAX_EPOCHS" --lossfoo "$LOSS_FUNCTION_NAME" --name "$EXPERIMENT_NAME" --outputdir "$OUTPUT_DIRECTORY" --epochsaveinterval "$EPOCH_SAVE_INTERVAL" --resolution "$RESOLUTION" --emailaddress "linas.vu@gmail.com"
