import glob

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from joblib import Parallel, delayed
from skimage import io
from skimage.color import rgb2gray

import metrics as Met

sess = tf.compat.v1.Session() 

nprocs = 8

# DATA_PATH = "../ct-data/prepared_mayo_experiment_data_other_split/test_full_dose/0"
DATA_PATH = r"C:\Users\Lenovo\repos\ct-reconstruction\output\prepared_mayo_experiment_data_other_split\test_full_dose\0"

def load_imgs(img_file):
    orig_image = io.imread(img_file)
    grayscale = rgb2gray(orig_image)
    del orig_image
    return grayscale

metrics_list = {
        "custom_tf_gssim": Met.custom_tf_gssim,
        "root_mean_squared_error": Met.root_mean_squared_error,
        #"dssim_metric": Met.dssim_metric,
        #"msssim_metric": Met.msssim_metric,
        #"custom_tf_ssim": Met.custom_tf_ssim,
        #"psnr": Met.psnr,
        "mean_split_4_gssim": Met.mean_split_4_gssim,
        #"sum_split_4_gssim": Met.sum_split_4_gssim,
        "mean_split_16_gssim": Met.mean_split_16_gssim,
        #"max_split_16_gssim": Met.max_split_16_gssim,
        #"sum_split_16_gssim": Met.sum_split_16_gssim,
        #"mean_split_64_gssim": Met.mean_split_64_gssim,
        #"sum_split_64_gssim": Met.sum_split_64_gssim,
        #"mean_top_3_in_4_split_16_gssim": Met.mean_top_3_in_4_split_16_gssim,
        #"mean_top_3_in_4_split_64_gssim": Met.mean_top_3_in_4_split_64_gssim,
        #"old_slssim_4": Met.old_slssim_4,
        #"old_slssim_8": Met.old_slssim_8,
        #"experiment_metric" : Met.experiment_metric,
        #"slgssim_8": Met.slgssim_8,
        #"slgssim_4": Met.slgssim_4,
        #"three_dimensional_gssim": Met.three_dimensional_gssim,
        #"three_dimensional_experimental_gssim": Met.three_dimensional_experimental_gssim,
        #"three_dimensional_gssim_proper_only": Met.three_dimensional_gssim_proper_only,
        #"three_dimensional_experimental_gssim_proper_only": Met.three_dimensional_experimental_gssim_proper_only,
        #"three_dimensional_ssim_proper_only": Met.three_dimensional_experimental_ssim_proper_only,
        #"three_dimensional_mse_proper_only": Met.three_dimensional_experimental_mse_proper_only,
}

#FILE = "/img_2.png"
#img = io.imread(DATA_PATH + FILE)
#io.imshow(img)
#io.show()

files = glob.glob(DATA_PATH + "/*png", recursive=True)

X_data = []
X_data.extend(Parallel(n_jobs=nprocs)(delayed(load_imgs)(im_file) for im_file in files))

#
# embed()


noise = np.arange(0.001, 0.01, 0.003)



D = {"test": ""}
for met in metrics_list.keys():
    D[met] = []
del D["test"]


with tf.Session() as sess:
    for met in metrics_list.keys():
        for n in noise:
            vals = []
            for k in range(10):
                s = np.random.normal(0, n, 512*512).reshape((512, 512))
                im = np.array(X_data[k]).astype(np.float32)
                imn = np.array(X_data[k]) + s
                imn = np.clip(imn, 0, 1).astype(np.float32)
                m = metrics_list[met]
                im = np.expand_dims(im, axis=2)
                im = np.expand_dims(im, axis=0)
                imn = np.expand_dims(imn, axis=2)
                imn = np.expand_dims(imn, axis=0)
                v = m(im, imn) 
                vals.append(sess.run(v))
            D[met].append(np.mean(vals))
            print("HELLO")
            break
                #print(sess.run(v))

for met in metrics_list.keys():
    plt.plot(noise, D[met])
plt.legend(metrics_list.keys())
plt.show()


